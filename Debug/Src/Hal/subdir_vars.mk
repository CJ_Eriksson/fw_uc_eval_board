################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/Hal/adc14.c \
../Src/Hal/aes256.c \
../Src/Hal/comp_e.c \
../Src/Hal/cpu.c \
../Src/Hal/crc32.c \
../Src/Hal/cs.c \
../Src/Hal/dma.c \
../Src/Hal/flash.c \
../Src/Hal/flash_a.c \
../Src/Hal/fpu.c \
../Src/Hal/gpio.c \
../Src/Hal/i2c.c \
../Src/Hal/interrupt.c \
../Src/Hal/lcd_f.c \
../Src/Hal/mpu.c \
../Src/Hal/pcm.c \
../Src/Hal/pmap.c \
../Src/Hal/pss.c \
../Src/Hal/ref_a.c \
../Src/Hal/reset.c \
../Src/Hal/rtc_c.c \
../Src/Hal/spi.c \
../Src/Hal/sysctl.c \
../Src/Hal/sysctl_a.c \
../Src/Hal/systick.c \
../Src/Hal/timer32.c \
../Src/Hal/timer_a.c \
../Src/Hal/uart.c \
../Src/Hal/wdt_a.c 

C_DEPS += \
./Src/Hal/adc14.d \
./Src/Hal/aes256.d \
./Src/Hal/comp_e.d \
./Src/Hal/cpu.d \
./Src/Hal/crc32.d \
./Src/Hal/cs.d \
./Src/Hal/dma.d \
./Src/Hal/flash.d \
./Src/Hal/flash_a.d \
./Src/Hal/fpu.d \
./Src/Hal/gpio.d \
./Src/Hal/i2c.d \
./Src/Hal/interrupt.d \
./Src/Hal/lcd_f.d \
./Src/Hal/mpu.d \
./Src/Hal/pcm.d \
./Src/Hal/pmap.d \
./Src/Hal/pss.d \
./Src/Hal/ref_a.d \
./Src/Hal/reset.d \
./Src/Hal/rtc_c.d \
./Src/Hal/spi.d \
./Src/Hal/sysctl.d \
./Src/Hal/sysctl_a.d \
./Src/Hal/systick.d \
./Src/Hal/timer32.d \
./Src/Hal/timer_a.d \
./Src/Hal/uart.d \
./Src/Hal/wdt_a.d 

OBJS += \
./Src/Hal/adc14.obj \
./Src/Hal/aes256.obj \
./Src/Hal/comp_e.obj \
./Src/Hal/cpu.obj \
./Src/Hal/crc32.obj \
./Src/Hal/cs.obj \
./Src/Hal/dma.obj \
./Src/Hal/flash.obj \
./Src/Hal/flash_a.obj \
./Src/Hal/fpu.obj \
./Src/Hal/gpio.obj \
./Src/Hal/i2c.obj \
./Src/Hal/interrupt.obj \
./Src/Hal/lcd_f.obj \
./Src/Hal/mpu.obj \
./Src/Hal/pcm.obj \
./Src/Hal/pmap.obj \
./Src/Hal/pss.obj \
./Src/Hal/ref_a.obj \
./Src/Hal/reset.obj \
./Src/Hal/rtc_c.obj \
./Src/Hal/spi.obj \
./Src/Hal/sysctl.obj \
./Src/Hal/sysctl_a.obj \
./Src/Hal/systick.obj \
./Src/Hal/timer32.obj \
./Src/Hal/timer_a.obj \
./Src/Hal/uart.obj \
./Src/Hal/wdt_a.obj 

OBJS__QUOTED += \
"Src\Hal\adc14.obj" \
"Src\Hal\aes256.obj" \
"Src\Hal\comp_e.obj" \
"Src\Hal\cpu.obj" \
"Src\Hal\crc32.obj" \
"Src\Hal\cs.obj" \
"Src\Hal\dma.obj" \
"Src\Hal\flash.obj" \
"Src\Hal\flash_a.obj" \
"Src\Hal\fpu.obj" \
"Src\Hal\gpio.obj" \
"Src\Hal\i2c.obj" \
"Src\Hal\interrupt.obj" \
"Src\Hal\lcd_f.obj" \
"Src\Hal\mpu.obj" \
"Src\Hal\pcm.obj" \
"Src\Hal\pmap.obj" \
"Src\Hal\pss.obj" \
"Src\Hal\ref_a.obj" \
"Src\Hal\reset.obj" \
"Src\Hal\rtc_c.obj" \
"Src\Hal\spi.obj" \
"Src\Hal\sysctl.obj" \
"Src\Hal\sysctl_a.obj" \
"Src\Hal\systick.obj" \
"Src\Hal\timer32.obj" \
"Src\Hal\timer_a.obj" \
"Src\Hal\uart.obj" \
"Src\Hal\wdt_a.obj" 

C_DEPS__QUOTED += \
"Src\Hal\adc14.d" \
"Src\Hal\aes256.d" \
"Src\Hal\comp_e.d" \
"Src\Hal\cpu.d" \
"Src\Hal\crc32.d" \
"Src\Hal\cs.d" \
"Src\Hal\dma.d" \
"Src\Hal\flash.d" \
"Src\Hal\flash_a.d" \
"Src\Hal\fpu.d" \
"Src\Hal\gpio.d" \
"Src\Hal\i2c.d" \
"Src\Hal\interrupt.d" \
"Src\Hal\lcd_f.d" \
"Src\Hal\mpu.d" \
"Src\Hal\pcm.d" \
"Src\Hal\pmap.d" \
"Src\Hal\pss.d" \
"Src\Hal\ref_a.d" \
"Src\Hal\reset.d" \
"Src\Hal\rtc_c.d" \
"Src\Hal\spi.d" \
"Src\Hal\sysctl.d" \
"Src\Hal\sysctl_a.d" \
"Src\Hal\systick.d" \
"Src\Hal\timer32.d" \
"Src\Hal\timer_a.d" \
"Src\Hal\uart.d" \
"Src\Hal\wdt_a.d" 

C_SRCS__QUOTED += \
"../Src/Hal/adc14.c" \
"../Src/Hal/aes256.c" \
"../Src/Hal/comp_e.c" \
"../Src/Hal/cpu.c" \
"../Src/Hal/crc32.c" \
"../Src/Hal/cs.c" \
"../Src/Hal/dma.c" \
"../Src/Hal/flash.c" \
"../Src/Hal/flash_a.c" \
"../Src/Hal/fpu.c" \
"../Src/Hal/gpio.c" \
"../Src/Hal/i2c.c" \
"../Src/Hal/interrupt.c" \
"../Src/Hal/lcd_f.c" \
"../Src/Hal/mpu.c" \
"../Src/Hal/pcm.c" \
"../Src/Hal/pmap.c" \
"../Src/Hal/pss.c" \
"../Src/Hal/ref_a.c" \
"../Src/Hal/reset.c" \
"../Src/Hal/rtc_c.c" \
"../Src/Hal/spi.c" \
"../Src/Hal/sysctl.c" \
"../Src/Hal/sysctl_a.c" \
"../Src/Hal/systick.c" \
"../Src/Hal/timer32.c" \
"../Src/Hal/timer_a.c" \
"../Src/Hal/uart.c" \
"../Src/Hal/wdt_a.c" 


