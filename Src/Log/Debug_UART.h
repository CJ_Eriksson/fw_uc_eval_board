/**
 * @file Debug_UART.h
 * @brief UART interface for transmitt of debug messages
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _DEBUG_UART_H_H
#define _DEBUG_UART_H_H

/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */
#define UART_DEBUG_MAX_RECEIVE_BUFFER 512
/* Debug message receive has a fixed value since if we use a DMA for UART
 * receive the length of the transfer needs to be known. If there is a way to
 * find at what address the DMA is transferring to the destination address this
 * can be done easier since we then will know the head of the buffer.
 */
#define UART_DEBUG_MESSAGE_SIZE 16  // Must be evenly divideable with the UART_DEBUG_MAX_RECEIVE_BUFFER

/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus debug_uart_dma_init(eUSCI_UART* device,
                                uint32_t dma_rx_channel,
                                uint32_t dma_tx_channel);
ErrorStatus debug_uart_rx_reset();
ErrorStatus debug_uart_dma_send(void* srcAddr, uint32_t number_of_bytes);
ErrorStatus debug_uart_read(uint8_t* read_buff, uint16_t* const number_of_bytes_read);
// TODO(caer) Add function to read size of received buffer
#ifdef __cplusplus
}
#endif

#endif /* _DEBUG_UART_H_H */

/*
 * End of file: Debug_UART.h
 */
