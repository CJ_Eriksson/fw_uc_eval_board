/**
 * @file Debug_UART.h
 * @brief UART interface for transmitt and receive of debug messages.
 * Each debug message consists of UART_DEBUG_MESSAGE_SIZE number of bytes. The
 * message will not be transfered to the receive buffer of the debug until a
 * complete message has been received. This is to simplify the protocol to only
 * look for the number of bytes received. No transmitt protocol is used and it
 * is up to the user of the driver to verify that all messages are sent in
 * length of 16 bytes.
 * The Transmitt is not dependent of UART_DEBUG_MESSAGE_SIZE. It will transmitt
 * the number of bytes that was sent to the send function.
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../Common/common_types.h"
#include "../Hal/uart.h"
#include "../Hal/dma.h"

/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "Debug_UART.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static eUSCI_UART* uart;
static uint8_t receiveBuffer[UART_DEBUG_MAX_RECEIVE_BUFFER];
#pragma DATA_ALIGN(receiveBuffer, UART_DEBUG_MAX_RECEIVE_BUFFER) // Align with size to easy handle overflow and wrap around at max size.

static uint8_t* receiverBufferHead = receiveBuffer;
static uint8_t* receiverBufferTail = receiveBuffer;
// The buffer is a circular buffer, this variable is a helper variable that
// keeps track of how much of the buffer that is currently used.
static uint16_t bufferUsed = 0;
static bool_t txBusy = FALSE;
static uint32_t dmaRxChannel;
static uint32_t dmaTxChannel;
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */
void uart_dma_tx_isr();
void uart_dma_rx_isr();
/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief Init uart debug with DMA TX and RX
 * @param [in]  device - Uart device
 *              dma_rx_channel - DMA RX Channel
 *              dma_tx_channel - DMA TX Channel
 * @return SUCCESS on SUCCESS
 */
ErrorStatus debug_uart_dma_init(eUSCI_UART* device,
                                uint32_t dma_rx_channel,
                                uint32_t dma_tx_channel)
{
  if (device == NULL) {
    return ERROR;
  } else {
    uart = device;
    dmaRxChannel = dma_rx_channel;
    dmaTxChannel = dma_tx_channel;
  }
  // DMA Receive init
  DMA_disableChannelAttribute(dmaRxChannel ,
                              UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST |
                              UDMA_ATTR_HIGH_PRIORITY |
                              UDMA_ATTR_REQMASK);
  // Setting Control Indexes.
  DMA_setChannelControl(UDMA_PRI_SELECT | dmaRxChannel ,
                        UDMA_SIZE_8 |
                        UDMA_SRC_INC_NONE |
                        UDMA_DST_INC_8 |
                        UDMA_ARB_1);

  DMA_assignChannel(dmaRxChannel);

  DMA_clearInterruptFlag(dmaRxChannel&0x0F);
  DMA_setChannelTransfer(UDMA_PRI_SELECT | dmaRxChannel ,
                         UDMA_MODE_BASIC, (void*) &((EUSCI_A_Type*)uart->moduleInstance)->RXBUF,
                         (void*)receiverBufferHead, UART_DEBUG_MESSAGE_SIZE);

  DMA_assignInterrupt(DMA_INT1, dmaRxChannel&0x0F);   // TODO(caer) dma interrupt shall be handled by main, not in the driver instance.
  DMA_registerInterrupt(DMA_INT1,
                        &uart_dma_rx_isr);
  DMA_enableChannel(dmaRxChannel);

  // DMA Transmitt init
  DMA_disableChannelAttribute(dmaTxChannel ,
                              UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST |
                              UDMA_ATTR_HIGH_PRIORITY |
                              UDMA_ATTR_REQMASK);
  // Setting Control Indexes.
  DMA_setChannelControl(UDMA_PRI_SELECT | dmaTxChannel ,
                        UDMA_SIZE_8 |
                        UDMA_SRC_INC_8 |
                        UDMA_DST_INC_NONE |
                        UDMA_ARB_1);

  DMA_assignChannel(dmaTxChannel);

  DMA_clearInterruptFlag(dmaTxChannel  & 0x0F);

  DMA_assignInterrupt(DMA_INT2, dmaTxChannel  & 0x0F);  // TODO(caer) dma interrupt shall be handled by main, not in the driver instance.
  DMA_registerInterrupt(DMA_INT2,
                        &uart_dma_tx_isr);
  DMA_enableChannel(dmaTxChannel);
  return SUCCESS;
}

/**
 * @brief Read the received data from the RX buffer. A packet is 16 byte long
 * and all data is returned in units of 16 bytes.
 * @param [out] read_buffer - Data buffer to store the received data in.
 *                            Size of the buffer is handled by the user
 *              number_of_bytes_read - Number of bytes written to the read_buffer
 * @return SUCCESS on SUCCESS
 */
ErrorStatus debug_uart_read(uint8_t* read_buff, uint16_t* const number_of_bytes_read)
{
  ErrorStatus returnStatus = SUCCESS;
  *number_of_bytes_read = 0;
  if (bufferUsed >= UART_DEBUG_MESSAGE_SIZE) {
    uint8_t i = 0;
    for (i; i < UART_DEBUG_MESSAGE_SIZE; ++i) {
      if (bufferUsed <= UART_DEBUG_MAX_RECEIVE_BUFFER) {
        *read_buff++ = *receiverBufferTail;
        receiverBufferTail = receiveBuffer + ((uint32_t) receiverBufferTail + 1)
            % UART_DEBUG_MAX_RECEIVE_BUFFER;
        bufferUsed--;
        *number_of_bytes_read += 1;
      } else {
        returnStatus = ERROR;
        break;
      }
    }
  }
  return returnStatus;
}

/**
 * @brief Reset the receive buffer head and tail. This shall be used if a buffer
 * overflow has occur to reset the buffer pointers.
 * @param [in] -
 * @return SUCCESS on SUCCESS
 */
ErrorStatus debug_uart_rx_reset()
{
  receiverBufferHead = receiverBufferTail = &receiveBuffer[0];
  bufferUsed = 0;
  return SUCCESS;
}

/**
 * @brief Send debug data via UART
 * @param [in] srcAddr - Pointer to buffer that shall be transmitted
 *             number_of_bytes - Number of bytes to be sent
 * @return SUCCESS on SUCCESS
 */
ErrorStatus debug_uart_dma_send(void* srcAddr, uint32_t number_of_bytes)
{
  ErrorStatus returnStatus = ERROR;
  if (!txBusy) {
    // Need to reset the ISR, otherwise it will trigger the DMA and causes a
    // missed byte in the second byte transfer. I don't know why.
    UART_clearInterruptFlag(uart->moduleInstance, 0x0A);
    DMA_clearInterruptFlag(dmaTxChannel  & 0x0F);
    DMA_setChannelTransfer(UDMA_PRI_SELECT | dmaTxChannel ,
                           UDMA_MODE_BASIC,
                           srcAddr,
                           (void*) &((EUSCI_A_Type*)uart->moduleInstance)->TXBUF,
                           number_of_bytes);
    DMA_enableChannel(dmaTxChannel);
    txBusy = TRUE;  // Needs to be before trigger of TX, otherwise race conditions with the ISR that sets it to FALSE
    ((EUSCI_A_Type*)uart->moduleInstance)->IFG = 0x02; // Trigger TX interrupt to start the DMA transfer.
  } else {
    returnStatus = BUSY;
  }
  return returnStatus;
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief DMA ISR for transmit of data ready. This ISR is fired after the DMA
 * has sent all bytes
 * @param [in] -
 * @return None
 */
void uart_dma_tx_isr()
{
  DMA_clearInterruptFlag(dmaTxChannel  & 0x0F);
  txBusy = FALSE;
  // TODO(caer) Check if a total transmitt of byte is needed before enable new transfer. This migh be best to implement in the send function to not use a busy wait in the ISR
}
/**
 * @brief DMA ISR for receive of data. This ISR is fired after the DMA
 * has received UART_DEBUG_MESSAGE_SIZE number of bytes
 * @param [in] -
 * @return None
 */
void uart_dma_rx_isr()
{
  receiverBufferHead = receiveBuffer +
      ((uint32_t)receiverBufferHead + UART_DEBUG_MESSAGE_SIZE) %
      UART_DEBUG_MAX_RECEIVE_BUFFER;
  bufferUsed += UART_DEBUG_MESSAGE_SIZE;
  DMA_clearInterruptFlag(dmaRxChannel  & 0x0F);
  DMA_setChannelTransfer(UDMA_PRI_SELECT | dmaRxChannel ,
                         UDMA_MODE_BASIC,
                         (void*) &((EUSCI_A_Type*)uart->moduleInstance)->RXBUF,
                         (void*)receiverBufferHead,
                         UART_DEBUG_MESSAGE_SIZE);
  DMA_enableChannel(dmaRxChannel);
}
/*
 * End of file: Debug_UART.c
 */
