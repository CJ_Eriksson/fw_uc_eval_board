/**
 * @file Log_Buffer.c
 * @brief Buffer for log messages to be printed or sent. The log consists of two
 * ring buffers, which allows for two levels of logging. Main poll and one
 * interrupt level. If not all interrupts has the same priority, one additional
 * buffer will be needed that can handle that extra level of write to buffer or
 * the user shall assure that only one ISR level uses the logger. If not, the
 * buffer might be corrupted.
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../Common/common_types.h"
#include "../Hal/rtc_c.h"
#include "../Hal/systick.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "../Log/Log_Buffer.h"
/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define NUL_CHAR_SIZE        1u    /**< Size of the NUL character in bytes.            */
#define DBG_RING_BUFFER_SIZE 1024 /* Size of debug input ring buffers in nr of bytes. */
#define DBG_BUFFER_EMPTY 0u
#define PUT_CHAR_ERROR -1
#define DBG_POLL_OVERFLOW_MSG     "\r\n*** Debug Poll Buffer Overflow! ***\r\n"
#define DBG_INT_OVERFLOW_MSG      "\r\n*** Debug Int Buffer Overflow! ***\r\n"
#define TIME_ARRAY_SIZE_NO_YEAR 15
#define TIME_ARRAY_SIZE_YEAR 19
#define DBG_TX_BUFFER_SIZE 512
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */
typedef struct {
  uint32_t milliseconds;
  uint16_t seconds;
  uint16_t minutes;
  uint16_t hours;
  uint16_t day;
  uint16_t month;
  uint32_t year;
} ASCII_Calendar_s ;

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */


static uint8_t ringBufferOutPoll[DBG_RING_BUFFER_SIZE] = {0};
static uint16_t ringBufPollTailPos = 0u;
static uint16_t ringBufPollCnt      = DBG_BUFFER_EMPTY;
static bool_t ringBufPollOverflow = FALSE;
static uint8_t ringBufferOutInt[DBG_RING_BUFFER_SIZE] = {0};
static uint16_t ringBufIntTailPos = 0u;
static uint16_t ringBufIntCnt      = DBG_BUFFER_EMPTY;
static bool_t ringBufIntOverflow = FALSE;


/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */
static int16_t put_char(const plainChar ch, uint8_t* const buffer);
ErrorStatus get_time_date(plainChar* const time, bool_t use_years, uint8_t size_of_array);
/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief Initialization of the logger buffers
 * @param [in] none
 * @return SUCCESS on success
 */
ErrorStatus log_buffer_init(void)
{
  ErrorStatus retVal = SUCCESS;

  (void)memset(ringBufferOutPoll, 0, (int16_t)DBG_RING_BUFFER_SIZE); //lint !e960
  (void)memset(ringBufferOutInt, 0, (int16_t)DBG_RING_BUFFER_SIZE); //lint !e960
  ringBufPollCnt = DBG_BUFFER_EMPTY;
  ringBufIntCnt = DBG_BUFFER_EMPTY;
  ringBufPollOverflow = FALSE;
  ringBufIntOverflow = FALSE;

  return retVal;
}

/**
 * @brief Prints a string to debug poll log buffer. Writes the C string pointed
 * by format to the debug output.
 * @param [in] inString - The string to be printed
 * @return SUCCESS on success
 */
ErrorStatus log_buffer_poll_write_string(const plainChar * const inString)
{
  uint32_t index = 0uL;
  int16_t result = 0;
  int16_t nrCharsWritten = 0;   // Count bytes sent (or added to buffer), set to negative if error (EOF or PRINT_BAD_ARGUMENT)
  plainChar character;

  plainChar time[TIME_ARRAY_SIZE_NO_YEAR] = {0};
  uint8_t time_length = sizeof(time)/sizeof(time[0]);
  get_time_date(time, FALSE, time_length);
  uint8_t i = 0;
  for (i = 0; i < time_length; ++i) {
    character = time[i];
    put_char(character, &ringBufferOutPoll[0]);
  }
  put_char(0x20, &ringBufferOutPoll[0]);
  while (((plainChar) 0 != inString[index]) && (result >= 0)) {
    character = inString[index];
    index++;
    result = put_char(character, &ringBufferOutPoll[0]);
    nrCharsWritten++;
  }
  return SUCCESS; //TODO
}

/**
 * @brief Prints a string to debug interrupt log buffer. Writes the C string
 * pointed by format to the debug output.
 * @param [in] inString - The string to be printed
 * @return SUCCESS on success
 */
ErrorStatus log_buffer_int_write_string(const plainChar * const inString)
{
  uint32_t index = 0uL;
  int16_t result = 0;
  int16_t nrCharsWritten = 0;   // Count bytes sent (or added to buffer), set to negative if error (EOF or PRINT_BAD_ARGUMENT)
  plainChar character;

  plainChar time[TIME_ARRAY_SIZE_NO_YEAR] = {0};
  uint8_t time_length = sizeof(time)/sizeof(time[0]);
  get_time_date(time, FALSE, time_length);
  uint8_t i = 0;
  for (i = 0; i < time_length; ++i) {
    character = time[i];
    put_char(character, &ringBufferOutInt[0]);
  }
  put_char(0x20, &ringBufferOutInt[0]);
  while (((plainChar) 0 != inString[index]) && (result >= 0)) {
    character = inString[index];
    index++;
    result = put_char(character, &ringBufferOutInt[0]);
    nrCharsWritten++;
  }
  return SUCCESS; //TODO
}
/**
 * @brief Combine the poll and interrupt buffers to one log buffer. The function
 * does not sort the log messages according to time. First is the poll logg
 * written, then the isr logg.
 * @param [in] bufferRead - Pointer to read buffer to store both log buffers in
*              nrBytesWritten - Pointer to number of bytes written to buffer
 * @return SUCCESS on success
 */
ErrorStatus log_buffer_read(plainChar* bufferRead, uint16_t* const nrBytesWritten)
{
   *nrBytesWritten = 0;
   /* Alert external user when we have internal buffer overflow. */
   if (TRUE == ringBufPollOverflow) {
       uint16_t length = (uint16_t)sizeof(DBG_POLL_OVERFLOW_MSG) - NUL_CHAR_SIZE;
       uint8_t* message = (uint8_t *)DBG_POLL_OVERFLOW_MSG;
       for (length; length > 0; --length) {
         *bufferRead++ = *message++;
         *nrBytesWritten +=1;
       }
       ringBufPollOverflow = FALSE;
       ringBufPollTailPos = ringBufPollCnt = 0;
   } else {
     while ((ringBufPollCnt > DBG_BUFFER_EMPTY  )
         && (*nrBytesWritten   < DBG_TX_BUFFER_SIZE)) { //OBS TA BORT O ERS�TT MED BER�KNING AV TX L�NGD
       *bufferRead++ = ringBufferOutPoll[ringBufPollTailPos];
       ringBufPollCnt--;
       ringBufPollTailPos = (ringBufPollTailPos + 1u) % DBG_RING_BUFFER_SIZE;
       *nrBytesWritten +=1;
     }
   }
   /* Alert external user when we have internal buffer overflow. */
   if (TRUE == ringBufIntOverflow) {
     uint16_t length = (uint16_t)sizeof(DBG_INT_OVERFLOW_MSG) - NUL_CHAR_SIZE;
     uint8_t* message = (uint8_t *)DBG_INT_OVERFLOW_MSG;
   for (length; length > 0; --length) {
     *bufferRead++ = *message++;
     *nrBytesWritten +=1;
   }
     ringBufIntOverflow = FALSE;
     ringBufIntTailPos = ringBufIntCnt = 0;
   }
   else {
     while ((ringBufIntCnt > DBG_BUFFER_EMPTY  )
         && (*nrBytesWritten   < DBG_TX_BUFFER_SIZE)) {
       *bufferRead++ = ringBufferOutInt[ringBufIntTailPos];
       ringBufIntCnt--;
       ringBufIntTailPos = (ringBufIntTailPos + 1u) % DBG_RING_BUFFER_SIZE;
       *nrBytesWritten +=1;
     }
   }
   return SUCCESS; // TODO
}

/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief Puts a char in the debug output ring-buffer.
 * @param [in] ch - The character to put in the buffer
 *             buffer - Buffer to put the character in
 * @return This function returns the character written as an unsigned char cast
 * to an int16_t or EOF on error.
 */
static int16_t put_char(const plainChar ch, uint8_t* const buffer)
{
  static uint16_t ringBufPollHeadPos = 0u;
  static uint16_t ringBufIntHeadPos = 0u;
  int16_t  byteWritten    = (int16_t)ch;
  if (buffer == &ringBufferOutPoll[0]) {  // Poll log buffer
    if (ringBufPollCnt < DBG_RING_BUFFER_SIZE) {
      buffer[ringBufPollHeadPos] = (uint8_t)ch;
      ringBufPollHeadPos = (ringBufPollHeadPos + 1u) % DBG_RING_BUFFER_SIZE;
      ringBufPollCnt++;
    }
    else {
      ringBufPollOverflow = TRUE;
      byteWritten = PUT_CHAR_ERROR;
    }
  } else if (buffer == &ringBufferOutInt[0]) {  // Interrupt log buffer
    if (ringBufIntCnt < DBG_RING_BUFFER_SIZE) {
      buffer[ringBufIntHeadPos] = (uint8_t)ch;
      ringBufIntHeadPos = (ringBufIntHeadPos + 1u) % DBG_RING_BUFFER_SIZE;
      ringBufIntCnt++;
    }
    else {
      ringBufIntOverflow = TRUE;
      byteWritten = PUT_CHAR_ERROR;
    }
  } else {
    byteWritten = PUT_CHAR_ERROR;
  }

  return byteWritten;
}
/**
 * @brief Convert hex to ascii number. Maximum value 9999
 * @param [in] number - number to be converted
 * @return ascii number
 */
uint32_t hex_to_ascii(uint16_t number) {
  uint8_t i = 0;
  uint32_t ascii = 0x30303030;
  if (number < 10000) {
    while (number) {
      uint8_t d = number % 10;
      ascii |= d << (i++*8);
      number = number / 10;
    }
  }

  return ascii;
}

/**
 * @brief Get the current time and date
 * @param [in] use_years - Use year in date
 *        [in] size_of_array - size of the time array that will store the time
 *        and date
 *        [out] time - Time and date buffer pointer
 * @return SUCCESS on SUCCESS
 */
ErrorStatus get_time_date(plainChar* time, bool_t use_years, uint8_t size_of_array)
{
  if (time == NULL || size_of_array < TIME_ARRAY_SIZE_NO_YEAR) {
    return ERROR;
  }
  RTC_C_Calendar calenderTime = RTC_C_getCalendarTime();
  ASCII_Calendar_s calendarTimeAscii;
  calendarTimeAscii.milliseconds = hex_to_ascii(systime_get_tick()%1000);
  calendarTimeAscii.seconds = hex_to_ascii(calenderTime.seconds);
  calendarTimeAscii.minutes = hex_to_ascii(calenderTime.minutes);
  calendarTimeAscii.hours = hex_to_ascii(calenderTime.hours);
  calendarTimeAscii.day = hex_to_ascii(calenderTime.dayOfmonth);
  calendarTimeAscii.month = hex_to_ascii(calenderTime.month);
  calendarTimeAscii.year = hex_to_ascii(calenderTime.year);
  if (use_years == TRUE && size_of_array >= TIME_ARRAY_SIZE_YEAR) {
    *time++ = (calendarTimeAscii.year & 0xFF000000) >> 24;
    *time++ = (calendarTimeAscii.year & 0xFF0000) >> 16;
    *time++ = (calendarTimeAscii.year & 0xFF00) >> 8;
    *time++ = (calendarTimeAscii.year & 0xFF);
  }
  *time++ = (calendarTimeAscii.month & 0xFF00) >> 8;
  *time++ = (calendarTimeAscii.month & 0xFF);
  *time++ = (calendarTimeAscii.day & 0xFF00) >> 8;
  *time++ = (calendarTimeAscii.day & 0xFF);
  *time++ = (plainChar)':';
  *time++ = (calendarTimeAscii.hours & 0xFF00) >> 8;
  *time++ = (calendarTimeAscii.hours & 0xFF);
  *time++ = (calendarTimeAscii.minutes & 0xFF00) >> 8;
  *time++ = (calendarTimeAscii.minutes & 0xFF);
  *time++ = (calendarTimeAscii.seconds & 0xFF00) >> 8;
  *time++ = (calendarTimeAscii.seconds & 0xFF);
  *time++ = (plainChar)'.';
  *time++ = (calendarTimeAscii.milliseconds & 0xFF0000) >> 16;
  *time++ = (calendarTimeAscii.milliseconds & 0xFF00) >> 8;
  *time++ = (calendarTimeAscii.milliseconds & 0xFF);

  return SUCCESS;
}
/*
 * End of file: Log_Buffer.c
 */
