/**
 * @file Log_Buffer.h
 * @brief Buffer for log messages to be printed or sent. The log consists of two
 * ring buffers, which allows for two levels of logging. Main poll and one
 * interrupt level. If not all interrupts has the same priority, one additional
 * buffer will be needed that can handle that extra level of write to buffer.
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _LOG_BUFFER_H_H
#define _LOG_BUFFER_H_H

#include <stdint.h>
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus log_buffer_init(void);
ErrorStatus log_buffer_poll_write_string(const plainChar * const inString);
ErrorStatus log_buffer_int_write_string(const plainChar * const inString);
ErrorStatus log_buffer_read(plainChar * bufferRead, uint16_t* const nrBytesWritten);


#ifdef __cplusplus
}
#endif

#endif /* _LOG_BUFFER_H_H */

/*
 * End of file: Log_Buffer.h
 */
