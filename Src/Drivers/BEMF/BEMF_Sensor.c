/**
 * @file BEMF_Sensor.c
 * @brief Sensor for measuring the back EMF differentially over a motor winding
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Common/common_types.h"
#include "../../Hal/gpio.h"
#include "../../Hal/dma.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "BEMF_Sensor.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
/* At max speed 20000 usteps/s a ustep duration is 50us and with a
max ADC speed of 1Msps -> 50 samples/ustep. This sample rate per ustep will
be held at each speed down to 1000 usteps/s. With the different automatic sample
rates for the ADC the samples per usteps will be around 60 to 100. Therefore the
buffer size will be set to 110
*/
#define MAXIMUM_BEMF_BUFFER 256
#define STEP_FREQUENCY 75000 // TODO(caer) move
#define RUNNING_MEAN_COUNT 10
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */

static bool_t measReady  = FALSE;
static uint16_t bemfMeasurments[MAXIMUM_BEMF_BUFFER] = {0};
static uint16_t bemfRunningMean[RUNNING_MEAN_COUNT] = {0};
static uint8_t bemfRunningMeanCnt = 0;
static float filteredBemf = 0.0f;
static uint32_t prevBemfMeas = 0;
static float const filterConstant = 0.05;
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
ErrorStatus bemf_init(adc_channel_e channel)
{
  // TODO(caer) Change. This is just to initialize the DMA for the BEMF meas.
  // Can't use adc_start_meas every time since itt akes too long to execute
  adc_start_meas(channel,
                 TRUE,
                 MAXIMUM_BEMF_BUFFER,
                 bemfMeasurments);
  bemf_stop_meas();
}

ErrorStatus bemf_set_conversion_rate(float speed)
{
  ErrorStatus  returnStatus = ERROR;
  uint16_t speedUstepsPerSec = (uint16_t)(STEP_FREQUENCY*speed);
  uint32_t preDivFactor = ADC_PREDIVIDER_1;
  uint32_t divFactor = ADC_DIVIDER_1;
  uint32_t sampleAndHold = ADC_PULSE_WIDTH_4;

  /* Speeds at which to change ADC frequency to match the ~60 samples/step.
  * The numbers are calculated from the different ADC sample freqs that are
  * possible to achieve with ADC speed of 25MHz. If the stepper freq is changed
  * from 20kHz, the table needs to be rewritten.
  */
  /*
  if (speedUstepsPerSec <= 20000 && speedUstepsPerSec > 19230) {
    preDivFactor = ADC_PREDIVIDER_1;
    sampleAndHold = ADC_PULSE_WIDTH_4;
  } else if (speedUstepsPerSec > 14710) {
    divFactor = ADC_DIVIDER_1;
    sampleAndHold = ADC_PULSE_WIDTH_8;
  } else if (speedUstepsPerSec > 11230) {
    divFactor = ADC_DIVIDER_1;
    sampleAndHold = ADC_PULSE_WIDTH_16;
  } else if (speedUstepsPerSec > 9620) {
    divFactor = ADC_DIVIDER_2;
    sampleAndHold = ADC_PULSE_WIDTH_4;
  } else if (speedUstepsPerSec > 7560) {
    divFactor = ADC_DIVIDER_2;
    sampleAndHold = ADC_PULSE_WIDTH_8;
  } else if (speedUstepsPerSec > 6070) {
    divFactor = ADC_DIVIDER_3;
    sampleAndHold = ADC_PULSE_WIDTH_4;
  } else if (speedUstepsPerSec > 4990) {
    divFactor = ADC_DIVIDER_1;
    sampleAndHold = ADC_PULSE_WIDTH_64;
  } else if (speedUstepsPerSec > 3870) {
    divFactor = ADC_DIVIDER_2;
    sampleAndHold = ADC_PULSE_WIDTH_32;
  } else if (speedUstepsPerSec > 3040) {
    divFactor = ADC_DIVIDER_5;
    sampleAndHold = ADC_PULSE_WIDTH_8;
  } else if (speedUstepsPerSec > 2010) {
    divFactor = ADC_DIVIDER_2;
    sampleAndHold = ADC_PULSE_WIDTH_64;
  } else if (speedUstepsPerSec > 990) {
    divFactor = ADC_DIVIDER_5;
    sampleAndHold = ADC_PULSE_WIDTH_32;
  } else if (speedUstepsPerSec > 570) {
    divFactor = ADC_DIVIDER_6;
    sampleAndHold = ADC_PULSE_WIDTH_64;
  } else if (speedUstepsPerSec > 350) {
    divFactor = ADC_DIVIDER_6;
    sampleAndHold = ADC_PULSE_WIDTH_128;
  } else if (speedUstepsPerSec > 220) {
    divFactor = ADC_DIVIDER_7;
    sampleAndHold = ADC_PULSE_WIDTH_192;
  } else if (speedUstepsPerSec > 110) {
    divFactor = ADC_DIVIDER_7;
    preDivFactor = ADC_PREDIVIDER_4;
    sampleAndHold = ADC_PULSE_WIDTH_64;
  }*/

  divFactor = ADC_DIVIDER_1;
  sampleAndHold = ADC_PULSE_WIDTH_8;
  return adc_change_freq_and_sample_time(ADC_CLOCKSOURCE_ADCOSC,
                                        preDivFactor,
                                        divFactor,
                                        sampleAndHold);
}
ErrorStatus bemf_start_meas(adc_channel_e channel)
{
  ErrorStatus returnStatus = ERROR;
  DMA_setChannelDMATransfersRemaining(UDMA_PRI_SELECT | DMA_CH7_ADC14, MAXIMUM_BEMF_BUFFER);
  DMA_setChannelMode(UDMA_PRI_SELECT | DMA_CH7_ADC14 ,
                     UDMA_MODE_BASIC);
  if (adc_trigger_dma_conversion() == SUCCESS) {
    returnStatus = SUCCESS;

  }
  measReady = FALSE;
  //GPIO_setOutputHighOnPin(10,0x04);
  return returnStatus;
}

ErrorStatus bemf_stop_meas()
{
  adc_stop_meas();
  //GPIO_setOutputLowOnPin(10,0x04);
  measReady = TRUE;  // TODO(caer) Add to a DMA interrupt
  (void)bemf_get_mean_last_10();
  return SUCCESS;
}
uint32_t bemf_get_mean_last_10()
{
  uint16_t i = 0;
  uint32_t meas = 0;
  if (measReady) {
    i = (MAXIMUM_BEMF_BUFFER - DMA_getChannelDMATransfersRemaining(UDMA_PRI_SELECT | DMA_CH7_ADC14) - 5);
    if (i <= MAXIMUM_BEMF_BUFFER) { // TODO (caer) Remove. Check stop also
      uint16_t stop = i - 10;
     for (i; i > stop; i--) {
       meas += bemfMeasurments[i];
     }
     meas /= 10;
     bemfRunningMean[bemfRunningMeanCnt++] = meas/10;
     if (bemfRunningMeanCnt == RUNNING_MEAN_COUNT) {
       bemfRunningMeanCnt = 0;
     }
     measReady = FALSE; // Only read once
    }

    filteredBemf = filterConstant * (float)meas +
        (1-filterConstant) * prevBemfMeas;
    prevBemfMeas = meas;
  }
  return meas;
}

float bemf_get_filtered()
{
  return filteredBemf;
}

uint32_t bemf_get_last_meas()
{
  return prevBemfMeas;
}

uint32_t bemf_get_running_mean()
{
  uint8_t i = 0;
  uint32_t meas = 0;
  for (i = 0; i<RUNNING_MEAN_COUNT; ++i) {
    meas += bemfRunningMean[i];
  }

  return meas / RUNNING_MEAN_COUNT;
}

uint16_t* bemf_get_measarray_pnt()
{
  return bemfMeasurments;
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */

/*
 * End of file: BEMF_Sensor.c
 */
