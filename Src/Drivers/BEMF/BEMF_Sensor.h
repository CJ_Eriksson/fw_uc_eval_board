/**
 * @file BEMF_Sensor.h
 * @brief Sensor for measuring the back EMF differentially over a motor winding
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _BEMF_SENSOR_H_
#define _BEMF_SENSOR_H_

/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

#include "../ADC/ADC.h"
/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */
ErrorStatus bemf_init(adc_channel_e channel);
ErrorStatus bemf_set_conversion_rate(float speed);
ErrorStatus bemf_start_meas(adc_channel_e channel);
ErrorStatus bemf_stop_meas();
uint32_t bemf_get_mean_last_10();
float bemf_get_filtered();
uint32_t bemf_get_last_meas();
uint32_t  bemf_get_running_mean();
uint16_t* bemf_get_measarray_pnt();

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */

#ifdef __cplusplus
}
#endif

#endif /* _BEMF_SENSOR_H_ */

/*
 * End of file: BEMF_Sensor.h
 */
