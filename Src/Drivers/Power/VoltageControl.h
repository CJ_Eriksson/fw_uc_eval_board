/**
 * @file VoltageControl.h
 * @brief Driver for control an monitoring of the supply voltages to the
 * different IC:s
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _VOLTAGECONTROL_H_
#define _VOLTAGECONTROL_H_

#include "../../Common/common_types.h"
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */
typedef enum {
  VC_MOTOR_V_ENABLE,
  VC_MD1_POWER_ENABLE,
  VC_MD2_POWER_ENABLE,
  VC_MD3_POWER_ENABLE,
  VC_V_ANALOG_ENABLE,
  VC_SUPERCAP_V_BAT_ENABLE,
  VC_SUPERCAP_CHARGE_ENABLE
} voltage_control_v_enable_e;

typedef enum {
  VC_VIN_SENSE_ENABLE,
  VC_MOTOR_SENSE_ENABLE,
  VC_SUPERCAP_SENSE_ENABLE
} voltage_control_v_sense_enable_e;

typedef enum {
  VC_VIN_SENSE,
  VC_MOTOR_SENSE,
  VC_SUPERCAP_SENSE
} voltage_control_v_source_e;

/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus vc_init();
ErrorStatus vc_power_enable(voltage_control_v_enable_e device);
ErrorStatus vc_power_disable(voltage_control_v_enable_e device);
ErrorStatus vc_measure_enable(voltage_control_v_sense_enable_e device);
ErrorStatus vc_measure_disable(voltage_control_v_sense_enable_e device);
ErrorStatus vc_get_voltage_meas(voltage_control_v_source_e source,
                                uint16_t* data,
                                uint8_t num_conv);
#ifdef __cplusplus
}
#endif

#endif /* _VOLTAGECONTROL_H_ */

/*
 * End of file: VoltageControl.h
 */
