/**
 * @file SuperCap.h
 * @brief Driver for the Super Capacitor circuit
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _SUPERCAP_H_
#define _SUPERCAP_H_

/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */
#define SUPER_CAP_VOLTAGE_CHARGED_LEVEL 3000
#define MINIMUM_CAP_VOLTAGE_FOR_V_MOTOR 1200
/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */
typedef enum {
  SC_CHARGE_IDLE,
  SC_CHARGE_ENABLE,
  SC_CHARGE_MEASURE,
  SC_CHARGE_CHECK,
  SC_CHARGE_ERROR,
}sc_charge_sm_e;

typedef enum {
  SC_SUPER_CAP_VOLTAGE,
  SC_MOTOR_VOLTAGE
}sc_voltage_e;
/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus super_cap_init(uint_fast8_t v_motor_low_voltage_port,
                           uint_fast16_t v_motor_low_voltage_pin);
ErrorStatus super_cap_charge_start(void);
ErrorStatus super_cap_charge_sm(void);
ErrorStatus super_cap_get_voltage(sc_voltage_e source, uint16_t* const voltage_level);
ErrorStatus super_cap_enable_motor_power(void);
ErrorStatus super_cap_disable_motor_power(void);

void super_cap_isr(void);
void super_cap_isr_low_voltage_pin(void);

#ifdef __cplusplus
}
#endif

#endif /* _SUPERCAP_H_ */

/*
 * End of file: SuperCap.h
 */
