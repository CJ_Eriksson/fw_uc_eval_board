/**
 * @file SuperCap.c
 * @brief Driver for the Super Capacitor circuit
 * First start the super cap charging by do the init followed by the
 * super_cap_charge_start. This function starts a ADC meas with DMA transfer.
 * Then continuously poll the state machine that changes state according to the
 * measurement done for the cap voltage.
 *
 * @copyright
 *        Unpublished Work Copyright 2021 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Board/board.h"
#include "../Power/VoltageControl.h"
#include "../Time/Systime.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "SuperCap.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define SUPER_CAP_CHARGE_ADC_CONVERSIONS 10
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static gpio_s vm_low_voltage;
static sc_charge_sm_e sc_charge_current_state;
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief Init of super cap
 * @param [in] v_motor_low_voltage_port - GPIO port for motor low voltage
 *        [in] v_motor_low_voltage_pin -  GPIO pin for motor low voltage
 * @return None
 */
ErrorStatus super_cap_init(uint_fast8_t v_motor_low_voltage_port,
                           uint_fast16_t v_motor_low_voltage_pin)
{
  ErrorStatus returnStatus = ERROR;
  vm_low_voltage.port = v_motor_low_voltage_port;
  vm_low_voltage.pin = v_motor_low_voltage_pin;

  return returnStatus;
}

/**
 * @brief Start the charging of the super capacitors
 * @param [in] -
 * @return SUCCESS on SUCCESS
 */
ErrorStatus super_cap_charge_start(void) {
  ErrorStatus returnStatus = ERROR;
  if (vc_get_power_status(VC_MOTOR_V_ENABLE) == FALSE) {  // TODO(caer) Don't charge if motor power is on????
    if (sc_charge_current_state == SC_CHARGE_IDLE) {
      if (vc_power_enable(VC_SUPERCAP_V_BAT_ENABLE) == SUCCESS) {
        if (vc_power_enable(VC_SUPERCAP_CHARGE_ENABLE) == SUCCESS) {
            sc_charge_current_state = SC_CHARGE_ENABLE;
            // TODO(caer) Enable of timer interrupt in timer.c Leave it continuously running for now
            returnStatus = SUCCESS;
          }
       }
    }
  }

  return returnStatus;
}
/**
 * @brief Enable super cap power to the motor voltage DC/DC. If voltage too low,
 *        the function returns error
 * @param [in] -
 * @return SUCCESS on SUCCESS
 */
ErrorStatus super_cap_enable_motor_power(void)
{
  ErrorStatus returnStatus = ERROR;
  uint16_t capVoltage = 0;
  if (super_cap_get_voltage(SC_SUPER_CAP_VOLTAGE, &capVoltage) == SUCCESS) {
    if (capVoltage >= MINIMUM_CAP_VOLTAGE_FOR_V_MOTOR) {
      returnStatus = vc_power_enable(VC_MOTOR_V_ENABLE);
    }
  }

  return returnStatus;
}

/**
 * @brief Disable super cap power to the motor voltage DC/DC.
 * @param [in] -
 * @return SUCCESS on SUCCESS
 */
ErrorStatus super_cap_disable_motor_power(void)
{
  ErrorStatus returnStatus = ERROR;
  returnStatus = vc_power_disable(VC_MOTOR_V_ENABLE);
  return returnStatus;
}
/**
 * @brief Start charging of super capacitor. The charging is disabled
 *        automatically when capacitor voltage has reached
 *        SUPER_CAP_VOLTAGE_CHARGED_LEVEL. Shall be continuously polled in main.
 *        State transition for meas is handled in DMA ADC ISR
 * @param [in] -
 * @return SUCCESS on SUCCESS
 */
ErrorStatus super_cap_charge_sm(void)
{
  ErrorStatus returnStatus = SUCCESS;
  uint16_t data[SUPER_CAP_CHARGE_ADC_CONVERSIONS];
  uint8_t i = 0;
  uint32_t measdata = 0;
  ErrorStatus result;
  switch (sc_charge_current_state) {
    case SC_CHARGE_IDLE:
      break;
    case SC_CHARGE_ENABLE:
      break;
    case SC_CHARGE_MEASURE:
      result = vc_get_voltage_meas(VC_SUPERCAP_SENSE,
                                               data,
                                               SUPER_CAP_CHARGE_ADC_CONVERSIONS);
      if (result == SUCCESS) {
        sc_charge_current_state = SC_CHARGE_CHECK;
      } else if (result == ERROR) {
        sc_charge_current_state = SC_CHARGE_ERROR;
      }
      // If busy remain in state
      break;
    case SC_CHARGE_CHECK:
      for (i = 0; i < SUPER_CAP_CHARGE_ADC_CONVERSIONS; ++i) {
        measdata += data[i];
      }
      if (((float)measdata / SUPER_CAP_CHARGE_ADC_CONVERSIONS) >=
          SUPER_CAP_VOLTAGE_CHARGED_LEVEL) {
        sc_charge_current_state = SC_CHARGE_IDLE; // Capacitor charged
        vc_power_disable(VC_SUPERCAP_CHARGE_ENABLE);
        vc_power_disable(VC_SUPERCAP_V_BAT_ENABLE);
      } else { // Capacitor needs more charge
        sc_charge_current_state = SC_CHARGE_ENABLE;
      }
      vc_measure_disable(VC_SUPERCAP_SENSE_ENABLE);
      break;
    case SC_CHARGE_ERROR:
      vc_power_disable(VC_SUPERCAP_CHARGE_ENABLE);
      vc_power_disable(VC_SUPERCAP_V_BAT_ENABLE);
      vc_measure_disable(VC_SUPERCAP_SENSE_ENABLE);
      sc_charge_current_state = SC_CHARGE_IDLE;
      returnStatus = ERROR;
      break;
  default:
    break;
  }
  return returnStatus;
}

/**
 * @brief Get current charge of super cap or motor voltage .
 *        The call is a blocking ADC conversion.
 * @param [in] source - Voltage to be measured
 *        [out] voltage_level - Pointer for voltage level data
 * @return SUCCESS on SUCCESS
 */
ErrorStatus super_cap_get_voltage(sc_voltage_e source, uint16_t* const voltage_level)
{
  ErrorStatus returnStatus = ERROR;
  voltage_control_v_sense_enable_e device;
  voltage_control_v_source_e sense;
  switch (source) {
    case SC_SUPER_CAP_VOLTAGE:
      device = VC_SUPERCAP_SENSE_ENABLE;
      sense = VC_SUPERCAP_SENSE;
      break;
    case SC_MOTOR_VOLTAGE:
      device = VC_MOTOR_SENSE_ENABLE;  // This enables the motor output
      sense = VC_MOTOR_SENSE;
      break;
    default:
      break;
  }
  vc_measure_enable(device);
  uint32_t tickstart = systime_get_tick();
  uint16_t data[SUPER_CAP_CHARGE_ADC_CONVERSIONS];
  do {
    returnStatus = vc_get_voltage_meas(sense,
                                       data,
                                       SUPER_CAP_CHARGE_ADC_CONVERSIONS);
    if (returnStatus == SUCCESS) {
      break;
    }
  } while(systime_get_tick() != (tickstart + 3));
  vc_measure_disable(device);
  if (returnStatus == SUCCESS) {
    uint32_t measdata = 0;
    uint8_t i = 0;
    for (i = 0; i < SUPER_CAP_CHARGE_ADC_CONVERSIONS; ++i) {
      measdata += data[i];
    }
    *voltage_level = (uint16_t)((float)measdata / SUPER_CAP_CHARGE_ADC_CONVERSIONS);
  }
  return returnStatus;
}

/**
 * @brief ISR for DMA ADC meas ready
 * @param [in] -
 * @return None
 */
void super_cap_isr(void)
{
  if (sc_charge_current_state == SC_CHARGE_ENABLE) {
    vc_measure_enable(VC_SUPERCAP_SENSE_ENABLE);

    sc_charge_current_state = SC_CHARGE_MEASURE;

  }
}
/**
 * @brief ISR for pin interrupt from DC/DC that senses too low input voltage
 * @param [in] -
 * @return None
 */
void super_cap_isr_low_voltage_pin(void)
{
  // This interrupt shall be preceded by other isr that stops motors etc before
  // power is turned off. For now in simple HW test the motor power is turned
  // off in this ISR
  super_cap_disable_motor_power();
}

/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */

/*
 * End of file: SuperCap.c
 */
