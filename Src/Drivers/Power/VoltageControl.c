/**
 * @file VoltageControl.c
 * @brief Driver for control an monitoring of the supply voltages to the
 * different IC:s
 *
 * @copyright
 *        Unpublished Work Copyright 2021 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Board/board.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "VoltageControl.h"
#include "../ADC/adc.h"
#include "../../HAL/gpio.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define VOLTAGE_CONVERSION_FACTOR  ADC_REFERENCE_VOLTAGE/0x4000 // ADC uses 14 bit resolution
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static gpio_s motorVoltEnable;
static gpio_s md1PowerEnable;
static gpio_s md2PowerEnable;
static gpio_s md3PowerEnable;
static gpio_s analogVoltEnable;
static gpio_s supercapVbatEnable;
static gpio_s supercapChargeEnable;
static gpio_s vinSenseEnable;
static gpio_s supercapVsenseEnable;
static gpio_s vinSense;
static gpio_s motorSense;
static gpio_s supercapSense;
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief Initialize power control GPIO:s
 * @param [in] All GPIO:s for power control
 * @return SUCCESS on SUCCESS
 */
ErrorStatus vc_init(uint_fast8_t motor_v_en_port, uint_fast16_t motor_v_en_pin,
                    uint_fast8_t md1_p_en_port, uint_fast16_t md1_p_en_pin,
                    uint_fast8_t md2_p_en_port, uint_fast16_t md2_p_en_pin,
                    uint_fast8_t md3_p_en_port, uint_fast16_t md3_p_en_pin,
                    uint_fast8_t anlog_v_en_port, uint_fast16_t analog_v_en_pin,
                    uint_fast8_t sc_v_en_port, uint_fast16_t sc_v_en_pin,
                    uint_fast8_t sc_ch_en_port, uint_fast16_t sc_ch_en_pin,
                    uint_fast8_t v_sense_en_port, uint_fast16_t v_sense_en_pin,
                    uint_fast8_t sc_sen_en_port, uint_fast16_t sc_sen_en_pin)
{
  motorVoltEnable.port = motor_v_en_port;
  md1PowerEnable.port = md1_p_en_port;
  md2PowerEnable.port = md2_p_en_port;
  md3PowerEnable.port = md3_p_en_port;
  analogVoltEnable.port = anlog_v_en_port;
  supercapVbatEnable.port = sc_v_en_port;
  supercapChargeEnable.port = sc_ch_en_port;
  vinSenseEnable.port = v_sense_en_port;
  supercapVsenseEnable.port = sc_sen_en_port;

  motorVoltEnable.pin = motor_v_en_pin;
  md1PowerEnable.pin = md1_p_en_pin;
  md2PowerEnable.pin = md2_p_en_pin;
  md3PowerEnable.pin = md3_p_en_pin;
  analogVoltEnable.pin = analog_v_en_pin;
  supercapVbatEnable.pin = sc_v_en_pin;
  supercapChargeEnable.pin = sc_ch_en_pin;
  vinSenseEnable.pin = v_sense_en_pin;
  supercapVsenseEnable.pin = sc_sen_en_pin;
  return SUCCESS;
}

/**
 * @brief Enable power for different periferals
 * @param [in] device - periferal to enable
 * @return SUCCESS on SUCCESS
 */
ErrorStatus vc_power_enable(voltage_control_v_enable_e device)
{
  ErrorStatus returnStatus = SUCCESS;
  switch (device) {
    case VC_MOTOR_V_ENABLE:
      GPIO_setOutputHighOnPin(motorVoltEnable.port, motorVoltEnable.pin);
      break;
    case VC_MD1_POWER_ENABLE:
      GPIO_setOutputHighOnPin(md1PowerEnable.port, md1PowerEnable.pin);
      break;
    case VC_MD2_POWER_ENABLE:
      GPIO_setOutputHighOnPin(md2PowerEnable.port, md2PowerEnable.pin);
      break;
    case VC_MD3_POWER_ENABLE:
      GPIO_setOutputHighOnPin(md3PowerEnable.port, md3PowerEnable.pin);
      break;
    case VC_V_ANALOG_ENABLE:
      GPIO_setOutputHighOnPin(analogVoltEnable.port, analogVoltEnable.pin);
      break;
    case VC_SUPERCAP_V_BAT_ENABLE:
      GPIO_setOutputHighOnPin(supercapVbatEnable.port, supercapVbatEnable.pin);
      break;
    case VC_SUPERCAP_CHARGE_ENABLE:
      GPIO_setOutputHighOnPin(supercapChargeEnable.port,
                              supercapChargeEnable.pin);
      break;
    default:
      returnStatus = ERROR;
      break;
  }
  return returnStatus;

}

/**
 * @brief Disable power for different peripherals
 * @param [in] device - device to control
 * @return SUCCESS on SUCCESS
 */
ErrorStatus vc_power_disable(voltage_control_v_enable_e device)
{
  ErrorStatus returnStatus = SUCCESS;
  switch (device) {
    case VC_MOTOR_V_ENABLE:
      GPIO_setOutputLowOnPin(motorVoltEnable.port, motorVoltEnable.pin);
      break;
    case VC_MD1_POWER_ENABLE:
      GPIO_setOutputLowOnPin(md1PowerEnable.port, md1PowerEnable.pin);
      break;
    case VC_MD2_POWER_ENABLE:
      GPIO_setOutputLowOnPin(md2PowerEnable.port, md2PowerEnable.pin);
      break;
    case VC_MD3_POWER_ENABLE:
      GPIO_setOutputLowOnPin(md3PowerEnable.port, md3PowerEnable.pin);
      break;
    case VC_V_ANALOG_ENABLE:
      GPIO_setOutputLowOnPin(analogVoltEnable.port, analogVoltEnable.pin);
      break;
    case VC_SUPERCAP_V_BAT_ENABLE:
      GPIO_setOutputLowOnPin(supercapVbatEnable.port, supercapVbatEnable.pin);
      break;
    case VC_SUPERCAP_CHARGE_ENABLE:
      GPIO_setOutputLowOnPin(supercapChargeEnable.port,
                              supercapChargeEnable.pin);
      break;
    default:
      returnStatus = ERROR;
      break;
  }
  return returnStatus;
}

/**
 * @brief Enable voltage divider for measurement of voltage level for selected
 *        source.
 *        NOTE! The motor voltage is turned on when using the
 *        VC_MOTOR_SENSE_ENABLE
 * @param [in] device - device to control
 * @return SUCCESS on SUCCESS
 */
ErrorStatus vc_measure_enable(voltage_control_v_sense_enable_e device)
{
  ErrorStatus returnStatus = SUCCESS;
  switch (device) {
    case VC_VIN_SENSE_ENABLE:
      GPIO_setOutputHighOnPin(vinSenseEnable.port, vinSenseEnable.pin);
      break;
    case VC_SUPERCAP_SENSE_ENABLE:
      GPIO_setOutputHighOnPin(supercapVsenseEnable.port,
                             supercapVsenseEnable.pin);
      break;
    case VC_MOTOR_SENSE_ENABLE:
      GPIO_setOutputHighOnPin(motorVoltEnable.port, motorVoltEnable.pin);
      break;
    default:
      returnStatus = ERROR;
      break;
  }
  return returnStatus;
}

/**
 * @brief Disable voltage divider for measurement of voltage level for selected
 *        source.
 *        NOTE! The motor voltage is turned off when using the
 *        VC_MOTOR_SENSE_ENABLE
 * @param [in] device - device to control
 * @return SUCCESS on SUCCESS
 */
ErrorStatus vc_measure_disable(voltage_control_v_sense_enable_e device)
{
  ErrorStatus returnStatus = SUCCESS;
  switch (device) {
    case VC_VIN_SENSE_ENABLE:
      GPIO_setOutputLowOnPin(vinSenseEnable.port, vinSenseEnable.pin);
      break;
    case VC_SUPERCAP_SENSE_ENABLE:
      GPIO_setOutputLowOnPin(supercapVsenseEnable.port,
                             supercapVsenseEnable.pin);
      break;
    case VC_MOTOR_SENSE_ENABLE:
      GPIO_setOutputLowOnPin(motorVoltEnable.port, motorVoltEnable.pin);
      break;
    default:
      returnStatus = ERROR;
      break;
  }
  return returnStatus;
}

bool_t vc_get_power_status(voltage_control_v_enable_e device)
{
  uint8_t pinValue;
  switch (device) {
    case VC_MOTOR_V_ENABLE:
      pinValue = GPIO_getOutputPinValue(motorVoltEnable.port,
                                        motorVoltEnable.pin);
      break;
    case VC_MD1_POWER_ENABLE:
      pinValue = GPIO_getOutputPinValue(md1PowerEnable.port,
                                        md1PowerEnable.pin);
      break;
    case VC_MD2_POWER_ENABLE:
      pinValue = GPIO_getOutputPinValue(md2PowerEnable.port,
                                        md2PowerEnable.pin);
      break;
    case VC_MD3_POWER_ENABLE:
      pinValue = GPIO_getOutputPinValue(md3PowerEnable.port,
                                        md3PowerEnable.pin);
      break;
    case VC_V_ANALOG_ENABLE:
      pinValue = GPIO_getOutputPinValue(analogVoltEnable.port,
                                        analogVoltEnable.pin);
      break;
    case VC_SUPERCAP_V_BAT_ENABLE:
      pinValue = GPIO_getOutputPinValue(supercapVbatEnable.port,
                                        supercapVbatEnable.pin);
      break;
    case VC_SUPERCAP_CHARGE_ENABLE:
      pinValue = GPIO_getOutputPinValue(supercapChargeEnable.port,
                                        supercapChargeEnable.pin);
      break;
    default:
      break;
  }
  return (pinValue == GPIO_INPUT_PIN_HIGH ? TRUE:FALSE);
}
/**
 * @brief Get motor voltage measurement. A function call to this function
 *        starts a measurement of the selected source. The function returns busy
 *        until the data array is filled, the ADC measurement of that source is
 *        completed. Best way is to poll this function until it returns SUCCESS.
 *        Then the measurement is completed.
 *
 * @param [in] source - source to measure
 *             data - pointer to data array to store the results
 *             num_conv - number of conversions
 * @return SUCCESS on SUCCESS, BUSY during ongoing measurement
 */
ErrorStatus vc_get_voltage_meas(voltage_control_v_source_e source,
                                uint16_t* data,
                                uint8_t num_conv)  // TODO(caer) Add variable to check what channel is measured, here or in ADC to prevent multi calls for start of ADC conversion and to know what channel the data is from
{
  ErrorStatus returnStatus = ERROR;
  static bool_t busy = FALSE;
  if (!busy) {
    switch (source) {
      case VC_VIN_SENSE:
        if (adc_start_meas(A12, TRUE, num_conv, data) == SUCCESS) {
          busy = TRUE;
          returnStatus = BUSY;
        }
        break;
      case VC_MOTOR_SENSE:
        if (adc_start_meas(A7, TRUE, num_conv, data) == SUCCESS) {
          busy = TRUE;
          returnStatus = BUSY;
        }
        break;
      case VC_SUPERCAP_SENSE:
        if (adc_start_meas(A6, TRUE, num_conv, data) == SUCCESS) {
          busy = TRUE;
          returnStatus = BUSY;
        }
        break;
      default:
        returnStatus = ERROR;
        break;
    }
  } else {
    if (adc_conversion_complete()) {
      uint8_t i = 0;
      for (i = 0; i < num_conv; ++i) {
        data[i] = (uint16_t)(((float)data[i]) * VOLTAGE_CONVERSION_FACTOR);
      }
      busy = FALSE;
      returnStatus = SUCCESS;
    }
  }

  return returnStatus;
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */

/*
 * End of file: VoltageControl.c
 */
