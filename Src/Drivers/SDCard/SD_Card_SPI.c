/**
 * @file sd_card_spi.c
 * @brief Commands and communication via SPI for the SD-card.
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Fatfs/ff.h"         /* Obtains integer types */ // REMOVE AFTER IMP
#include "../../Common/common_types.h"
#include "../../Board/board.h"
#include "../Time/Systime.h"
#include "../../Fatfs/diskio.h"
#include "../../Hal/dma.h"
#include "SD_Card_SPI.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define CMD0                0
#define CMD0_ARG            0x00000000
#define CMD2                2
#define CMD10               10
#define CMD8                8
#define CMD8_CHECK_PATTERN  0xAA
#define CMD8_ARG            0x000000100 | CMD8_CHECK_PATTERN  // 2.7 to 3.6V
#define CMD12               12
#define ACMD13  (0x80+13) /* SD_STATUS (SDC) */
#define CMD32               32
#define CMD32_ARG           0
#define CMD55               55
#define CMD55_ARG           0
#define CMD59               59
#define CMD59_ARG           0x00000001
#define CMD58               58
#define CMD58_ARG           0x00000000
#define ACMD41              41 | 0x80
#define ACMD41_ARG          0x40000000
#define ACMD23              23 | 0x80
#define CMD13                  13
#define CMD9                  9
#define CMD17                   17
#define CMD18                   18
#define CMD24                   24
#define CMD24_ARG               0x00
#define CMD25                   25
#define SD_MAX_READ_ATTEMPTS    120
#define SD_MAX_WRITE_ATTEMPTS   120
#define VOL_ACC(X)          (X & 0x1F)
#define VOLTAGE_ACC_27_33   0b00000001
#define VOLTAGE_ACC_LOW     0b00000010
#define VOLTAGE_ACC_RES1    0b00000100
#define VOLTAGE_ACC_RES2    0b00001000

#define SD_START_TOKEN          0xFE
#define SD_START_TOKEN_MULTI_WRITE 0xFC
#define SD_STOP_TRAN 0xFD;
#define SD_BLOCK_LEN        512
#define CRC16_LENGTH         2


/* MMC card type flags (MMC_GET_TYPE) */
#define CT_MMC    0x01    /* MMC ver 3 */
#define CT_SD1    0x02    /* SD ver 1 */
#define CT_SD2    0x04    /* SD ver 2 */
#define CT_SDC    (CT_SD1|CT_SD2) /* SD */
#define CT_BLOCK  0x08    /* Block addressing */

/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
spi_s* spi = NULL;
static gpio_s cs;
static bool_t sd_card_powered_up = FALSE;
static uint8_t CRC_table[256] = {0};
static uint8_t dummy = 0xFF;
static uint8_t CardType = 0;
static uint16_t sdCardSupplyVoltage;
DSTATUS Stat = STA_NOINIT;  /* Physical drive status */
static uint32_t dmaRxChannel;
static uint32_t dmaTxChannel;
static bool_t sd_size_sdsc;
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */

ErrorStatus sd_read_response(sd_response_type type, uint8_t* res);

ErrorStatus read_r1_respond(uint8_t* res);
ErrorStatus read_one_byte(uint8_t* res);
static ErrorStatus rcvr_datablock (uint8_t *buff, uint16_t btr);
static ErrorStatus read_card_status(uint16_t* status);
ErrorStatus chip_select(bool_t set);
void power_up();
ErrorStatus send_command(uint8_t cmd, uint32_t arg, uint8_t* r1_response);
ErrorStatus read_command_response(uint8_t cmd, uint8_t* response);
void generate_crc7_table();
uint8_t crc7(uint8_t* message, uint8_t nr_of_bytes);
uint16_t crc16_one(uint16_t crc_in, uint8_t data);
uint16_t crc16_buf(const uint8_t * p_buf, uint16_t len);
ErrorStatus check_voltage(uint16_t supply_voltage, uint16_t ocr_accepted_voltages);
static ErrorStatus wait_ready (uint8_t wt);
static ErrorStatus xchg_spi(uint8_t* tx_data, uint8_t* rx_data, uint16_t nr_of_bytes);



/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
ErrorStatus sd_spi_init(spi_s* const spi_p,
                    uint_fast8_t cs_port,
                    uint_fast16_t cs_pin,
                    uint16_t sd_card_supply_voltage,
                    uint32_t dma_rx_channel,
                    uint32_t dma_tx_channel)
{
  spi = spi_p;
  ErrorStatus returnStatus = ERROR;
  if(spi_p == NULL) {
    return returnStatus;
  }
  dmaRxChannel = dma_rx_channel;
  dmaTxChannel = dma_tx_channel;
  cs.port = cs_port;
  cs.pin = cs_pin;

  generate_crc7_table();
  cs_port = cs_port;
  cs_pin = cs_pin;

  sdCardSupplyVoltage = sd_card_supply_voltage;

  // DMA Receive init
  DMA_disableChannelAttribute(dmaRxChannel ,
                              UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST |
                              UDMA_ATTR_HIGH_PRIORITY |
                              UDMA_ATTR_REQMASK);
  // Setting Control Indexes.
  DMA_setChannelControl(UDMA_PRI_SELECT | dmaRxChannel ,
                        UDMA_SIZE_8 |
                        UDMA_SRC_INC_NONE |
                        UDMA_DST_INC_8 |
                        UDMA_ARB_1);

  DMA_assignChannel(dmaRxChannel);



  // DMA Transmitt init
  DMA_disableChannelAttribute(dmaTxChannel ,
                              UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST |
                              UDMA_ATTR_HIGH_PRIORITY |
                              UDMA_ATTR_REQMASK);
  // Setting Control Indexes.
  DMA_setChannelControl(UDMA_PRI_SELECT | dmaTxChannel ,
                        UDMA_SIZE_8 |
                        UDMA_SRC_INC_8 |
                        UDMA_DST_INC_NONE |
                        UDMA_ARB_1);

  DMA_assignChannel(dmaTxChannel);

  power_up();
}

DSTATUS sd_spi_initialize(uint8_t drv)
{
  SPI_changeMasterClock(spi->module,
                        SystemCoreClock, 80000);
  ErrorStatus returnStatusTemp = ERROR;
  if (drv != 0) return STA_NOINIT;    /* Supports only drive 0 */
  if (chip_select(TRUE) == SUCCESS) {
    uint8_t r1_response = 0xff;
     uint8_t response[4] = {0};
     // Send command 0, reset
     if (send_command(CMD0, CMD0_ARG, &r1_response) == SUCCESS) {
       if (r1_response <= SD_IN_IDLE) {  // SD-card in idle state
         returnStatusTemp = SUCCESS;
       }
     }
     // Send Command 8, check operation conditions
     if (returnStatusTemp == SUCCESS) {
       systime_wait_ms(1000);
       returnStatusTemp = ERROR;
       if (send_command(CMD8, CMD8_ARG, &r1_response) == SUCCESS) {
         if (read_command_response(CMD8, response) == SUCCESS) {
           if (r1_response <= SD_IN_IDLE) {  // SD-card still in idle state
             if (response[3] == CMD8_CHECK_PATTERN &&  // Voltage range is within 3.3V as the device requires
                 (response[2] & 0x1F) == VOLTAGE_ACC_27_33) {
               returnStatusTemp = SUCCESS;
             }
           }
         }
       }
     }

     // Send Command 59 to enable CRC
     systime_wait_ms(1);  // Don't know why this is needed. If CMD59 is not sent the delay is not needed...
     if (returnStatusTemp == SUCCESS) {
       returnStatusTemp = ERROR;
       if (send_command(CMD59, CMD59_ARG, &r1_response) == SUCCESS) {
         if (r1_response <= SD_IN_IDLE) {  // SD-card still in idle state
           returnStatusTemp = SUCCESS;
         }
       }
     }
     // Send Command 58, Operation conditions register
     if (returnStatusTemp == SUCCESS) {
       returnStatusTemp = ERROR;
       if (send_command(CMD58, CMD58_ARG, &r1_response) == SUCCESS) {
         if (read_command_response(CMD58, response) == SUCCESS) {
           if (check_voltage(sdCardSupplyVoltage,
                             (uint16_t)((response[1] << 8) | (response[2] & 0x80))) == 0) {  //
             returnStatusTemp = SUCCESS;
           }
         }
       }
     }

     // Send Command ACMD41, send operation condition (start card initialization)
     if (returnStatusTemp == SUCCESS) {
       returnStatusTemp = ERROR;
       uint16_t retries = 120;
       do {
         if (send_command(ACMD41, ACMD41_ARG, &r1_response) == SUCCESS) {
           if ((r1_response & 0xFE) == 0) {  // No error
             returnStatusTemp = SUCCESS;
             if ((r1_response & SD_IN_IDLE) == 0) {
               break;
             }
           }
         }
         systime_wait_ms(5);
       } while((returnStatusTemp == SUCCESS) && (--retries > 0));
       if (retries <= 0) {
         returnStatusTemp = ERROR;
       }
     }
     //Send Command 58
     if (returnStatusTemp == SUCCESS) {
       returnStatusTemp = ERROR;
       if (send_command(CMD58, CMD58_ARG, &r1_response) == SUCCESS) {
         if (read_command_response(CMD58, response) == SUCCESS) {
           if ((response[0] & 0x80) != 0) {  // Check if moved out of idle state
             sd_card_powered_up = TRUE;
             returnStatusTemp = SUCCESS;
             CardType = (response[0] & 0x40) ? CT_SD2 | CT_BLOCK : CT_SD2;  /* Card id SDv2 */
             Stat &= ~STA_NOINIT;
             if ((response[0] & 0x40) != 0) {
               sd_size_sdsc = TRUE;
             }
           }
         }
       }
     }
  }

  chip_select(FALSE);
  SPI_changeMasterClock(spi->module,
                        SystemCoreClock, spi->speed);
  return Stat;
}

DSTATUS sd_spi_status (
  BYTE drv    /* Physical drive number (1) */
)
{
  if (drv != 1) return STA_NOINIT;   /* Supports only drive 1 */

  return Stat;  /* Return disk status */
}

DRESULT sd_spi_read(uint32_t addr, uint8_t *buff, uint16_t count)
{
  DRESULT returnStatus = RES_ERROR;
  if (buff == NULL || count == 0) {
    return RES_PARERR;  /* Check parameter */
  }
  if (chip_select(TRUE) == SUCCESS) {
    uint8_t read;
    uint16_t readAttempts;
    uint8_t r1_response = 0xFF;
    uint8_t command = count == 1 ? CMD17 : CMD18;
      // send CMD17
    if (send_command(command, addr,  &r1_response) == SUCCESS) {
      // if response received from card
      if ((r1_response & 0xFE) == 0) {  // No error
        // wait for a response token (timeout = 100ms)
        readAttempts = 0;
        while(++readAttempts != SD_MAX_READ_ATTEMPTS) {
          xchg_spi(NULL, &read, 1);
          if(read != 0xFF) { // Wait for response from SD card
            break;
          }
        }
      }
    }
    if (read == 0xFE) {
      do {
        if (xchg_spi(NULL, buff, SD_BLOCK_LEN + CRC16_LENGTH) == SUCCESS) {
         uint16_t crc16 = crc16_buf(buff, SD_BLOCK_LEN);
         if ((((uint16_t)buff[SD_BLOCK_LEN] << 8)+ (uint16_t)buff[SD_BLOCK_LEN + 1]) == crc16) {
           returnStatus = RES_OK;
         } else {
           returnStatus = RES_ERROR;
           break;
         }
       } else {
         returnStatus = RES_ERROR;
         break;
       }
      } while(--count);
    } else {
      returnStatus = RES_ERROR;
    }
  } else {
    returnStatus = RES_ERROR;
  }
  chip_select(FALSE);
  return returnStatus;
}

DRESULT sd_spi_write(uint32_t addr, uint8_t *buff, uint16_t count)
{
  DRESULT returnStatus = RES_ERROR;
  if (buff == NULL || count == 0) {
    return RES_PARERR;  /* Check parameter */
  }

  //if (((uint8_t)Stat & STA_NOINIT) != 0) return RES_NOTRDY; /* Check drive status */
  //if (((uint8_t)Stat & STA_PROTECT) != 0) return RES_WRPRT; /* Check write protect */

  if (chip_select(TRUE) == SUCCESS) {
    uint8_t readAttempts = 0;
    uint8_t read;
    uint8_t r1_response = 0xFF;
    uint8_t token = count == 1 ? SD_START_TOKEN : SD_START_TOKEN_MULTI_WRITE;
    uint8_t command = count == 1 ? CMD24 : CMD25;
    // send CMD24, single write
    if (command == CMD25) {
      send_command(ACMD23, count, &r1_response);
    }
    if (send_command(command, addr, &r1_response) == SUCCESS) {
      if(r1_response != 0xFF) {
        for (count; count > 0 ; count--) {
          // send start token
          if (xchg_spi(&token, NULL, 1) != SUCCESS) {
           return returnStatus;
          }
          // write buffer to card
          if (xchg_spi(buff, NULL, SD_BLOCK_LEN) != SUCCESS) {
           return returnStatus;
          }
          // Write CRC16 to card
          uint16_t crc16 = crc16_buf(buff, SD_BLOCK_LEN);
          uint8_t crc16_buffer[2] = {(uint8_t)(crc16 >> 8), (uint8_t)(0xFF & crc16)};
          if (xchg_spi(crc16_buffer, NULL, CRC16_LENGTH) != SUCCESS) {
           return returnStatus;
          }
          //////////// IS THIS AFTER EACH BYTE????///////////////////
          // wait for a response from SD card of the data transmitted
          readAttempts = 0;
          while(++readAttempts != SD_MAX_READ_ATTEMPTS) {
            xchg_spi(NULL, &read, 1);
           if(read != 0xFF) {
             break;
           }
           systime_wait_ms(1);
          }
          // if data accepted
          if((read & 0x1F) == 0x05) {
            // wait for write to finish (timeout = 250ms)
            readAttempts = 0;
            while(++readAttempts != SD_MAX_READ_ATTEMPTS) {
              xchg_spi(NULL, &read, 1);
              if(read !=0x00) {  // Data line is held low by SD-card until write is finished
                // Don't know why this additional byte send is needed,
                // SD card don't respond correct to CMD after write if this is
                // not here...
                uint8_t i = 0;
                for (i ;i<10;++i) {
                  xchg_spi(&dummy, NULL, 1);
                }
                uint16_t status;
                if (read_card_status(&status) == SUCCESS) {
                  if (status == 0) {
                    returnStatus = RES_OK;
                  }
                }
                break;
              }
              systime_wait_ms(1);
            }
          }
        }
        if (command == CMD25 && returnStatus == RES_OK) {  // Send stop transmission if multi sector write
          token = SD_STOP_TRAN;
          if (xchg_spi(&token, NULL, 1) != SUCCESS) {
            returnStatus = RES_OK;
          } else {
            returnStatus = RES_ERROR;
          }
        }
      }
    }
  }
  chip_select(FALSE);
  return returnStatus;
}

DRESULT sd_spi_ioctl (
  uint8_t cmd,   /* Control command code */
  void *buff    /* Pointer to the control data */
)
{
  DRESULT returnStatus;
  uint8_t n, csd[16];
  uint32_t *dp, st, ed, csize;
  uint8_t r1_repsonse;
  if (Stat & STA_NOINIT) return RES_NOTRDY; /* Check if drive is ready */

  returnStatus = RES_ERROR;

  switch (cmd) {
  case CTRL_SYNC :    /* Wait for end of internal write process of the drive */
    if (chip_select(TRUE) == SUCCESS) {
      returnStatus = RES_OK;
    }
    break;

  case GET_SECTOR_COUNT : /* Get drive capacity in unit of sector (DWORD) */
    if ((send_command(CMD9, 0, &r1_repsonse) == SUCCESS) && rcvr_datablock(csd, 16)) {
      if ((csd[0] >> 6) == 1) { /* SDC ver 2.00 */
        csize = csd[9] + ((uint16_t)csd[8] << 8) + ((uint32_t)(csd[7] & 63) << 16) + 1;
        *(uint32_t*)buff = csize << 10;
      } else {          /* SDC ver 1.XX or MMC ver 3 */
        n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
        csize = (csd[8] >> 6) + ((WORD)csd[7] << 2) + ((WORD)(csd[6] & 3) << 10) + 1;
        *(uint32_t*)buff = csize << (n - 9);
      }
      returnStatus = RES_OK;
    }
    break;

  case GET_BLOCK_SIZE : /* Get erase block size in unit of sector (DWORD) */
    //if (CardType & CT_SD2) {  /* SDC ver 2.00 */
    if (send_command(ACMD13, 0, &r1_repsonse) == 0) { /* Read SD status */
      xchg_spi(dummy, NULL, 1);
      if (rcvr_datablock(csd, 16)) {        /* Read partial block */
        for (n = 64 - 16; n; n--) xchg_spi(dummy, NULL, 1); /* Purge trailing data */
        *(uint32_t*)buff = 16UL << (csd[10] >> 4);
        returnStatus = RES_OK;
      }
    }
   // } else {          /* SDC ver 1.XX or MMC */
   //   if ((send_cmd(CMD9, 0) == 0) && rcvr_datablock(csd, 16)) {  /* Read CSD */
   //     if (CardType & CT_SD1) {  /* SDC ver 1.XX */
   //       *(DWORD*)buff = (((csd[10] & 63) << 1) + ((WORD)(csd[11] & 128) >> 7) + 1) << ((csd[13] >> 6) - 1);
   //     } else {          /* MMC */
   //       *(DWORD*)buff = ((WORD)((csd[10] & 124) >> 2) + 1) * (((csd[11] & 3) << 3) + ((csd[11] & 224) >> 5) + 1);
   //     }
   //     returnStatus = RES_OK;
   //   }
   // }*/
    break;

  //case CTRL_TRIM :  /* Erase a block of sectors (used when _USE_ERASE == 1) */
    //if (!(CardType & CT_SDC)) break;        /* Check if the card is SDC */
  //  if (USER_SPI_ioctl(MMC_GET_CSD, csd)) break; /* Get CSD */
  // if (!(csd[0] >> 6) && !(csd[10] & 0x40)) break; /* Check if sector erase can be applied to the card */
  //  dp = buff; st = dp[0]; ed = dp[1];        /* Load sector block */
  //  if (!(CardType & CT_BLOCK)) {
  //    st *= 512; ed *= 512;
  //  }
  //  if (send_cmd(CMD32, st) == 0 && send_cmd(CMD33, ed) == 0 && send_cmd(CMD38, 0) == 0 && wait_ready(30000)) { /* Erase sector block */
  //    returnStatus = RES_OK; /* FatFs does not check result of this command */
  // }
  //  break;

  default:
    returnStatus = RES_PARERR;
  }

  chip_select(FALSE);

  return returnStatus;
}

DRESULT sd_spi_erase_single_block(uint32_t addr)
{
  DRESULT returnStatus = RES_ERROR;
  chip_select(TRUE);
  uint8_t r1_response;
  if (send_command(CMD32, addr, &r1_response) == SUCCESS) {
    if(r1_response <= SD_IN_IDLE) {
      returnStatus = RES_OK;
    }
  }
  return returnStatus;
}

DRESULT sd_spi_read_info(uint8_t cmd, uint8_t* buf)
{
  DRESULT returnStatus = RES_ERROR;
    chip_select(TRUE);
    uint8_t r1_response;
    if (send_command(cmd, 0, &r1_response) == SUCCESS) {
      if (read_command_response(cmd, buf) == SUCCESS) {
        returnStatus = RES_OK;
      }
    }
    chip_select(FALSE);
    return returnStatus;
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */

static ErrorStatus rcvr_datablock (  /* 1:OK, 0:Error */
  uint8_t *buff,     /* Data buffer */
  uint16_t btr      /* Data block length (byte) */
)
{
  ErrorStatus returnStatus = ERROR;
  if (wait_ready(100) == SUCCESS) {
    if (xchg_spi(NULL, buff, btr + 2) == SUCCESS) {
      returnStatus = SUCCESS; /* Store trailing data to the buffer */
    }
  }

  return returnStatus;           /* Function succeeded */
}


static ErrorStatus read_card_status(uint16_t* status)
{
  ErrorStatus returnStatus = ERROR;
  if (status == NULL) {
    return returnStatus;
  }
  uint8_t response[3];
  if (send_command(CMD13, 0,  &response[0]) == SUCCESS) {
    if (read_command_response(CMD13, &response[1]) == SUCCESS) {
      *status = (uint16_t)((uint16_t)response[1] << 8 | response[2]);
      returnStatus = SUCCESS;
    }
  }
  return returnStatus;
}
ErrorStatus chip_select(bool_t set)
{
  ErrorStatus returnStatus = ERROR;
  if (set) {
    GPIO_setOutputLowOnPin(cs.port, cs.pin);
    xchg_spi(&dummy, NULL, 1);
    if (wait_ready(10) == SUCCESS) {
      returnStatus = SUCCESS;
    }
  } else {
    GPIO_setOutputHighOnPin(cs.port, cs.pin);
    xchg_spi(&dummy, NULL, 1);
    returnStatus = SUCCESS;
  }
  return returnStatus;
}

/*-----------------------------------------------------------------------*/
/* Wait for card ready                                                   */
/*-----------------------------------------------------------------------*/
static ErrorStatus wait_ready (uint8_t wt)
{
  ErrorStatus returnStatus = ERROR;
  //wait_ready needs its own timer, unfortunately, so it can't use the
  //spi_timer functions
  uint32_t waitSpiTimerTickStart;
  uint32_t waitSpiTimerTickDelay;
  uint8_t response = 0xFF;
  waitSpiTimerTickStart = systime_get_tick();
  waitSpiTimerTickDelay = (uint32_t)wt;
  do {
    xchg_spi(NULL, &response, 1);
    if (response == 0xFF) {
      returnStatus = SUCCESS;
      break;
    }
   } while (((systime_get_tick() - waitSpiTimerTickStart) < waitSpiTimerTickDelay)); /* Wait for card goes ready or timeout */

  return returnStatus;
}

void power_up()
{
  // Make sure that SD card is deselected
  chip_select(FALSE);
  // Wait for power up
  systime_wait_ms(1);

  // Send 80 clock cycles to sync with SD-card
  uint8_t i = 0;
  for(i; i < 10; i++) {
    xchg_spi(&dummy, NULL, 1);
  }
}

ErrorStatus send_command(uint8_t cmd, uint32_t arg, uint8_t* r1_response)
{
  ErrorStatus returnStatus = ERROR;
  uint8_t message[6] = {0};

  if ((cmd & 0x80) != 0) { // Send a CMD55 prior to ACMD<n> command
    cmd &= 0x7F;
    if (send_command(CMD55, 0, r1_response) == SUCCESS) {
      if (*r1_response > 1) {
        return ERROR;
      }
    }
  }

  if (cmd != CMD12) {
    chip_select(FALSE);
    if (chip_select(TRUE) == ERROR) {
      return returnStatus;
    }
  }
  message[0] = cmd | 0x40;
  message[1] = (uint8_t)(arg >> 24);
  message[2] = (uint8_t)(arg >> 16);
  message[3] = (uint8_t)(arg >> 8);
  message[4] = (uint8_t)(arg);
  message[5] = (crc7(message, 5) << 1) | 0x01;
  if (xchg_spi(message, NULL, 6) == SUCCESS) {
    returnStatus = SUCCESS;
  }
  if (returnStatus == SUCCESS) {
    returnStatus = ERROR;
    uint8_t i = 0;
    for (i; i<10; ++i) {
      if (xchg_spi(NULL, r1_response, 1) == SUCCESS) {
        if ((*r1_response & 0x80) == 0) {  // R1 response bit 7 always is 0
          returnStatus = SUCCESS;
          break;
        }
      }
    }
  }
  return returnStatus;
}

ErrorStatus read_command_response(uint8_t cmd, uint8_t* response)
{
  ErrorStatus returnStatus = ERROR;
  if (((cmd == CMD8 || cmd == CMD13 || cmd == CMD58) && response == NULL)) {
    return returnStatus;
  }
  sd_response_type response_type;
  uint8_t nr_of_response_bytes;
  switch (cmd) {
    case CMD0:
    case CMD17:
    case CMD24:
    case CMD55:
    case CMD59:
    case CMD32:
    case ACMD41:
      response_type = SD_R1;
      break;
    case CMD8:
      response_type = SD_R7;
      nr_of_response_bytes = 4;
      break;
    case CMD13:
      response_type = SD_R2_CMD13;
      nr_of_response_bytes = 1;
      break;
    case CMD2:
    case CMD9:
    case CMD10:
      response_type = SD_R2;
      nr_of_response_bytes = 20;
      break;
    case CMD58:
      response_type = SD_R3;
      nr_of_response_bytes = 4;
      break;
    default:
      break;
  }

  // Not only R1 response
  if ((uint8_t)response_type > (uint8_t)SD_R1) {
    returnStatus = ERROR;
    if (xchg_spi(NULL, response, nr_of_response_bytes) == SUCCESS) {
      returnStatus = SUCCESS;
    }
  }
  return returnStatus;
}

void generate_crc7_table()
{
  int i, j;
  uint8_t CRC_poly = 0x89;  // the value of our CRC-7 polynomial

  // generate a table value for all 256 possible byte values
  for (i = 0; i < 256; ++i) {
    CRC_table[i] = (i & 0x80) ? i ^ CRC_poly : i;
    for (j = 1; j < 8; ++j) {
      CRC_table[i] <<= 1;
        if (CRC_table[i] & 0x80)
          CRC_table[i] ^= CRC_poly;
    }
  }
}

uint8_t crc7(uint8_t* message, uint8_t nr_of_bytes)
{
  int i;
  uint8_t crc7 = 0;

  for (i = 0; i < nr_of_bytes; ++i)
    crc7 = CRC_table[(crc7 << 1) ^ message[i]];

  return crc7;
}

// Calculate CRC16 CCITT
// It's a 16 bit CRC with polynomial x^16 + x^12 + x^5 + 1
// input:
//   crcIn - the CRC before (0 for rist step)
//   data - byte for CRC calculation
// return: the CRC16 value
uint16_t crc16_one(uint16_t crc_in, uint8_t data)
{
  crc_in  = (uint8_t)(crc_in >> 8)|(crc_in << 8);
  crc_in ^=  data;
  crc_in ^= (uint8_t)(crc_in & 0xff) >> 4;
  crc_in ^= (crc_in << 8) << 4;
  crc_in ^= ((crc_in & 0xff) << 4) << 1;

  return crc_in;
}

// Calculate CRC16 CCITT value of the buffer
// input:
//   pBuf - pointer to the buffer
//   len - length of the buffer
// return: the CRC16 value
uint16_t crc16_buf(const uint8_t * p_buf, uint16_t len)
{
  uint16_t crc = 0;

  while (len--) crc = crc16_one(crc,*p_buf++);

  return crc;
}

ErrorStatus check_voltage(uint16_t supply_voltage, uint16_t ocr_accepted_voltages)
{
  ErrorStatus returnStatus = ERROR;
  ocr_accepted_voltages >>= 7;
  uint16_t start_voltage = 2700;
  uint16_t increment_voltage = 100;
  uint8_t i = 0;
  for (i; i < 8; ++i) {
    if (supply_voltage >= (start_voltage + increment_voltage*i) && supply_voltage <= (start_voltage + increment_voltage*(i+1)) && ((ocr_accepted_voltages & (1 << i)) != 0)) {
      returnStatus = SUCCESS;
      break;
    }
  }
  return returnStatus;
}

/* Exchange one or more bytes */
static ErrorStatus xchg_spi(uint8_t* tx_data, uint8_t* rx_data, uint16_t nr_of_bytes)
{
  ErrorStatus returnStatus = SUCCESS;

  if (rx_data == NULL) {
    DMA_clearInterruptFlag(dmaTxChannel  & 0x0F);
    DMA_setChannelTransfer(UDMA_PRI_SELECT | dmaTxChannel ,
                           UDMA_MODE_BASIC,
                           tx_data,
                           (void*) &((EUSCI_B_SPI_Type*)spi->module)->TXBUF,
                           nr_of_bytes);
    DMA_enableChannel(dmaTxChannel);
    while(((DMA_getInterruptStatus() & (1<<DMA_CH2_RESERVED0)) == 0));  // TODO(caer) Timeout
    while(SPI_isBusy(spi->module) == EUSCI_SPI_BUSY);

  } else {
    // Change TX DMA settings to not increment.
    // This is for clock pulse generation
    DMA_setChannelControl(UDMA_PRI_SELECT | dmaTxChannel ,
                          UDMA_SIZE_8 |
                          UDMA_SRC_INC_NONE |
                          UDMA_DST_INC_NONE |
                          UDMA_ARB_1);

    DMA_clearInterruptFlag(dmaRxChannel&0x0F);
    DMA_clearInterruptFlag(dmaTxChannel&0x0F);
    DMA_setChannelTransfer(UDMA_PRI_SELECT | dmaRxChannel ,
                           UDMA_MODE_BASIC, (void*) &((EUSCI_B_SPI_Type*)spi->module)->RXBUF,  // TODO(caer) EUSCI_B shall not be fixed.
                           (void*)rx_data, nr_of_bytes);
    DMA_setChannelTransfer(UDMA_PRI_SELECT | dmaTxChannel ,
                           UDMA_MODE_BASIC,
                           (void*)&dummy,
                           (void*) &((EUSCI_B_SPI_Type*)spi->module)->TXBUF,
                           nr_of_bytes);
    DMA_enableChannel(dmaTxChannel);
    DMA_enableChannel(dmaRxChannel);
    ((EUSCI_B_SPI_Type*)spi->module)->IFG = 0x02;  // Trigger TX interrupt to start the DMA transfer.
    while(((DMA_getInterruptStatus() & (1<<DMA_CH2_RESERVED0)) == 0) && ((DMA_getInterruptStatus() & (1<<DMA_CH3_RESERVED0)) == 0)); // When transmitted all clock pulses it will trigger a interrupt TODO(caer) Timeout
    while(SPI_isBusy(spi->module) == EUSCI_SPI_BUSY);
    DMA_clearInterruptFlag(dmaRxChannel&0x0F);
    DMA_clearInterruptFlag(dmaTxChannel&0x0F);
    // Reset TX settings to increment source address
    DMA_setChannelControl(UDMA_PRI_SELECT | dmaTxChannel ,
                          UDMA_SIZE_8 |
                          UDMA_SRC_INC_8 |
                          UDMA_DST_INC_NONE |
                          UDMA_ARB_1);
  }
  return returnStatus;
}
/*
 * End of file: sd.c
 */
