/**
 * @file sd_card_spi.h
 * @brief Commands and communication via SPI for the SD-card.
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef SD_CARD_SPI_H_
#define SD_CARD_SPI_H_
#include "../../Hal/spi.h"
#include "../../Fatfs/diskio.h"
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */
#define SD_PARAM_ERROR      0b01000000
#define SD_ADDR_ERROR       0b00100000
#define SD_ERASE_SEQ_ERROR  0b00010000
#define SD_CRC_ERROR        0b00001000
#define SD_ILLEGAL_CMD      0b00000100
#define SD_ERASE_RESET      0b00000010
#define SD_IN_IDLE          0b00000001
/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */
typedef enum {
  SD_R1 = 0,
  SD_R2_CMD13,
  SD_R2,
  SD_R3,
  SD_R7
} sd_response_type;
/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */
/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */

ErrorStatus sd_spi_init(spi_s* const spi_p,
                    uint_fast8_t cs_port,
                    uint_fast16_t cs_pin,
                    uint16_t sd_card_supply_voltage,
                    uint32_t dma_rx_channel,
                    uint32_t dma_tx_channel);
DSTATUS sd_spi_initialize(uint8_t drv);
DSTATUS sd_spi_status (uint8_t drv);
DRESULT sd_spi_ioctl (uint8_t cmd, void *buff);
DRESULT sd_spi_write_single_block(uint32_t addr, uint8_t *buf);
DRESULT sd_spi_write(uint32_t addr, uint8_t *buff, uint16_t count);
DRESULT sd_spi_read_single_block(uint32_t addr, uint8_t *buf);
DRESULT sd_spi_read(uint32_t addr, uint8_t *buff, uint16_t count);
DRESULT sd_spi_erase_single_block(uint32_t addr);
DRESULT sd_spi_read_info(uint8_t cmd, uint8_t* buf);

#ifdef __cplusplus
}
#endif

#endif /* SD_CARD_SPI_H_ */

