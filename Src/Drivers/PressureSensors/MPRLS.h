/**
 * @file MPRLS.h
 * @brief Driver for the MPRLS pressure sensor from Honeywell
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _MPRLS_H_
#define _MPRLS_H_

#include "../../HAL/i2c.h"
#include "../../Board/board.h"
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */
typedef struct {
  eUSCI_I2C* i2c;
  uint8_t address;
  uint_fast8_t resSensorPort;
  uint_fast16_t resSensorPin;
  uint_fast8_t measReadyPort;
  uint_fast16_t measReadyPin;
}mprls_sensor_s;
/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus MPRLS_init(mprls_sensor_s* sensor);
ErrorStatus MPRL_reset(mprls_sensor_s* sensor);
ErrorStatus MPRL_start_measure(mprls_sensor_s* sensor);
ErrorStatus MPRL_read_measure(mprls_sensor_s* sensor, float* data);

#ifdef __cplusplus
}
#endif

#endif /* _MPRLS_H_ */

/*
 * End of file: MPRLS.h
 */
