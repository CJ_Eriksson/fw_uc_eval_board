/**
 * @file MPRLS.c
 * @brief Driver for the MPRLS pressure sensor from Honeywell
 *
 * @copyright
 *        Unpublished Work Copyright 2021 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Drivers/Time/systime.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "MPRLS.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define POWER_INDICATION 0x40
#define MEAS_BUSY 0x20
#define INTEGRETY_TEST 0x04
#define SATURATION 0x01
// TODO(caer) Shall it be configurable since it is depending on what type of MPRLS sensor that is used.
#define P_MIN 0.0f  // Minimum pressure (psi)
#define P_MAX 25.0f // Maximum pressure (psi)
#define OUT_MIN 1677722.0f  // Minimum out value from sensor 10% of 2^24
#define OUT_MAX 15099494.0f  // Maximum pressure from sensor 90% of 2^24
#define PSI_TO_CMH2O 68.95f // Conversion factor from psi to cmH2O
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief Initialize of the MPRLS sensor
 * @param [in] sensor - Struct that contains the pointer to the I2C instance and
 *                      the GPIOs for the selected sensor.
 * @return SUCCESS on SUCCESS
 */
ErrorStatus MPRLS_init(mprls_sensor_s* sensor)
{
  I2C_setMode(sensor->i2c->moduleInstance, EUSCI_B_I2C_TRANSMIT_MODE);
  I2C_setTimeout(sensor->i2c->moduleInstance, EUSCI_B_I2C_TIMEOUT_34_MS);
  I2C_setSlaveAddress(sensor->i2c->moduleInstance, sensor->address);
  I2C_enableModule(sensor->i2c->moduleInstance);
  return SUCCESS;
}

/**
 * @brief Reset the sensor
 * @param [in] sensor - Struct that contains the pointer to the I2C instance and
 *                      the GPIOs for the selected sensor.
 * @return SUCCESS on SUCCESS
 */
ErrorStatus MPRL_reset(mprls_sensor_s* sensor)
{
  volatile uint16_t delay = 0;
  GPIO_setOutputHighOnPin(sensor->resSensorPort, sensor->resSensorPin);
  while(++delay < 100);
  GPIO_setOutputLowOnPin(sensor->resSensorPort, sensor->resSensorPin);
  while(++delay < 200);
  GPIO_setOutputHighOnPin(sensor->resSensorPort, sensor->resSensorPin);
  systime_wait_ms(3); // 3ms start-up after reset
  return SUCCESS;
}

/**
 * @brief Initialize a start of measurement for the selected sensor
 * @param [in] sensor - Struct that contains the pointer to the I2C instance and
 *                      the GPIOs for the selected sensor.
 * @return SUCCESS on SUCCESS
 */
ErrorStatus MPRL_start_measure(mprls_sensor_s* sensor)
{
  if (I2C_isBusBusy(sensor->i2c->moduleInstance)) {
    return BUSY;
  }
  ErrorStatus returnStatus = ERROR;
  uint8_t startMeasData[3] = {0xAA, 0x00, 0x00};
  I2C_setSlaveAddress(sensor->i2c->moduleInstance, sensor->address);
  I2C_setMode(sensor->i2c->moduleInstance, EUSCI_B_I2C_TRANSMIT_MODE);
  if (I2C_masterSendMultiByteStartWithTimeout(sensor->i2c->moduleInstance,
                                          startMeasData[0], 100)) {
    if (I2C_masterSendMultiByteNextWithTimeout(sensor->i2c->moduleInstance, startMeasData[1], 100)) {
      if (I2C_masterSendMultiByteFinishWithTimeout(sensor->i2c->moduleInstance,
                                                   startMeasData[2], 100)) {
        returnStatus = SUCCESS;
      }
    }
  }
  return returnStatus;
}

/**
 * @brief Read the selected sensor measurement
 * @param [in] sensor - Struct that contains the pointer to the I2C instance and
 *                      the GPIOs for the selected sensor.
 *        [out] data - Calculated pressure
 * @return SUCCESS on SUCCESS
 *         BUSY when mesurement ongoing
 *         ERROR when fault detected
 */
ErrorStatus MPRL_read_measure(mprls_sensor_s* sensor, float* data)
{
 // if (I2C_isBusBusy(sensor->i2c->moduleInstance) != 0) {
  //  return BUSY;
 // }
  if (I2C_isBusBusy(sensor->i2c->moduleInstance)) {
    return BUSY;
  }
  ErrorStatus returnStatus = SUCCESS;
  uint8_t status1 = 0xFF;
  uint8_t status = 0xFF;
  uint8_t measData[3];
  I2C_setSlaveAddress(sensor->i2c->moduleInstance, sensor->address);
  //I2C_setMode(sensor->i2c->moduleInstance, EUSCI_B_I2C_RECEIVE_MODE);

  status1 = I2C_masterReceiveSingleByte(sensor->i2c->moduleInstance);

  //I2C_masterReceiveStart(sensor->i2c->moduleInstance);
  //I2C_masterReceiveSingleTimeout(sensor->i2c->moduleInstance, &status, 100);
  /*if ((status1 & POWER_INDICATION) == 0) {
    return ERROR;
  }*/
  if ((status1 & MEAS_BUSY) != 0) {
    return BUSY;
  } else {
    I2C_masterReceiveStart(sensor->i2c->moduleInstance);
    status = I2C_masterReceiveSingle(sensor->i2c->moduleInstance);
    measData[0] = I2C_masterReceiveSingle(sensor->i2c->moduleInstance);
    //(void)I2C_masterReceiveSingleTimeout(sensor->i2c->moduleInstance, &measData[0], 100);
    measData[1] = I2C_masterReceiveSingle(sensor->i2c->moduleInstance);
    //(void)I2C_masterReceiveSingleTimeout(sensor->i2c->moduleInstance, &measData[1], 100);
    I2C_masterReceiveMultiByteFinishWithTimeout(sensor->i2c->moduleInstance, &measData[2], 100);
    /*if (I2C_masterReceiveSingleTimeout(sensor->i2c->moduleInstance, &measData[0], 100) == true) {
      if (I2C_masterReceiveSingleTimeout(sensor->i2c->moduleInstance, &measData[1], 100) == true) {
        if (I2C_masterReceiveMultiByteFinishWithTimeout(sensor->i2c->moduleInstance, &measData[2], 100) == true) {
          returnStatus = SUCCESS;
        }
      }
    }*/
    if (returnStatus == SUCCESS) {
      if ((status & MEAS_BUSY) != 0) {
        returnStatus = BUSY;
      } else if (((status & POWER_INDICATION) != 0) &&
          ((status & INTEGRETY_TEST) == 0) &&
          ((status & SATURATION) == 0)) {
        uint32_t tempData = 0;
        tempData = ((uint32_t)measData[0] & 0xFF)  << 16;
        tempData |= ((uint32_t)measData[1] & 0xFF) << 8;
        tempData |= ((uint32_t)measData[2] & 0xFF);
        *data = (((((float)tempData - OUT_MIN) * (P_MAX + P_MIN)) /
            (OUT_MAX - OUT_MIN)) + P_MIN) * PSI_TO_CMH2O;
        returnStatus = SUCCESS;
      }
    }
  }

  return returnStatus;
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */

/*
 * End of file: MPRLS.c
 */
