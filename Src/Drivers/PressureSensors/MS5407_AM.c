/**
 * @file MS5407_AM.c
 * @brief Driver for the MS5407_AM analog pressure sensor from TE Connectivity
 *
 * @copyright
 *        Unpublished Work Copyright 2021 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "MS5407_AM.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define MS5407_CONVERSIONS 10
#define MS5407_GAIN 7.6667f
#define MS5407_MV_PER_CMH2O 18.2f // According to datasheet average mV/bar = 56. 1 Bar ~1020cmH2O
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static uint16_t measurementArray[MS5407_CONVERSIONS]; // If assigned to {0} the DMA is fucked up...
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief Initialize of the MS5407 sensor
 * @param [in] sensor - Struct that contains the sensor configuration
 * @return SUCCESS on SUCCESS
 */
ErrorStatus MS5407_init(ms5407_sensor_s* sensor) {
  (void)(sensor);
  return SUCCESS;
}

/**
 * @brief Start measurement of selected sensor
 * @param [in] sensor - Struct that contains the sensor configuration
 * @return SUCCESS on SUCCESS
 */
ErrorStatus MS5407_start_measure(ms5407_sensor_s* sensor)
{
  return adc_start_meas(sensor->adcChannel, TRUE, MS5407_CONVERSIONS, measurementArray);
}

/**
 * @brief Read the measurmenet of the sensor from the ADC
 * @param [in] sensor - Struct that contains the sensor configuration
 * @return SUCCESS on SUCCESS
 *         BUSY when conversion not ready
 */
ErrorStatus MP5407_read_measure(ms5407_sensor_s* sensor, float* data)
{
  ErrorStatus returnStatus = ERROR;
  bool_t dataReady = adc_check_meas_ready(sensor->adcChannel);
  uint16_t meas[MS5407_CONVERSIONS];
  if (dataReady == TRUE) {
    uint8_t i = 0;
    uint32_t tempData = 0;
    for (i = 0; i < MS5407_CONVERSIONS; ++i) {
      tempData += measurementArray[i];
    }
    *data = (float)tempData / MS5407_CONVERSIONS;
    *data = (((((*data) / (float)ADC_MAX_VALUE)) * ADC_REFERENCE_VOLTAGE) / MS5407_GAIN) * MS5407_MV_PER_CMH2O;
    returnStatus = SUCCESS;
  } else {
    returnStatus = BUSY;
  }
  return returnStatus;
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */

/*
 * End of file: MS5407_AM.c
 */
