/**
 * @file ICM_20948.c
 * @brief Driver for the 9-axis IMU
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include <stdio.h>
#include <string.h>
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Hal/spi.h"
#include "../../Hal/eusci.h"
#include "../../Common/common_types.h"
#include "../../Board/board.h"

#include "ICM_20948.h"



/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define ICM-20948 1
#define MAG_RES 0.15f
#define SPI_TIMEOUT      10
#define REGISTER_BANK_ADDRESS  0x7F
#define GYRO_FILTER_COEFF 4
#define ACC_FILTER_COEFF 2
#define SAMPLE_RATE_DIVISOR 0 // Gives a divisor of 1 ICM20948
#define DMP_CODE_SIZE 14250
#define DMP_START_ADDRESS   ((unsigned short)0x1000)
#define DMP_MEM_BANK_SIZE   256
#define DMP_LOAD_START      0x90

#define ICM20648_BIT_GYRO_FCHOICE        0x01                        /**< Gyro Digital Low-Pass Filter enable bit    */
#define ICM20648_SHIFT_GYRO_FS_SEL       1                           /**< Gyro Full Scale Select bit shift           */
#define ICM20648_SHIFT_GYRO_DLPCFG       3                           /**< Gyro DLPF Config bit shift                 */
#define ICM20648_MASK_GYRO_FULLSCALE     0x06                        /**< Gyro Full Scale Select bitmask             */
#define ICM20648_MASK_GYRO_BW            0x39                        /**< Gyro Bandwidth Select bitmask              */
#define ICM20648_GYRO_FULLSCALE_250DPS   (0x00 << ICM20648_SHIFT_GYRO_FS_SEL)    /**< Gyro Full Scale = 250 deg/sec  */
#define ICM20648_GYRO_FULLSCALE_500DPS   (0x01 << ICM20648_SHIFT_GYRO_FS_SEL)    /**< Gyro Full Scale = 500 deg/sec  */
#define ICM20648_GYRO_FULLSCALE_1000DPS  (0x02 << ICM20648_SHIFT_GYRO_FS_SEL)    /**< Gyro Full Scale = 1000 deg/sec */
#define ICM20648_GYRO_FULLSCALE_2000DPS  (0x03 << ICM20648_SHIFT_GYRO_FS_SEL)    /**< Gyro Full Scale = 2000 deg/sec */
#define ICM20648_GYRO_BW_12100HZ         (0x00 << ICM20648_SHIFT_GYRO_DLPCFG)                                     /**< Gyro Bandwidth = 12100 Hz */
#define ICM20648_GYRO_BW_360HZ           ( (0x07 << ICM20648_SHIFT_GYRO_DLPCFG) | ICM20648_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 360 Hz   */
#define ICM20648_GYRO_BW_200HZ           ( (0x00 << ICM20648_SHIFT_GYRO_DLPCFG) | ICM20648_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 200 Hz   */
#define ICM20648_GYRO_BW_150HZ           ( (0x01 << ICM20648_SHIFT_GYRO_DLPCFG) | ICM20648_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 150 Hz   */
#define ICM20648_GYRO_BW_120HZ           ( (0x02 << ICM20648_SHIFT_GYRO_DLPCFG) | ICM20648_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 120 Hz   */
#define ICM20648_GYRO_BW_51HZ            ( (0x03 << ICM20648_SHIFT_GYRO_DLPCFG) | ICM20648_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 51 Hz    */
#define ICM20648_GYRO_BW_24HZ            ( (0x04 << ICM20648_SHIFT_GYRO_DLPCFG) | ICM20648_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 24 Hz    */
#define ICM20648_GYRO_BW_12HZ            ( (0x05 << ICM20648_SHIFT_GYRO_DLPCFG) | ICM20648_BIT_GYRO_FCHOICE)      /**< Gyro Bandwidth = 12 Hz    */
#define ICM20648_GYRO_BW_6HZ             ( (0x06 << ICM20648_SHIFT_GYRO_DLPCFG) | ICM20648_BIT_GYRO_FCHOICE)

#define ICM20648_BIT_ACCEL_FCHOICE       0x01                        /**< Accel Digital Low-Pass Filter enable bit               */
#define ICM20648_SHIFT_ACCEL_FS          1                           /**< Accel Full Scale Select bit shift                      */
#define ICM20648_SHIFT_ACCEL_DLPCFG      3                           /**< Accel DLPF Config bit shift                            */
#define ICM20648_MASK_ACCEL_FULLSCALE    0x06                        /**< Accel Full Scale Select bitmask                        */
#define ICM20648_MASK_ACCEL_BW           0x39                        /**< Accel Bandwidth Select bitmask                         */
#define ICM20648_ACCEL_FULLSCALE_2G      (0x00 << ICM20648_SHIFT_ACCEL_FS)    /**< Accel Full Scale = 2 g  */
#define ICM20648_ACCEL_FULLSCALE_4G      (0x01 << ICM20648_SHIFT_ACCEL_FS)    /**< Accel Full Scale = 4 g  */
#define ICM20648_ACCEL_FULLSCALE_8G      (0x02 << ICM20648_SHIFT_ACCEL_FS)    /**< Accel Full Scale = 8 g  */
#define ICM20648_ACCEL_FULLSCALE_16G     (0x03 << ICM20648_SHIFT_ACCEL_FS)    /**< Accel Full Scale = 16 g */
#define ICM20648_ACCEL_BW_1210HZ         (0x00 << ICM20648_SHIFT_ACCEL_DLPCFG)                                    /**< Accel Bandwidth = 1210 Hz  */
#define ICM20648_ACCEL_BW_470HZ          ( (0x07 << ICM20648_SHIFT_ACCEL_DLPCFG) | ICM20648_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 470 Hz   */
#define ICM20648_ACCEL_BW_246HZ          ( (0x00 << ICM20648_SHIFT_ACCEL_DLPCFG) | ICM20648_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 246 Hz   */
#define ICM20648_ACCEL_BW_111HZ          ( (0x02 << ICM20648_SHIFT_ACCEL_DLPCFG) | ICM20648_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 111 Hz   */
#define ICM20648_ACCEL_BW_50HZ           ( (0x03 << ICM20648_SHIFT_ACCEL_DLPCFG) | ICM20648_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 50 Hz    */
#define ICM20648_ACCEL_BW_24HZ           ( (0x04 << ICM20648_SHIFT_ACCEL_DLPCFG) | ICM20648_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 24 Hz    */
#define ICM20648_ACCEL_BW_12HZ           ( (0x05 << ICM20648_SHIFT_ACCEL_DLPCFG) | ICM20648_BIT_ACCEL_FCHOICE)    /**< Accel Bandwidth = 12 Hz    */
#define ICM20648_ACCEL_BW_6HZ            ( (0x06 << ICM20648_SHIFT_ACCEL_DLPCFG) | ICM20648_BIT_ACCEL_FCHOICE)
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */
#ifdef ICM-20948
typedef enum {
  MAG_WHO_AM_I = 0x00,
  STATUS = 0x10,
  MAG_XL = 0x11,
  MAG_XH = 0x12,
  MAG_YL = 0x13,
  MAG_YH = 0x14,
  MAG_ZL = 0x15,
  MAG_ZH = 0x16,
  CTRL2 = 0x31,
  CTRL3 = 0x32
} imu_magnetometer_register_e;

typedef enum {
  MAG_POWER_DOWN = 0x00,
  MAG_MODE_SINGLE_MEAS = 0x01,
  MAG_MODE1_10Hz = 0x02,
  MAG_MODE2_20Hz = 0x04,
  MAG_MODE3_50Hz = 0x06,
  MAG_MODE4_100Hz = 0x08,
  MAG_MODE_SELF_TEST = 0x10
} imu_magnetometer_mode_e;

#endif
typedef enum {
  WHO_AM_I = 0x00,
  USER_CTRL = 0x03,
  LP_CONFIG = 0x05,
  PWR_MGMT_1 = 0x06,
  PWR_MGMT_2 = 0x07,
  INT_PIN_CFG = 0x0F,
  INT_ENABLE = 0x10,
  INT_ENABLE_1 = 0x11,
  INT_ENABLE_2 = 0x12,
  INT_ENABLE_3 = 0x13,
  I2C_MST_STATUS = 0x17,
  INT_STATUS = 0x19,
  INT_STATUS_1 = 0x1A,
  INT_STATUS_2 = 0x1B,
  INT_STATUS_3 = 0x1C,
  DELAY_TIMEH = 0x28,
  DELAY_TIMEL = 0x29,
  ACCEL_XOUT_H = 0x2D,
  ACCEL_XOUT_L = 0x2E,
  ACCEL_YOUT_H = 0x2F,
  ACCEL_YOUT_L = 0x30,
  ACCEL_ZOUT_H = 0x31,
  ACCEL_ZOUT_L = 0x32,
  GYRO_XOUT_H = 0x33,
  GYRO_XOUT_L = 0x34,
  GYRO_YOUT_H = 0x35,
  GYRO_YOUT_L = 0x36,
  GYRO_ZOUT_H = 0x37,
  GYRO_ZOUT_L = 0x38,
  TEMP_OUT_H = 0x39,
  TEMP_OUT_L = 0x3A,
  EXT_SLV_SENSE_D0 = 0x3B,
  EXT_SLV_SENSE_D1 = 0x3C,
  EXT_SLV_SENSE_D2 = 0x3D,
  EXT_SLV_SENSE_D3 = 0x3E,
  EXT_SLV_SENSE_D4 = 0x40,
  EXT_SLV_SENSE_D5 = 0x41,
  EXT_SLV_SENSE_D6 = 0x42,
  EXT_SLV_SENSE_D7 = 0x43,
  FIFO_EN_1 = 0x66,
  FIFO_EN_2 = 0x67,
  FIFO_RST = 0x68,
  FIFO_MODE = 0x69,
  FIFO_COUNTH = 0x70,
  FIFO_COUNTL = 0x71,
  FIFO_R_W = 0x72,
  dataRDY_STATUS = 0x74,
  FIFO_CFG = 0x76,
} imu_register_bank_0_e;

typedef enum {
  GYRO_SMPLRT_DIV = 0x00,
  GYRO_CONFIG_1 = 0x01,
  GYRO_CONFIG_2 = 0x02,
  XG_OFFS_USRH = 0x03,
  XG_OFFS_USRL = 0x04,
  YG_OFFS_USRH = 0x05,
  YG_OFFS_USRL = 0x06,
  ZG_OFFS_USRH = 0x07,
  ZG_OFFS_USRL = 0x08,
  ACCEL_SMPLRT_DIV_1 = 0x10,
  ACCEL_SMPLRT_DIV_2 = 0x11,
  ACCEL_INTEL_CTRL = 0x12,
  ACCEL_WOM_THR = 0x13,
  ACCEL_CONFIG = 0x14,
  ACCEL_CONFIG_2 = 0x15,
} imu_register_bank_2_e;

typedef enum {
  I2C_MASTER_ODR_CONFIG = 0x00,
  I2C_MASTER_CTRL = 0x01,
  I2C_SLV0_ADDR = 0x03,
  I2C_SLV0_REG = 0x04,
  I2C_SLV0_CTRL = 0x05,
  I2C_SLV0_DO = 0x06,
  I2C_SLV4_ADDR = 0x13,
  I2C_SLV4_REG = 0x14,
  I2C_SLV4_CTRL = 0x15,
  I2C_SLV4_DO = 0x16,
  I2C_SLV4_DI = 0x17
} imu_register_bank_3_e;

typedef enum {
  BANK_ZERO,
  BANK_ONE,
  BANK_TWO,
  BANK_THREE
} imu_register_bank;
/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */
float mag_hardi_x_calib = 0;
float mag_hardi_y_calib = 0;
float mag_hardi_z_calib = 0;
float mag_softi_x_calib = 0;
float mag_softi_y_calib = 0;
float mag_softi_z_calib = 0;

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static uint_fast8_t* dummypointer = NULL;
static spi_s* spiPointer = NULL;
static gpio_s cs;
static float gyroScaleFactors[4] = {131.0f, 65.5f, 32.8f, 16.4f};
static float accScaleFactors[4] = {16384.0f, 8192.0f, 4096.0f, 2048.0f};
static float gyroUsedScaleFactor = 1;
static float accUsedScaleFactor = 1;
static unsigned char lLastBankSelected=0xFF;

/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */
static ErrorStatus read_spi(uint8_t const reg, uint8_t* const data, uint16_t const size);
static ErrorStatus write_spi(uint8_t adr, uint8_t* data,  uint16_t length);
static ErrorStatus select_bank(imu_register_bank bank);
static ErrorStatus dmp_firmware_load(const unsigned char *data_start, unsigned short size_start, unsigned short load_addr);
static ErrorStatus enable_sensor(bool_t acc, bool_t gyro);
static ErrorStatus set_sample_rate(float acc_smplr, float gyro_smplr);
static ErrorStatus set_accel_bandwidth(uint8_t acc_bw);
static ErrorStatus set_gyro_bandwidth(uint8_t gyro_bw);
static ErrorStatus calibrate(float *accelBiasScaled, float *gyroBiasScaled);
static ErrorStatus imu_magnetometer_hard_soft_iron_calib();
static ErrorStatus imu_enable_magnetometer();
static ErrorStatus imu_write_magnetometer_i2c(uint8_t address, uint8_t reg, uint8_t nr_of_bytes, uint8_t data);
static ErrorStatus imu_trigger_read_data_magnetometer_i2c(uint8_t address, uint8_t nr_of_bytes);
static ErrorStatus imu_read_write_magnetometer_i2c_one_byte(uint8_t address, uint8_t reg, uint8_t* data, bool_t read);
static ErrorStatus imu_stop_ext0();
static ErrorStatus imu_set_magnetometer_i2c_register(uint8_t address, uint8_t reg);
static ErrorStatus xchg_spi(uint8_t* tx_data,
                                   uint8_t* rx_data,
                                   uint16_t nr_of_bytes);

/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */

ErrorStatus imu_init(spi_s * const spi_p,
                      uint_fast8_t cs_port,
                      uint_fast16_t cs_pin,
                      imu_acc_sensitivity_e acc_sensitivity,
                      bool_t acc_dlp_enable,
                      imu_gyro_sensitivity_e gyro_sensitivity,
                      bool_t gyro_dlp_enable)
{
  ErrorStatus returnStatus = ERROR;
  cs.port = cs_port;
  cs.pin = cs_pin;
  dummypointer = &cs_port;
  if (spi_p == NULL) {
    return returnStatus;
  }
  spiPointer = spi_p;

  if (select_bank(BANK_ZERO) == SUCCESS) {  // Select bank zero as target for SPI communication
    uint8_t data = 0;
    if (read_spi(PWR_MGMT_1, &data, 1) == SUCCESS) {
      data &=~0x40; // Reset sleep bit
      if (write_spi(PWR_MGMT_1, &data, 1) == SUCCESS) {
        if (select_bank(BANK_TWO) == SUCCESS) {  // Select bank two as target for SPI communication since sensitivity registers for acc and gyro is in bank 2
          data = GYRO_FILTER_COEFF << 3 | gyro_sensitivity << 1 | gyro_dlp_enable; // Set gyro sensitivity and enable/disable digital low pass filer
          if (write_spi(GYRO_CONFIG_1, &data, 1) == SUCCESS) {
            gyroUsedScaleFactor = gyroScaleFactors[(uint8_t)gyro_sensitivity];
            data = ACC_FILTER_COEFF << 3 | acc_sensitivity << 1 | acc_dlp_enable; // Set acc sensitivity and enable/disable digital low pass filer
            if (write_spi(ACCEL_CONFIG, &data, 1) == SUCCESS) {
              accUsedScaleFactor = accScaleFactors[(uint8_t)acc_sensitivity];
              data = SAMPLE_RATE_DIVISOR;
              if (write_spi(GYRO_SMPLRT_DIV, &data, 1) == SUCCESS) {
                data = SAMPLE_RATE_DIVISOR;
                if (write_spi(ACCEL_SMPLRT_DIV_2, &data, 1) == SUCCESS) {
                  if (select_bank(BANK_ZERO) == SUCCESS) {  // Select bank zero as target for SPI communication. Holds acc and gyro data
                    returnStatus = SUCCESS;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  if (returnStatus == SUCCESS) {
    returnStatus = ERROR;
    float acc[3] = {0};
    float gyro[3] = {0};
    if (calibrate(acc, gyro) == SUCCESS) {
      returnStatus = SUCCESS;
    }

    // Enable i2c master. The magnetometer is connected to a i2c bus inside the
    // device
    if (returnStatus == SUCCESS) {
      returnStatus = ERROR;
      uint8_t dataR = 0;
      if (read_spi(USER_CTRL, &dataR, 1) == SUCCESS) {
        dataR |= 0x20;
        if (write_spi(USER_CTRL, &dataR, 1) == SUCCESS) {
          read_spi(USER_CTRL, &dataR, 1);
          if (imu_enable_magnetometer() == SUCCESS) {
            returnStatus = SUCCESS;
            //imu_magnetometer_hard_soft_iron_calib();
          }
        }
      }
    }
  }
  enable_sensor(TRUE, TRUE);


// try to use the onboard DMP process for quarternion calculations. Not finished
//  if (returnStatus == SUCCESS) {
//    unsigned char result = 0;
//    if (dmp_firmware_load(dmp_memory, DMP_CODE_SIZE, DMP_LOAD_START) == SUCCESS) {
//      unsigned char dmp_cfg[2] = {0x10, 0x00};
//      unsigned char data[4]={0};
//
//      result =  (unsigned char)write_spi(0x70, dmp_cfg, 2);
//      //reset data output control registers
//      result += (unsigned char)write_spi(0x40, 2, &data[0]);
//      result += (unsigned char)write_spi(0x42, 2, &data[0]);
//
//    //reset data interrupt control register
//      result += (unsigned char)write_spi(0x4C, 2, &data[0]);
//
//      //reset motion event control register
//      result += (unsigned char)write_spi(0x4E, 2, &data[0]);
//
//      //reset data ready status register
//      result += (unsigned char)write_spi(0x8A, 2, &data[0]);
//      if (result != 0) {
//        returnStatus = ErrorStatusRROR;
//      }
//    }
//  }
//  // FIFO Setup.
//  if (returnStatus == SUCCESS) {
//    result =  (unsigned char)write_spi(0x76,  &data[0], 1); // FIFO Config. fixme do once? burst write?
//    data[0] = 0x1F;
//    result +=  (unsigned char)write_spi(0x68, &data[0], 1); // Reset all FIFOs.
//    data[0] = 0x1e;
//    result +=  (unsigned char)write_spi(0x68, &data[0], 1); // Keep all but Gyro FIFO in reset.
//    data[0] = 0x00;
//    result +=  (unsigned char)write_spi(0x66, &data[0], 1); // Slave FIFO turned off.
//    data[0] = 0x00;
//    result +=  (unsigned char)write_spi(0x67, &data[0], 1); // Hardware FIFO turned off.
//    if (result != 0) {
//      returnStatus = ErrorStatusRROR;
//    }
//  }
  return returnStatus;
}

ErrorStatus imu_check_dataReady()
{
  ErrorStatus returnStatus = ERROR;
  uint8_t data;
  if (read_spi(INT_STATUS_1, &data, 1) == SUCCESS) {
    if ((data & 0x01) != 0) {
      returnStatus = SUCCESS;
    }
  }
  return returnStatus;
}

ErrorStatus imu_get_sensitivity(float* const acc, float* const gyro)
{
  *acc = accUsedScaleFactor;
  *gyro = gyroUsedScaleFactor;
  return SUCCESS;
}

ErrorStatus imu_read_acc(imu_acc_axes_e axes, float* const acc_data)
{
  ErrorStatus returnStatus = ERROR;
  imu_register_bank_0_e reg_address_buf[2] = {0, 0};
  switch (axes) {
    case IMU_ACC_X:
      reg_address_buf[0] = ACCEL_XOUT_H;
      reg_address_buf[1] = ACCEL_XOUT_L;
      break;
    case IMU_ACC_Y:
      reg_address_buf[0] = ACCEL_YOUT_H;
      reg_address_buf[1] = ACCEL_YOUT_L;
      break;
    case IMU_ACC_Z:
      reg_address_buf[0] = ACCEL_ZOUT_H;
      reg_address_buf[1] = ACCEL_ZOUT_L;
      break;
    default:
      break;
  }
  uint8_t data;
  int16_t acc_data_temp = 0;
  if (reg_address_buf[0] != 0 &&  reg_address_buf[1] != 0) {
    if (read_spi(reg_address_buf[0], &data, 1) == SUCCESS) {
      acc_data_temp = data << 8;
      if (read_spi(reg_address_buf[1], &data, 1) == SUCCESS) {
        acc_data_temp |= data;
        *acc_data = (float)acc_data_temp / accUsedScaleFactor;
        returnStatus = SUCCESS;
      }
    }
  }

  return returnStatus;
}

ErrorStatus imu_read_gyro(imu_gyro_axes_e axes, float* const gyro_data)
{
  ErrorStatus returnStatus = ERROR;
  imu_register_bank_0_e reg_address_buf[2] = {0, 0};
  switch (axes) {
    case IMU_GYRO_X:
      reg_address_buf[0] = GYRO_XOUT_H;
      reg_address_buf[1] = GYRO_XOUT_L;
      break;
    case IMU_GYRO_Y:
      reg_address_buf[0] = GYRO_YOUT_H;
      reg_address_buf[1] = GYRO_YOUT_L;
      break;
    case IMU_GYRO_Z:
      reg_address_buf[0] = GYRO_ZOUT_H;
      reg_address_buf[1] = GYRO_ZOUT_L;
      break;
    default:
      break;
  }
  uint8_t data;
  int16_t gyro_data_temp = 0;
  if (reg_address_buf[0] != 0 &&  reg_address_buf[1] != 0) {
    if (read_spi(reg_address_buf[0], &data, 1) == SUCCESS) {
      gyro_data_temp = data << 8;
      if (read_spi(reg_address_buf[1], &data, 1) == SUCCESS) {
        gyro_data_temp |= data;
        *gyro_data = (float)gyro_data_temp / gyroUsedScaleFactor;
        returnStatus = SUCCESS;
      }
    }
  }
  return returnStatus;
}
/*
void imu_get_mag_calibration(FusionVector3* hard_iron, FusionRotationMatrix* soft_iron)
{
  // Values calculated from one calibration. Used when no calibration needed at start
  hard_iron->axis.x = 15.17855f;
  hard_iron->axis.y = -33.80356f;
  hard_iron->axis.z = -37.29715f;

  soft_iron->element.xx = 1.034842f;
  soft_iron->element.xy = 0.013539f;
  soft_iron->element.xz = 0.005519f;
  soft_iron->element.yx = 0.013539f;
  soft_iron->element.yy = 1.022150f;
  soft_iron->element.yz = 0.008887f;
  soft_iron->element.zx = 0.005519f;
  soft_iron->element.zy = 0.008887f;
  soft_iron->element.zz = 1.033490f;
}
*/
ErrorStatus imu_get_magnetometer_from_ext_reg(int16_t* mag_x, int16_t* mag_y, int16_t* mag_z)
{
  ErrorStatus returnStatus = ERROR;
  uint8_t data[13];
  if (select_bank(BANK_ZERO) == SUCCESS) {
    if (read_spi(EXT_SLV_SENSE_D0, data, 13) == SUCCESS) {
      returnStatus = SUCCESS;
      *mag_x = data[5] | data[6] << 8;
      *mag_y = data[7] | data[8] << 8;
      *mag_z = data[9] | data[10] << 8;
    }
  }
  return returnStatus;
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */
static ErrorStatus read_spi(uint8_t const reg,
                            uint8_t* const data,
                            uint16_t const length)
{
  ErrorStatus returnStatus = ERROR;
  GPIO_setOutputLowOnPin(cs.port, cs.pin);
  uint8_t regRead = 0x80 | (uint8_t)(reg); // 0x80 indicates a read

  if (xchg_spi(&regRead, NULL, 1) == SUCCESS) {
    if (xchg_spi(NULL, data, length) == SUCCESS) {
      returnStatus = SUCCESS;
    }
  }
  GPIO_setOutputHighOnPin(cs.port, cs.pin);
  return returnStatus;
}

static ErrorStatus write_spi(uint8_t reg, uint8_t* data, uint16_t length)
{
  ErrorStatus returnStatus = ERROR;
  GPIO_setOutputLowOnPin(cs.port, cs.pin);

  if (xchg_spi(&reg, NULL, 1) == SUCCESS) {
    if (xchg_spi(data, NULL, length) == SUCCESS) {
      returnStatus = SUCCESS;
    }
  }
  GPIO_setOutputHighOnPin(cs.port, cs.pin);
  return returnStatus;
}

static ErrorStatus select_bank(imu_register_bank bank)
{
  ErrorStatus returnStatus = ERROR;
  uint8_t bankTemp = (0x30 & (bank << 4));

  if (write_spi(REGISTER_BANK_ADDRESS, &bankTemp, 1) == SUCCESS) {
    uint8_t data = 0;
    if (read_spi(REGISTER_BANK_ADDRESS, &data, 1) == SUCCESS) {
      if ((0x30 & data) == (0x30 & (bank << 4))) {
        returnStatus = SUCCESS;
      }
    }
  }
  return returnStatus;
}

static ErrorStatus enable_sensor(bool_t acc, bool_t gyro) {
  ErrorStatus returnStatus = ERROR;
  uint8_t enableSensors = 0;
  if (!acc) {
    enableSensors |= 0x38;
  }
  if (!gyro) {
    enableSensors |= 0x07;
  }

  if (select_bank(BANK_ZERO) == SUCCESS) {
    if (write_spi(PWR_MGMT_2, &enableSensors, 1) == SUCCESS) {
      returnStatus = SUCCESS;
    }
  }
  return returnStatus;
}

static ErrorStatus set_sample_rate(float acc_smplr, float gyro_smplr) {
  uint8_t gyroDiv;
  float sampleRate;
  ErrorStatus returnStatus = ERROR;
  /* Calculate the sample rate divider */
  sampleRate = (1125.0 / gyro_smplr) - 1.0;

  /* Check if it fits in the divider register */
  if ( sampleRate > 255.0 ) {
    sampleRate = 255.0;
  }

  if ( sampleRate < 0 ) {
    sampleRate = 0.0;
  }

  /* Write the value to the register */
  gyroDiv = (uint8_t) sampleRate;
  if (select_bank(BANK_ZERO) == SUCCESS) {
    if (write_spi(GYRO_SMPLRT_DIV, &gyroDiv, 1) == SUCCESS) {
      returnStatus = SUCCESS;
    }
  }
  if (returnStatus == SUCCESS) {
    returnStatus = ERROR;
    uint16_t accDiv;

    /* Calculate the sample rate divider */
    sampleRate = (1125.0 / acc_smplr) - 1.0;

    /* Check if it fits in the divider registers */
    if ( sampleRate > 4095.0 ) {
      sampleRate = 4095.0;
    }

    if ( sampleRate < 0 ) {
      sampleRate = 0.0;
    }

    /* Write the value to the registers */
    accDiv = (uint16_t) sampleRate;
    uint8_t data = (uint8_t)((accDiv & 0xFF00) >> 8);
    if (write_spi(ACCEL_SMPLRT_DIV_1, &data, 1) == SUCCESS) {
      data = (uint8_t)(accDiv & 0xFF);
      if (write_spi(ACCEL_SMPLRT_DIV_1, &data, 1) == SUCCESS) {
        returnStatus = SUCCESS;
      }
    }
  }

  return returnStatus;
}

static ErrorStatus set_accel_bandwidth(uint8_t acc_bw)
{
  uint8_t reg;
  ErrorStatus returnStatus = ERROR;
  if (select_bank(BANK_TWO) == SUCCESS) {
    if (read_spi(ACCEL_CONFIG, &reg, 1) == SUCCESS) {
      reg &= ~ ICM20648_MASK_ACCEL_BW;
      reg |= acc_bw & ICM20648_MASK_ACCEL_BW;
      if (write_spi(ACCEL_CONFIG, &reg, 1) == SUCCESS) {
        returnStatus = SUCCESS;
      }
    }
  }
  return returnStatus;
}

static ErrorStatus set_gyro_bandwidth(uint8_t gyro_bw)
{
  uint8_t reg;
  ErrorStatus returnStatus = ERROR;
  if (select_bank(BANK_TWO) == SUCCESS) {
    if (read_spi(GYRO_CONFIG_1, &reg, 1) == SUCCESS) {
      reg &= ~ ICM20648_MASK_GYRO_BW;
      reg |= gyro_bw & ICM20648_MASK_GYRO_BW;
      if (write_spi(GYRO_CONFIG_1, &reg, 1) == SUCCESS) {
        returnStatus = SUCCESS;
      }
    }
  }

  return returnStatus;
}

static ErrorStatus get_accel_resolution(float *accelRes)
{
  uint8_t reg;
  ErrorStatus returnStatus = ERROR;
  /* Read the actual acceleration full scale setting */
  if (select_bank(BANK_TWO) == SUCCESS) {
     if (read_spi(ACCEL_CONFIG, &reg, 1) == SUCCESS) {
       reg &= ICM20648_MASK_ACCEL_FULLSCALE;
       returnStatus = SUCCESS;

      /* Calculate the resolution */
      switch (reg) {
        case ICM20648_ACCEL_FULLSCALE_2G:
          *accelRes = 2.0 / 32768.0;
          break;
        case ICM20648_ACCEL_FULLSCALE_4G:
          *accelRes = 4.0 / 32768.0;
          break;
        case ICM20648_ACCEL_FULLSCALE_8G:
          *accelRes = 8.0 / 32768.0;
          break;
        case ICM20648_ACCEL_FULLSCALE_16G:
          *accelRes = 16.0 / 32768.0;
          break;
      }
    }
  }
  return returnStatus;
}

static ErrorStatus get_gyro_resolution(float *gyroRes)
{
    uint8_t reg;
    ErrorStatus returnStatus = ERROR;
    /* Read the actual gyroscope full scale setting */
    if (select_bank(BANK_TWO) == SUCCESS) {
       if (read_spi(GYRO_CONFIG_1, &reg, 1) == SUCCESS) {
         reg &= ICM20648_MASK_GYRO_FULLSCALE;
         returnStatus = SUCCESS;

        /* Calculate the resolution */
        switch ( reg ) {
          case ICM20648_GYRO_FULLSCALE_250DPS:
            *gyroRes = 250.0 / 32768.0;
            break;
          case ICM20648_GYRO_FULLSCALE_500DPS:
            *gyroRes = 500.0 / 32768.0;
            break;
          case ICM20648_GYRO_FULLSCALE_1000DPS:
            *gyroRes = 1000.0 / 32768.0;
            break;
          case ICM20648_GYRO_FULLSCALE_2000DPS:
            *gyroRes = 2000.0 / 32768.0;
            break;
          }
       }
    }
    return returnStatus;
}
static ErrorStatus calibrate(float *accelBiasScaled, float *gyroBiasScaled)
{
  ErrorStatus returnStatus = ERROR;
  uint8_t data[12];
  uint16_t i, packetCount, fifoCount;
  int32_t gyroBias[3] = { 0, 0, 0 };
  int32_t accelBias[3] = { 0, 0, 0 };
  int32_t accelTemp[3];
  int32_t gyroTemp[3];
  int32_t accelBiasFactory[3];
  int32_t gyroBiasStored[3];
  float gyroRes, accelRes;
  systime_wait_ms(192);
  systime_wait_ms(1);
  systime_wait_ms(1);
  systime_wait_ms(2);
  /* Enable the accelerometer and the gyro */
  enable_sensor(TRUE, TRUE);

  /* Set 1kHz sample rate */
  set_sample_rate(1100.0, 1100.0);

  /* 246Hz BW for the accelerometer and 200Hz for the gyroscope */
  set_accel_bandwidth(ICM20648_ACCEL_BW_246HZ);
  set_gyro_bandwidth(ICM20648_GYRO_BW_12HZ);

  /* Retrieve the resolution per bit */
  get_accel_resolution(&accelRes);
  get_gyro_resolution(&gyroRes);

  /* The accel sensor needs max 30ms, the gyro max 35ms to fully start */
  /* Experiments show that the gyro needs more time to get reliable results */
  systime_wait_ms(50);
  uint8_t dataR = 0;
  /* Disable the FIFO */
  if (select_bank(BANK_ZERO) == SUCCESS) {
    if (read_spi(USER_CTRL, &dataR, 1) == SUCCESS) {
      dataR &= ~0x40;
      if (write_spi(USER_CTRL, &dataR, 1) == SUCCESS) {
        dataR = 0x0F;
        if (write_spi(FIFO_MODE, &dataR, 1) == SUCCESS) {
          dataR = 0x1E;
          if (write_spi(FIFO_EN_2, &dataR, 1) == SUCCESS) {
            dataR = 0x0F;
            if (write_spi(FIFO_RST, &dataR, 1) == SUCCESS) {
              dataR = 0x00;
              if (write_spi(FIFO_RST, &dataR, 1) == SUCCESS) {
                if (read_spi(USER_CTRL, &dataR, 1) == SUCCESS) {
                  dataR |= 0x40;
                  if (write_spi(USER_CTRL, &dataR, 1) == SUCCESS) {
                    dataR = 0;
                    if (read_spi(USER_CTRL, &dataR, 1) == SUCCESS) {
                      returnStatus = SUCCESS;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  /* The max FIFO size is 4096 bytes, one set of measurements takes 12 bytes */
  /* (3 axes, 2 sensors, 2 bytes each value ) 340 samples use 4080 bytes of FIFO */
  /* Loop until at least 4080 samples gathered */
  fifoCount = 0;
  systime_wait_ms(50);
  systime_wait_ms(50);
  systime_wait_ms(50);
  read_spi(FIFO_COUNTH, &data[0], 2);
  fifoCount = ( (uint16_t) (data[0] << 8) | data[1]);
  systime_wait_ms(50);
  read_spi(FIFO_COUNTH, &data[0], 2);
  fifoCount = ( (uint16_t) (data[0] << 8) | data[1]);
  systime_wait_ms(50);
  read_spi(FIFO_COUNTH, &data[0], 2);
  fifoCount = ( (uint16_t) (data[0] << 8) | data[1]);
  while ( fifoCount < 4080 ) {
    systime_wait_ms(50);
    /* Read FIFO sample count */
    read_spi(FIFO_COUNTH, &data[0], 2);
    /* Convert to a 16 bit value */
    fifoCount = ( (uint16_t) (data[0] << 8) | data[1]);
  }

  /* Disable accelerometer and gyro to store the data in FIFO */
  dataR = 0x00;
  write_spi(FIFO_EN_2, &dataR, 1);

  /* Read FIFO sample count */
  read_spi(FIFO_COUNTH, &data[0], 2);

  /* Convert to a 16 bit value */
  fifoCount = ( (uint16_t) (data[0] << 8) | data[1]);

  /* Calculate the number of data sets (3 axis of accel an gyro, two bytes each = 12 bytes) */
  packetCount = fifoCount / 12;

  /* Retrieve the data from the FIFO */
  for ( i = 0; i < packetCount; i++ ) {
    read_spi(FIFO_R_W, &data[0], 12);
    /* Convert to 16 bit signed accel and gyro x,y and z values */
    accelTemp[0] = ( (int16_t) (data[0] << 8) | data[1]);
    accelTemp[1] = ( (int16_t) (data[2] << 8) | data[3]);
    accelTemp[2] = ( (int16_t) (data[4] << 8) | data[5]);
    gyroTemp[0] = ( (int16_t) (data[6] << 8) | data[7]);
    gyroTemp[1] = ( (int16_t) (data[8] << 8) | data[9]);
    gyroTemp[2] = ( (int16_t) (data[10] << 8) | data[11]);

    /* Sum the values */
    accelBias[0] += accelTemp[0];
    accelBias[1] += accelTemp[1];
    accelBias[2] += accelTemp[2];
    gyroBias[0] += gyroTemp[0];
    gyroBias[1] += gyroTemp[1];
    gyroBias[2] += gyroTemp[2];
  }

  /* Divide by packet count to get the average */
  accelBias[0] /= packetCount;
  accelBias[1] /= packetCount;
  accelBias[2] /= packetCount;
  gyroBias[0] /= packetCount;
  gyroBias[1] /= packetCount;
  gyroBias[2] /= packetCount;

  accelBiasScaled[0] = accelBias[0];
  accelBiasScaled[1] = accelBias[1];
  accelBiasScaled[2] = accelBias[2];

  /* Accelerometer: add or remove (depending on the orientation of the chip) 1G (gravity) from the Z axis value */
  if ( accelBias[2] > 0L ) {
      accelBias[2] -= (int32_t) (1.0 / accelRes);
  } else {
      accelBias[2] += (int32_t) (1.0 / accelRes);
  }

  /* Convert the values to degrees per sec for displaying */
  gyroBiasScaled[0] = (float) gyroBias[0] * gyroRes;
  gyroBiasScaled[1] = (float) gyroBias[1] * gyroRes;
  gyroBiasScaled[2] = (float) gyroBias[2] * gyroRes;

  /* Read stored gyro trim values. After reset these values are all 0 */
  if (select_bank(BANK_TWO) == SUCCESS) {
    read_spi(XG_OFFS_USRH, &data[0], 2);
    gyroBiasStored[0] = ( (int16_t) (data[0] << 8) | data[1]);
    read_spi(YG_OFFS_USRH, &data[0], 2);
    gyroBiasStored[1] = ( (int16_t) (data[0] << 8) | data[1]);
    read_spi(ZG_OFFS_USRH, &data[0], 2);
    gyroBiasStored[2] = ( (int16_t) (data[0] << 8) | data[1]);
  }

  /* The gyro bias should be stored in 1000dps full scaled format. We measured in 250dps to get */
  /* the best sensitivity, so need to divide by 4 */
  /* Substract from the stored calibration value */
  gyroBiasStored[0] -= gyroBias[0] / 4;
  gyroBiasStored[1] -= gyroBias[1] / 4;
  gyroBiasStored[2] -= gyroBias[2] / 4;

  /* Split the values into two bytes */
  data[0] = (gyroBiasStored[0] >> 8) & 0xFF;
  data[1] = (gyroBiasStored[0]) & 0xFF;
  data[2] = (gyroBiasStored[1] >> 8) & 0xFF;
  data[3] = (gyroBiasStored[1]) & 0xFF;
  data[4] = (gyroBiasStored[2] >> 8) & 0xFF;
  data[5] = (gyroBiasStored[2]) & 0xFF;

  /* Write the  gyro bias values to the chip */
  write_spi(XG_OFFS_USRH, &data[0], 1);
  write_spi(XG_OFFS_USRL, &data[1], 1);
  write_spi(YG_OFFS_USRH, &data[2], 1);
  write_spi(YG_OFFS_USRL, &data[3], 1);
  write_spi(ZG_OFFS_USRH, &data[4], 1);
  write_spi(ZG_OFFS_USRL, &data[5], 1);


  /* Turn off FIFO */
  dataR = 0;
  if (select_bank(BANK_ZERO) == SUCCESS) {
    if (write_spi(USER_CTRL, &dataR, 1) == SUCCESS) {
      returnStatus = SUCCESS;
    }
  }
  /* Disable all sensors */
  //enable_sensor(FALSE, FALSE);

  return returnStatus;
}
#ifdef ICM-20948

static ErrorStatus imu_magnetometer_hard_soft_iron_calib()
{
  ErrorStatus returnStatus = ERROR;
  // Magneotmeter shall be on
  uint16_t sampleCount = 3000;
  int32_t magBias[3] = {0, 0, 0}, magScale[3] = {0, 0, 0};
  int16_t magMax[3] = {-32767, -32767, -32767}, magMin[3] = {32767, 32767, 32767}, magTemp[3] = {0, 0, 0};
  uint16_t i = 0;
  for (i; i<sampleCount; ++i) {
    imu_get_magnetometer_from_ext_reg(&magTemp[0], &magTemp[1], &magTemp[2]);
    int j = 0;
    for (j; j < 3; j++) {
      if(magTemp[j] > magMax[j]) {
        magMax[j] = magTemp[j];
      }
      if(magTemp[j] < magMin[j]) {
        magMin[j] = magTemp[j];
      }
    }
    systime_wait_ms(10); // Delay 10ms between measurement;
  }
  magBias[0]  = (magMax[0] + magMin[0])/2;  // get average x mag bias in counts
  magBias[1]  = (magMax[1] + magMin[1])/2;  // get average y mag bias in counts
  magBias[2]  = (magMax[2] + magMin[2])/2;  // get average z mag bias in counts

  // Get soft iron correction estimate
   magScale[0]  = (magMax[0] - magMin[0])/2;  // get average x axis max chord length in counts
   magScale[1]  = (magMax[1] - magMin[1])/2;  // get average y axis max chord length in counts
   magScale[2]  = (magMax[2] - magMin[2])/2;  // get average z axis max chord length in counts

   mag_hardi_x_calib = (float) magBias[0] * MAG_RES;
   mag_hardi_y_calib = (float) magBias[1] * MAG_RES;
   mag_hardi_z_calib = (float) magBias[2] * MAG_RES;

   float avg_rad = magScale[0] + magScale[1] + magScale[2];
   avg_rad /= 3.0;

   mag_softi_x_calib = avg_rad/((float)magScale[0]);
   mag_softi_y_calib = avg_rad/((float)magScale[1]);
   mag_softi_z_calib = avg_rad/((float)magScale[2]);
}



static ErrorStatus imu_enable_magnetometer()
{
  //imu_stop_ext0();
  ErrorStatus returnStatus = ERROR;
  uint8_t data = 0x18;
  if (select_bank(BANK_THREE) == SUCCESS) {
    write_spi(I2C_MASTER_CTRL, &data, 1);
    data = 0x04;
    write_spi(I2C_MASTER_ODR_CONFIG, &data, 1);

    //data = 0x01;
    //imu_read_write_magnetometer_i2c_one_byte(0x0C, (uint8_t)CTRL3, &data, FALSE);
    //systime_wait_ms(10);
    data = (uint8_t)MAG_MODE4_100Hz;
    if (imu_read_write_magnetometer_i2c_one_byte(0x0C, (uint8_t)CTRL2, &data, FALSE) == SUCCESS) {
      systime_wait_ms(1);
      if ( imu_set_magnetometer_i2c_register(0x0C, (uint8_t)MAG_WHO_AM_I) == SUCCESS) {
        if (imu_trigger_read_data_magnetometer_i2c(0x0C, 13) == SUCCESS) {
          returnStatus = SUCCESS;
        }
      }
    }
  }
  return returnStatus;
}

static ErrorStatus imu_stop_ext0()
{
  ErrorStatus returnStatus = ERROR;
  if (select_bank(BANK_THREE) == SUCCESS) {
    if (write_spi(I2C_SLV0_CTRL, 0x00, 1) == SUCCESS) {
      returnStatus = SUCCESS;
    }
  }
  return returnStatus;
}

static ErrorStatus imu_trigger_read_data_magnetometer_i2c(uint8_t address, uint8_t nr_of_bytes)
{
  ErrorStatus returnStatus = ERROR;
  if (select_bank(BANK_THREE) == SUCCESS) {
    address = address | 0x80;
    if (write_spi(I2C_SLV0_ADDR, &address, 1) == SUCCESS) {
      nr_of_bytes |= 0xA0; // EN and no register write
      if (write_spi(I2C_SLV0_CTRL, &nr_of_bytes, 1) == SUCCESS) {
        returnStatus = SUCCESS;
      }
    }
  }
  return returnStatus;
}

static ErrorStatus imu_set_magnetometer_i2c_register(uint8_t address, uint8_t reg)
{
  ErrorStatus returnStatus = ERROR;
  if (select_bank(BANK_THREE) == SUCCESS) {
    address = address;
    if (write_spi(I2C_SLV4_ADDR, &address, 1) == SUCCESS) {
      write_spi(I2C_SLV4_DO, &reg, 1);
      uint8_t ctrl = 0xA0;
      if (write_spi(I2C_SLV4_CTRL, &ctrl, 1) == SUCCESS ) {
        if (select_bank(BANK_ZERO) == SUCCESS) {
          uint8_t status = 0;
          while((status & 0x40) == 0) { // TODO (CAER) add timeout
            read_spi(I2C_MST_STATUS, &status, 1);
          }
          returnStatus = SUCCESS;
        }
      }
    }
  }
  return returnStatus;
}

static ErrorStatus imu_read_write_magnetometer_i2c_one_byte(uint8_t address, uint8_t reg, uint8_t* data, bool_t read)
{
  ErrorStatus returnStatus = ERROR;
  if (select_bank(BANK_THREE) == SUCCESS) {
    address = (((read) ? 0x80 : 0x00) | address);
    if (write_spi(I2C_SLV4_ADDR, &address, 1) == SUCCESS) {
      if (!read) {
        if (write_spi(I2C_SLV4_REG, &reg, 1) == SUCCESS) {
          if (write_spi(I2C_SLV4_DO, data, 1) == SUCCESS) {
            returnStatus = SUCCESS;
          }
        }
      }
      uint8_t ctrl;
      ctrl = 0x80;
      if (write_spi(I2C_SLV4_CTRL, &ctrl, 1) == SUCCESS ) {
        returnStatus = SUCCESS;
      } else {
        returnStatus = ERROR;
      }
      if (read) {
        returnStatus = ERROR;
        if (select_bank(BANK_ZERO) == SUCCESS) {
          uint8_t status = 0;
          while((status & 0x40) == 0) {
            read_spi(I2C_MST_STATUS, &status, 1);
          }
          if ((status & 0x40) != 0) {
            if (select_bank(BANK_THREE) == SUCCESS) {
              if (read_spi(I2C_SLV4_DI, data, 1) == SUCCESS)
                returnStatus = SUCCESS;
            }
          }
        }
      }
    }
  }
  return returnStatus;
}


static ErrorStatus imu_write_magnetometer_i2c(uint8_t address, uint8_t reg, uint8_t nr_of_bytes, uint8_t data)
{
  ErrorStatus returnStatus = ERROR;
  uint8_t datat = 0;
  if (select_bank(BANK_THREE) == SUCCESS) {
    datat = 0x10 | 0x08;
    if (write_spi(I2C_MASTER_CTRL, &datat, 1) == SUCCESS)
      if (write_spi(I2C_SLV0_ADDR, &address, 1) == SUCCESS) {
        read_spi(I2C_SLV0_ADDR, &datat, 1);
        if (write_spi(I2C_SLV0_REG, &reg, 1) == SUCCESS) {
          read_spi(I2C_SLV0_REG, &datat, 1);
          if (write_spi(I2C_SLV0_DO, &data, 1) == SUCCESS) {
            read_spi(I2C_SLV0_DO, &datat, 1);
            nr_of_bytes |= 0x80;
            if (write_spi(I2C_SLV0_CTRL, &nr_of_bytes, 1) == SUCCESS) {
              read_spi(I2C_SLV0_CTRL, &datat, 1);
              returnStatus = SUCCESS;
            }
          }
        }
      }
  }
  return returnStatus;
}
#endif
///**
//*  @brief      Read data from a register in DMP memory
//*  @param[in]  DMP memory address
//*  @param[in]  number of byte to be read
//*  @param[in]  input data from the register
//*  @return     0 if successful.
//*/
//static ErrorStatus dmp_read_mems(unsigned short reg, unsigned int length, unsigned char *data)
//{
//  ErrorStatus returnStatus = ErrorStatusRROR;
//  unsigned int bytesWritten = 0;
//  unsigned int thisLen;
//  unsigned char lBankSelected;
//  unsigned char lStartAddrSelected;
//
//  if(!data)
//      return returnStatus;
//
//  if (select_bank(BANK_ZERO) == SUCCESS) {
//    uint8_t data = 0;
//    if (read_spi(PWR_MGMT_1, &data, 1) == SUCCESS) {
//      data &= ~0x40; // Sleep disable
//      data &= ~0x20; // LP disabled
//      if (write_spi(PWR_MGMT_1, &data, 1) == SUCCESS) {
//        returnStatus == SUCCESS;
//      }
//    }
//  }
//
//  lBankSelected = (reg >> 8);
//  if (lBankSelected != lLastBankSelected)
//  {
//    returnStatus = write_spi(0x7E, &lBankSelected, 1);
//    if (returnStatus == ErrorStatusRROR) {
//      return returnStatus;
//    }
//    lLastBankSelected = lBankSelected;
//  }
//  while (bytesWritten < length)
//  {
//      lStartAddrSelected = (reg & 0xff);
//      /* Sets the starting read or write address for the selected memory, inside of the selected page (see MEM_SEL Register).
//         Contents are changed after read or write of the selected memory.
//         This register must be written prior to each access to initialize the register to the proper starting address.
//         The address will auto increment during burst transactions.  Two consecutive bursts without re-initializing the start address would skip one address. */
//      returnStatus = write_spi(0x7C, &lStartAddrSelected, 1);
//      if (returnStatus == ErrorStatusRROR) {
//        return returnStatus;
//      }
//      thisLen = min(INV_MAX_SERIAL_READ, length-bytesWritten);
//
//        /* Read data */
//      returnStatus = read_spi(0x7D, &data[bytesWritten], thisLen);
//
//      if (returnStatus == ErrorStatusRROR) {
//        return returnStatus;
//      }
//
//        bytesWritten += thisLen;
//        reg += thisLen;
//    }
//
//  return returnStatus;
//}
//
///**
//*  @brief       Write data to a register in DMP memory
//*  @param[in]   DMP memory address
//*  @param[in]   number of byte to be written
//*  @param[out]  output data from the register
//*  @return     0 if successful.
//*/
//ErrorStatus dmp_write_mems(unsigned short reg, unsigned int length, const unsigned char *data)
//{
//  ErrorStatus returnStatus = ErrorStatusRROR;
//  unsigned int bytesWritten = 0;
//  unsigned int thisLen;
//  unsigned char lBankSelected;
//  unsigned char lStartAddrSelected;
//
//  if(!data)
//      return ErrorStatusRROR;
//  if (select_bank(BANK_ZERO) == SUCCESS) {
//    uint8_t data = 0;
//    if (read_spi(PWR_MGMT_1, &data, 1) == SUCCESS) {
//      data &= ~0x40; // Sleep disable
//      data &= ~0x20; // LP disabled
//      if (write_spi(PWR_MGMT_1, &data, 1) == SUCCESS) {
//        returnStatus = SUCCESS;
//      }
//    }
//  }
//  if (returnStatus == SUCCESS) {
//    lBankSelected = (reg >> 8);
//      if (lBankSelected != lLastBankSelected)
//      {
//        returnStatus = write_spi(0x7E, &lBankSelected, 1);
//        lLastBankSelected = lBankSelected;
//      }
//      if (returnStatus == SUCCESS) {
//        while (bytesWritten < length)
//        {
//            lStartAddrSelected = (reg & 0xff);
//            /* Sets the starting read or write address for the selected memory, inside of the selected page (see MEM_SEL Register).
//               Contents are changed after read or write of the selected memory.
//               This register must be written prior to each access to initialize the register to the proper starting address.
//               The address will auto increment during burst transactions.  Two consecutive bursts without re-initializing the start address would skip one address. */
//            returnStatus = write_spi(0x7C, &lStartAddrSelected, 1);
//            if (returnStatus == ErrorStatusRROR) {
//              break;
//            }
//
//            thisLen = min(INV_MAX_SERIAL_WRITE, length-bytesWritten);
//
//            /* Write data */
//            returnStatus = write_spi(0x7D, &data[bytesWritten], thisLen);
//            if (returnStatus == ErrorStatusRROR) {
//              break;
//            }
//
//            bytesWritten += thisLen;
//            reg += thisLen;
//        }
//      }
//  }
//
//    return returnStatus;
//}
//
//ErrorStatus dmp_firmware_load(const unsigned char *data_start, unsigned short size_start, unsigned short load_addr)
//{
//    int write_size;
//    ErrorStatus returnStatus = ErrorStatusRROR;
//    unsigned short memaddr;
//    const unsigned char *data;
//    unsigned short size;
//    uint8_t data_cmp[INV_MAX_SERIAL_READ+1];
//
//    int flag = 0;
//    (void)memset(data_cmp, 0, (int16_t)(INV_MAX_SERIAL_READ + 1));
//    // Write DMP memory
//    data = data_start;
//    size = size_start;
//    memaddr = load_addr;
//    while (size > 0) {
//        write_size = min(size, INV_MAX_SERIAL_WRITE);
//        if ((memaddr & 0xff) + write_size > 0x100) {
//            // Moved across a bank
//            write_size = (memaddr & 0xff) + write_size - 0x100;
//        }
//        returnStatus = dmp_write_mems(memaddr, write_size, (unsigned char *)data);
//        if (returnStatus == ErrorStatusRROR) {
//          return returnStatus;
//        }
//
//        data += write_size;
//        size -= write_size;
//        memaddr += write_size;
//    }
//    //memcmp(data_cmp, data, write_size);
//    // Verify DMP memory
//
//    data = data_start;
//    size = size_start;
//    memaddr = load_addr;
//    while (size > 0) {
//        write_size = min(size, INV_MAX_SERIAL_READ);
//        if ((memaddr & 0xff) + write_size > 0x100) {
//            // Moved across a bank
//            write_size = (memaddr & 0xff) + write_size - 0x100;
//        }
//        returnStatus = dmp_read_mems(memaddr, write_size, data_cmp);
//        if (returnStatus == ErrorStatusRROR) {
//          flag++; // Error, DMP not written correctly
//        }
//
//        if (memcmp(data_cmp, data, write_size))
//            return ErrorStatusRROR;
//        data += write_size;
//        size -= write_size;
//        memaddr += write_size;
//    }
//
//    return SUCCESS;
//}


static ErrorStatus xchg_spi(uint8_t* tx_data,
                            uint8_t* rx_data,
                            uint16_t nr_of_bytes)
{
  ErrorStatus returnStatus = SUCCESS;
  if (rx_data == NULL) {
    for (nr_of_bytes; nr_of_bytes > 0; nr_of_bytes--) {
      SPI_transmitData(spiPointer->module, *tx_data++) ;
      while(SPI_isBusy(spiPointer->module) == EUSCI_SPI_BUSY);  // TODO (caer) add timeout
    }
  } else {
    for (nr_of_bytes; nr_of_bytes > 0; nr_of_bytes--) {
      SPI_clearInterruptFlag(spiPointer->module, EUSCI_SPI_RECEIVE_INTERRUPT);
      SPI_transmitData(spiPointer->module, 0xFF) ;
      while(SPI_getInterruptStatus(spiPointer->module, EUSCI_SPI_RECEIVE_INTERRUPT) !=
          EUSCI_SPI_RECEIVE_INTERRUPT); // TODO (caer) add timeout
      *rx_data++ = SPI_receiveData(spiPointer->module);
    }
  }
  return returnStatus;
}

/*
 * End of file: ICM_20948.c
 */
