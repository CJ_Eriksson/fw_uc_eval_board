/**
 * @file ICM_20948.h
 * @brief Driver for the 9-axis IMU
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _ICM_20948_H_
#define _ICM_20948_H_

#include "../../Hal/spi.h"
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */
#ifndef min
#define min(x,y)    (((x)<(y))?(x):(y))
#endif

#ifndef max
#define max(x,y)    (((x)>(y))?(x):(y))
#endif

#define INV_MAX_SERIAL_READ 16
#define INV_MAX_SERIAL_WRITE 16


/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */
typedef enum {
  IMU_STATUS_SUCCESS,
  IMU_STATUS_ERROR
} imu_status_e;

typedef enum {
  IMU_ACC_X,
  IMU_ACC_Y,
  IMU_ACC_Z
} imu_acc_axes_e;

typedef enum {
  IMU_GYRO_X,
  IMU_GYRO_Y,
  IMU_GYRO_Z
} imu_gyro_axes_e;

typedef enum {
  IMU_ACC_SENSITIVITY_2G,
  IMU_ACC_SENSITIVITY_4G,
  IMU_ACC_SENSITIVITY_8G,
  IMU_ACC_SENSITIVITY_16G,
} imu_acc_sensitivity_e;

typedef enum {
  IMU_GYRO_SENSITIVITY_250dps,
  IMU_GYRO_SENSITIVITY_500dps,
  IMU_GYRO_SENSITIVITY_1000dps,
  IMU_GYRO_SENSITIVITY_2000dps,
} imu_gyro_sensitivity_e;
/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus imu_init(spi_s * const spi_p,
                      uint_fast8_t cs_port,
                      uint_fast16_t cs_pin,
                      imu_acc_sensitivity_e acc_sensitivity,
                      bool_t acc_dlp_enable,
                      imu_gyro_sensitivity_e gyro_sensitivity,
                      bool_t gyro_dlp_enable);
ErrorStatus imu_check_data_ready();
ErrorStatus imu_get_sensitivity(float* const acc, float* const gyro);
ErrorStatus imu_read_acc(imu_acc_axes_e axes, float* const acc_data);
ErrorStatus imu_read_gyro(imu_gyro_axes_e axes, float* const gyro_data);
//void imu_get_mag_calibration(FusionVector3* hard_iron, FusionRotationMatrix* soft_iron);
ErrorStatus imu_get_magnetometer_from_ext_reg(int16_t* mag_x, int16_t* mag_y, int16_t* mag_z);
#ifdef __cplusplus
}
#endif

#endif /* _ICM_20948_H_ */

/*
 * End of file: ICM_20948.h
 */
