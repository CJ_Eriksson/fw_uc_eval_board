/**
 * @file Button.c
 * @brief Driver for button
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Board/board.h"
#include "../Time/Systime.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "Button.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define DEBOUNCE_TIME 20;
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */
typedef enum {
  IDLE,
  DEBOUNCE,
  HIGH,
  LOW,
  HIGHLOW_IDLE
}button_state_e;

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static gpio_s button1;
static gpio_s button2;
static uint8_t button1Last;
static uint8_t button2Last;
static void (*callbackButton1Press)(void);
static void (*callbackButton1Release)(void);
static void (*callbackButton2Press)(void);
static void (*callbackButton2Release)(void);
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
ErrorStatus button_init(uint_fast8_t button1_port,
                        uint_fast16_t button1_pin,
                        uint_fast8_t button2_port,
                        uint_fast16_t button2_pin,
                        void (*cb_b1_press)(void),
                        void (*cb_b1_release)(void),
                        void (*cb_b2_press)(void),
                        void (*cb_b2_release)(void))
{
  ErrorStatus returnStatus = SUCCESS;
  button1.port = button1_port;
  button1.pin = button1_pin;
  button2.port = button2_port;
  button2.pin = button2_pin;
  button1Last = GPIO_getInputPinValue(button1.port, button1.pin);
  button2Last = GPIO_getInputPinValue(button2.port, button2.pin);
  callbackButton1Press = cb_b1_press;
  callbackButton1Release = cb_b1_release;
  callbackButton2Press = cb_b2_press;
  callbackButton2Release = cb_b2_release;
  return returnStatus;
}

ErrorStatus button_poll()
{
  static uint32_t debounceTick1Stop;
  static uint32_t debounceTick2Stop;
  static button_state_e button1SM = IDLE;
  static button_state_e button2SM = IDLE;
  uint8_t button1Now = GPIO_getInputPinValue(button1.port, button1.pin);
  switch (button1SM) {
    case IDLE:
      button1SM = DEBOUNCE;
      debounceTick1Stop= systime_get_tick() + DEBOUNCE_TIME;
      break;
    case DEBOUNCE:
      if (button1Last != button1Now) {
        button1SM = IDLE;
      } else if ((int16_t)(systime_get_tick() - debounceTick1Stop) > 0) {
        if (button1Now == GPIO_INPUT_PIN_HIGH) {
          button1SM = HIGH;
        } else {
          button1SM = LOW;
        }
      }
      break;
    case HIGH:
      button1SM = HIGHLOW_IDLE;
      if (callbackButton1Press != NULL) {
        callbackButton1Press();
      }
      break;
    case LOW:
      button1SM = HIGHLOW_IDLE;
      if (callbackButton1Release != NULL) {
        callbackButton1Release();
      }
      break;
    case HIGHLOW_IDLE:
      if (button1Last != button1Now) {
        button1SM = IDLE;
      }
      break;
  }
  button1Last = button1Now;

  uint8_t button2Now = GPIO_getInputPinValue(button2.port, button2.pin);
  switch (button2SM) {
    case IDLE:
      button2SM = DEBOUNCE;
      debounceTick2Stop= systime_get_tick() + DEBOUNCE_TIME;
      break;
    case DEBOUNCE:
      if (button2Last != button2Now) {
        button2SM = IDLE;
      } else if ((int16_t)(systime_get_tick() - debounceTick2Stop) > 0) {
        if (button2Now == GPIO_INPUT_PIN_HIGH) {
          button2SM = HIGH;
        } else {
          button2SM = LOW;
        }
      }
      break;
    case HIGH:
      button2SM = HIGHLOW_IDLE;
      if (callbackButton2Press != NULL) {
        callbackButton2Press();
      }
      break;
    case LOW:
      button2SM = HIGHLOW_IDLE;
      if (callbackButton2Release != NULL) {
        callbackButton2Release();
      }
      break;
    case HIGHLOW_IDLE:
      if (button2Last != button1Now) {
        button2SM = IDLE;
      }
      break;
  }
  button2Last = button2Now;
  return SUCCESS;
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */

/*
 * End of file: Button.c
 */
