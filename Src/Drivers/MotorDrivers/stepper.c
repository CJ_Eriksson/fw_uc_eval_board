/**
 * @file stepper.c
 * @brief Helper functions for steppper motors using dir. step and PWM Vref as
 *        current control
 *
 * @copyright
 *        Unpublished Work Copyright 2021 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Common/common_types.h"
#include <math.h>
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "stepper.h"
#include "../BEMF/BEMF_Sensor.h"
#include "../ADC/ADC.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define ZERO_BEMF 300
#define BEMF_ZEROS 2
#define STEP_ON_TIME_FACTOR 0.00889 // ON for STEP
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
  static uint8_t stepSizeBemf = 1;
  static uint8_t stepSizeBemfMeas1 = 1;
  static uint8_t stepSizeBemfMeas2 = 1;
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */
  bool_t bemf_home_check_zero();
  bool_t bemf_home_check_derivative();
/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */

/**
 * @brief Set PWM duty cycle of stepper Vref timer to specific duty cycle.
 *        The duty cycle corresponds to a current Voltage level that in turn
 *        corrensponds to a current level for the stepper motor
 *
 * @param [in] vref_timer - Timer for Vref PWM signal
 *        [in] duty_cycle - Duty cycle for the PWM signal
 * @return SUCCESS on SUCCESS
 */
ErrorStatus stepper_set_current(timer_s* vref_timer, float duty_cycle)
{
  ErrorStatus returnStatus = SUCCESS;
  Timer_A_setCompareValue(vref_timer->timer,
                          vref_timer->pwmModeConfig.compareRegister,
                          MIN((uint16_t)vref_timer->pwmModeConfig.timerPeriod,
                              (uint16_t)vref_timer->pwmModeConfig.timerPeriod *
                              duty_cycle));
  return returnStatus;
}

/**
 * @brief Set step mode used for stepper. Only used for BEMF measurement
 *
 * @param [in] step_size - Step size used by driver
 * @return SUCCESS on SUCCESS
 */
ErrorStatus stepper_set_step_size(stepper_step_mode_e step_size)
{
  ErrorStatus returnStatus = SUCCESS;
  // TODO (caer) Move
  switch (step_size) {
    case STEPPER_1_2_STEP:
      stepSizeBemf = 8;
      stepSizeBemfMeas1 = 6;
      stepSizeBemfMeas2 = 4;
      break;
    case STEPPER_1_4_STEP:
      stepSizeBemf = 16;
      stepSizeBemfMeas1 = 13;
      stepSizeBemfMeas2 = 7;
      break;
    case STEPPER_1_8_STEP:
      stepSizeBemf = 32;
      stepSizeBemfMeas1 = 27;
      stepSizeBemfMeas2 = 13;
      break;
  }
  return returnStatus;
}
/**
 * @brief ISR function for stepper that shall be called at defined frequency
 *        for the stepper function. The function compares the actual step
 *        position with the calculated trajectory and returns true if a step
 *        shall be taken.
 *
 * @param [in] current_step - current actual step for the stepper motor
 *        [in] trajectory - Trajectory instance for the stepper motor
 *        [in] step_pin - GPIO for step
 *        [in] dir_pin - GPIO for direction
 *        [in] stepper_mode Step size
 * @return SUCCESS on SUCCESS
 */
ErrorStatus stepper_step(int32_t* current_step, trajectory_s* trajectory, gpio_s step_io, gpio_s dir_io, stepper_step_mode_e stepper_mode, stepper_state_e* stepper_state)
{
  volatile static uint32_t enableCnt = 0;
  static bool_t first = TRUE;
  static bool_t bemfRunning = FALSE;
  volatile uint8_t dummy = 100;
  ErrorStatus returnStatus = ERROR;
  int32_t distance =
      *current_step - trajectory->position;
  static bool_t currDir;
  if (distance < 0) {
    currDir = TRUE;
  } else if (distance > 0) {
    currDir = FALSE;
  }

  if ((uint8_t)currDir != GPIO_getOutputPinValue(dir_io.port, dir_io.pin)) {
    if (currDir == GPIO_OUTPUT_PIN_LOW) {
      GPIO_setOutputLowOnPin(dir_io.port, dir_io.pin);
    } else {
      GPIO_setOutputHighOnPin(dir_io.port, dir_io.pin);
    }
  } else if (distance < 0) {
    enableCnt = (uint32_t)((float)STEP_FREQUENCY * STEP_ON_TIME_FACTOR);
    GPIO_setOutputHighOnPin(step_io.port, step_io.pin);
    GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_3);
    if (first == FALSE) {
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      GPIO_setOutputLowOnPin(step_io.port, step_io.pin);
      GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_3);
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      GPIO_setOutputHighOnPin(step_io.port, step_io.pin);
      GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_3);
    } else {
      first = FALSE;
    }

    //if (trajectory->speed == trajectory->maxSpeed) {
      if(*current_step%stepSizeBemf == stepSizeBemfMeas1) {
        bemf_start_meas(A10);
        //GPIO_setOutputHighOnPin(10,0x04);
        bemfRunning = TRUE;
      }  else if (bemfRunning){
        bemf_stop_meas();
        bemfRunning = FALSE;
        if (*stepper_state == STEPPER_HOMING) {
          //if (trajectory->speed == trajectory->maxSpeed) {
            if (bemf_home_check_zero()) {
              trajectory->target = trajectory->position;
              trajectory->speed = 0.0f;
              *stepper_state = STEPPER_NORMAL;
            }
          //}
        }

        //GPIO_setOutputLowOnPin(10,0x04);
      } else if (*current_step%16 == 10) {
        bemf_set_conversion_rate(trajectory->speed);
      }
   // }
    *current_step += 1;

  } else if (distance != 0) {
    enableCnt = (uint32_t)((float)STEP_FREQUENCY * STEP_ON_TIME_FACTOR);
    GPIO_setOutputHighOnPin(step_io.port, step_io.pin);
    GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_3);
    if (first == FALSE) {
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      GPIO_setOutputLowOnPin(step_io.port, step_io.pin);
      GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_3);
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      dummy /= 5;
      GPIO_setOutputHighOnPin(step_io.port, step_io.pin);
      GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_3);
    } else {
      first = FALSE;
    }
    //if (trajectory->speed == -trajectory->maxSpeed) {
      if(*current_step%stepSizeBemf == stepSizeBemfMeas2) {
        bemf_start_meas(A10);
        //GPIO_setOutputHighOnPin(10,0x04);
        bemfRunning = TRUE;
      }  else if (bemfRunning){
        bemf_stop_meas();
        bemfRunning = FALSE;
        if (*stepper_state == STEPPER_HOMING) {
          //if (trajectory->speed == -trajectory->maxSpeed) {
            if (bemf_home_check_zero()) {
              trajectory->target = trajectory->position;
              trajectory->speed = 0.0f;
              *stepper_state = STEPPER_NORMAL;
            }
          //}
        }
       // GPIO_setOutputLowOnPin(10,0x04);
      } else if (*current_step%16 == 10) {
        bemf_set_conversion_rate(trajectory->speed);
      }
   // }
    *current_step -= 1;

  }
/*

  if (enableCnt > 0) {
    GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_5);
    enableCnt--;
  } else {
    GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_5);
  }*/
  trajectory_calc_no_break((trajectory->target - trajectory->position), trajectory);
  GPIO_setOutputLowOnPin(step_io.port, step_io.pin);
  GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_3);
  return returnStatus;
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief Check home position for BEMF using method for when the motor is
 * stalled, the BEMF is 0.
 *
 * @return TRUE if BEMF ~0 for defined number of times in a row
 */
bool_t bemf_home_check_zero()
{
  bool_t returnStatus = FALSE;
  static uint8_t number_of_zeros = 0;
  bemfthreshold = 60;

  if (bemf_get_last_meas() <= bemfthreshold) {
    number_of_zeros++;
  } else {
    number_of_zeros = 0;
  }

  if (number_of_zeros == BEMF_ZEROS) {
    returnStatus = TRUE;
  }

  return returnStatus;
}

/**
 * @brief Check home position for BEMF using derivative measurement. Large BEMF
 * change will trigger as a home position
 *
 * @return TRUE if large BEMF derivative
 */
bool_t bemf_home_check_derivative()
{
  bool_t returnState = FALSE;
  float const bemf_diff = 500; // Depending on speed
  float bf = bemf_get_filtered();
  float bl = bemf_get_last_meas();
  if(fabs(bf - bl) > bemf_diff) {
    returnState = TRUE;
  }
  return returnState;
}
/*
 * End of file: stepper.c
 */
