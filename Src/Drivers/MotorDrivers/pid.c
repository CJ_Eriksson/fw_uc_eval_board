#include "pid.h"



static float e_[3];
static float k_[3];
static float kp_;
static float ki_;
static float kd_;

/// Reset prev. error inputs
void pid_reset() {
  e_[0] = 0.0f;
  e_[1] = 0.0f;
  e_[2] = 0.0f;
}

/// Calculate control feedback for one time quanta.
/// @param e Error input.
/// @return Delta control output.
float pid_control(float e) {
  e_[2] = e_[1];
  e_[1] = e_[0];
  e_[0] = e;
  return k_[0] * e_[0] + k_[1] * e_[1] + k_[2] * e_[2];
}

/// Set PID control constants.
/// @param kp Proportional constant
/// @param ki Integral constant
/// @param kd Derivative constant
void pid_set(float kp, float ki, float kd) {
  static const float dt_ = 0.00191939f; // ONLY WHEN MOVING AT 2087 full-step speed
  kp_ = kp;
  ki_ = ki;
  kd_ = kd;

  k_[0] = ki * dt_ + kp + kd / dt_;
  k_[1] = -kp - 2 * kd / dt_;
  k_[2] = kd / dt_;
}



