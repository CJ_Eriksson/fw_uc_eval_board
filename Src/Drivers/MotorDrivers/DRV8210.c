/**
 * @file DRV8210.c
 * @brief Driver for the DRV8210 motor driver
 *
 * @copyright
 *        Unpublished Work Copyright 2021 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Common/common_types.h"
#include "../../Hal/gpio.h"
#include "../BEMF/BEMF_Sensor.h"
#include "trajectory.h"
#include "stepper.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "DRV8210.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define RSENSE 0.1f
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */
typedef enum {
  DRV8210_COAST,
  DRV8210_ACTIVE_INCREASING,
  DRV8210_ACTIVE_DECREASING,
  DRV8210_FAST_DECAY_INCREASE,
  DRV8210_SLOW_DECAY_INCREASE,
  DRV8210_FAST_DECAY_DECREASE,
  DRV8210_SLOW_DECAY_DECREASE,
} drv8210_current_ctrl_modes_e;
/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */
// Stepper current (%) for each step during min step size of 1/32 steps/full
// steps. If smaller step-size is needed, the arrays needs to be expanded.
// cos(motor_angle * pi/180). motor_angle increment is 360/(step_size*4)
static float windingACurrent[128] = {1 ,0.999 ,0.995 ,0.989 ,0.981 ,0.97
                                       ,0.957 ,0.942 ,0.924 ,0.904 ,0.882 ,0.858
                                       ,0.831 ,0.803 ,0.773 ,0.741 ,0.707 ,0.672
                                       ,0.634 ,0.596 ,0.556 ,0.514 ,0.471 ,0.428
                                       ,0.383 ,0.337 ,0.29 ,0.243 ,0.195 ,0.147
                                       ,0.098 ,0.049 ,0 ,-0.049 ,-0.098 ,-0.147
                                       ,-0.195 ,-0.243 ,-0.29 ,-0.337 ,-0.383
                                       ,-0.428 ,-0.471 ,-0.514 ,-0.556 ,-0.596
                                       ,-0.634 ,-0.672 ,-0.707 ,-0.741 ,-0.773
                                       ,-0.803 ,-0.831 ,-0.858 ,-0.882 ,-0.904
                                       ,-0.924 ,-0.942 ,-0.957 ,-0.97 ,-0.981
                                       ,-0.989 ,-0.995 ,-0.999 ,-1 ,-0.999
                                       ,-0.995 ,-0.989 ,-0.981 ,-0.97 ,-0.957
                                       ,-0.942 ,-0.924 ,-0.904 ,-0.882 ,-0.858
                                       ,-0.831 ,-0.803 ,-0.773 ,-0.741 ,-0.707
                                       ,-0.672 ,-0.634 ,-0.596 ,-0.556 ,-0.514
                                       ,-0.471 ,-0.428 ,-0.383 ,-0.337 ,-0.29
                                       ,-0.243 ,-0.195 ,-0.147 ,-0.098 ,-0.049
                                       ,0 ,0.049 ,0.098 ,0.147 ,0.195 ,0.243
                                       ,0.29 ,0.337 ,0.383 ,0.428 ,0.471 ,0.514
                                       ,0.556 ,0.596 ,0.634 ,0.672 ,0.707 ,0.741
                                       ,0.773 ,0.803 ,0.831 ,0.858 ,0.882 ,0.904
                                       ,0.924 ,0.942 ,0.957 ,0.97 ,0.981 ,0.989
                                       ,0.995 ,0.999};
// sin(motor_angle * pi/180). motor_angle increment is 360/(step_size*4)
static float windingBCurrent[128] = {0 ,0.049 ,0.098 ,0.147 ,0.195 ,0.243
                                       ,0.29 ,0.337 ,0.383 ,0.428 ,0.471 ,0.514
                                       ,0.556 ,0.596 ,0.634 ,0.672 ,0.707 ,0.741
                                       ,0.773 ,0.803 ,0.831 ,0.858 ,0.882 ,0.904
                                       ,0.924 ,0.942 ,0.957 ,0.97 ,0.981 ,0.989
                                       ,0.995 ,0.999 ,1 ,0.999 ,0.995 ,0.989
                                       ,0.981 ,0.97 ,0.957 ,0.942 ,0.924 ,0.904
                                       ,0.882 ,0.858 ,0.831 ,0.803 ,0.773 ,0.741
                                       ,0.707 ,0.672 ,0.634 ,0.596 ,0.556 ,0.514
                                       ,0.471 ,0.428 ,0.383 ,0.337 ,0.29 ,0.243
                                       ,0.195 ,0.147 ,0.098 ,0.049 ,0 ,-0.049
                                       ,-0.098 ,-0.147 ,-0.195 ,-0.243 ,-0.29
                                       ,-0.337 ,-0.383 ,-0.428 ,-0.471 ,-0.514
                                       ,-0.556 ,-0.596 ,-0.634 ,-0.672 ,-0.707
                                       ,-0.741 ,-0.773 ,-0.803 ,-0.831 ,-0.858
                                       ,-0.882 ,-0.904 ,-0.924 ,-0.942 ,-0.957
                                       ,-0.97 ,-0.981 ,-0.989 ,-0.995 ,-0.999
                                       ,-1 ,-0.999 ,-0.995 ,-0.989 ,-0.981
                                       ,-0.97 ,-0.957 ,-0.942 ,-0.924 ,-0.904
                                       ,-0.882 ,-0.858 ,-0.831 ,-0.803 ,-0.773
                                       ,-0.741 ,-0.707 ,-0.672 ,-0.634 ,-0.596
                                       ,-0.556 ,-0.514 ,-0.471 ,-0.428 ,-0.383
                                       ,-0.337 ,-0.29 ,-0.243 ,-0.195 ,-0.147
                                       ,-0.098 ,-0.049};

static float currentDampingFactor = 0.7f;  // Default, no damping of current
/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static uint8_t stepSize;
static timer_s* aIn1Timer;
static timer_s* aIn2Timer;
static timer_s* bIn1Timer;
static timer_s* bIn2Timer;
static gpio_s ain1;
static gpio_s ain2;
static gpio_s bin1;
static gpio_s bin2;
static trajectory_s motorTrajectory;
static bool stepperStepEnable = FALSE;
static uint16_t maxCurrent;
static uint16_t idleCurrent;
static int32_t windingACurrentTarget;
static int32_t windingBCurrentTarget;
static timer_s* vrefTimer;
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
// !!OBS!! All timers shall have the same frequency!
ErrorStatus drv8210_init(timer_s* vref_timer,
                         uint_fast8_t ain1_port, uint_fast16_t ain1_pin,
                         uint_fast8_t ain2_port, uint_fast16_t ain2_pin,
                         uint_fast8_t bin1_port, uint_fast16_t bin1_pin,
                         uint_fast8_t bin2_port, uint_fast16_t bin2_pin,
                         uint16_t acceleration,
                         uint16_t min_speed,
                         uint16_t max_speed,
                         uint16_t max_current,
                         uint16_t idle_current)
{

  vrefTimer = vref_timer;

  drv8210_set_speed(max_speed, min_speed);
  drv8210_set_acceleration(acceleration);
  drv8210_set_step_size(STEPPER_1_16_STEP);
  drv8210_set_current(max_current, idle_current);
  ErrorStatus returnStatus = ERROR;
  ain1.port = ain1_port;
  ain1.pin = ain1_pin;
  ain2.port = ain2_port;
  ain2.pin = ain2_pin;

  bin1.port = bin1_port;
  bin1.pin = bin1_pin;
  bin2.port = bin2_port;
  bin2.pin = bin2_pin;
  adc_channel_e channels[2] = {A11, A18};
  adc_start_meas_multi(channels, 2, TRUE);

  return returnStatus;
}
/*
// !!OBS!! All timers shall have the same frequency!
ErrorStatus drv8210_init(timer_s* ain1_timer,
                         timer_s* ain2_timer,
                         timer_s* bin1_timer,
                         timer_s* bin2_timer,
                         uint16_t acceleration,
                         uint16_t min_speed,
                         uint16_t max_speed,
                         uint16_t max_current,
                         uint16_t idle_current)
{
  drv8210_set_speed(max_speed, min_speed);
  drv8210_set_acceleration(acceleration);
  drv8210_set_step_size(STEPPER_1_16_STEP);
  ErrorStatus returnStatus = ERROR;
  if (ain1_timer != NULL &&
      ain2_timer != NULL &&
      bin1_timer != NULL &&
      bin2_timer != NULL ) {

    aIn1Timer = ain1_timer;
    aIn2Timer = ain2_timer;
    bIn1Timer = bin1_timer;
    bIn2Timer = bin2_timer;

    if (aIn1Timer->timer == aIn2Timer->timer &&
        aIn2Timer->timer == bIn1Timer->timer &&
        bIn1Timer->timer == bIn2Timer->timer) {
      if (aIn1Timer->pwmModeConfig.timerPeriod == aIn2Timer->pwmModeConfig.timerPeriod &&
        aIn2Timer->pwmModeConfig.timerPeriod == bIn1Timer->pwmModeConfig.timerPeriod &&
        bIn1Timer->pwmModeConfig.timerPeriod == bIn2Timer->pwmModeConfig.timerPeriod) {
        Timer_A_generatePWM(aIn1Timer->timer,
                            &aIn1Timer->pwmModeConfig);
        Timer_A_generatePWM(aIn2Timer->timer,
                            &aIn2Timer->pwmModeConfig);
        Timer_A_generatePWM(bIn1Timer->timer,
                            &bIn1Timer->pwmModeConfig);
        Timer_A_generatePWM(bIn2Timer->timer,
                            &bIn2Timer->pwmModeConfig);
        returnStatus = SUCCESS;
      }
    }


  }
  return returnStatus;
}
*/
void drv8210_current_control()
{
  static drv8210_current_ctrl_modes_e windingAState = DRV8210_COAST;
  static drv8210_current_ctrl_modes_e windingBState = DRV8210_COAST;
  static uint32_t currA;
  currA = ADC14_getResult(ADC_MEM0);
  static uint32_t currB;
  currB = ADC14_getResult(ADC_MEM1);

  switch (windingAState) {
  case DRV8210_COAST:
    if (currA < (abs(windingACurrentTarget) >> 3)) {
      windingAState = DRV8210_ACTIVE_INCREASING;
      if (windingACurrentTarget > 0) {
        GPIO_setOutputHighOnPin(ain1.port, ain1.pin);
        GPIO_setOutputLowOnPin(ain2.port, ain2.pin);
      } else {
        GPIO_setOutputLowOnPin(ain1.port, ain1.pin);
        GPIO_setOutputHighOnPin(ain2.port, ain2.pin);
      }
    }
    break;
  case DRV8210_ACTIVE_INCREASING:
    if (currA >= (abs(windingACurrentTarget))) {
      windingAState = DRV8210_COAST;
      GPIO_setOutputLowOnPin(ain1.port, ain1.pin);
      GPIO_setOutputLowOnPin(ain2.port, ain2.pin);
    }
    break;
  default:
    GPIO_setOutputLowOnPin(ain1.port, ain1.pin);
    GPIO_setOutputLowOnPin(ain2.port, ain2.pin);
    break;
  }

  switch (windingBState) {
  case DRV8210_COAST:
    if (currB < (abs(windingBCurrentTarget) >> 3)) {
      windingBState = DRV8210_ACTIVE_INCREASING;
      if (windingBCurrentTarget > 0) {
        GPIO_setOutputHighOnPin(bin1.port, bin1.pin);
        GPIO_setOutputLowOnPin(bin2.port, bin2.pin);
      } else {
        GPIO_setOutputLowOnPin(bin1.port, bin1.pin);
        GPIO_setOutputHighOnPin(bin2.port, bin2.pin);
      }
    }
    break;
  case DRV8210_ACTIVE_INCREASING:
    if (currB >= (abs(windingBCurrentTarget))) {
      windingBState = DRV8210_COAST;
      GPIO_setOutputLowOnPin(bin1.port, bin1.pin);
      GPIO_setOutputLowOnPin(bin2.port, bin2.pin);
    }
    break;
  default:
    GPIO_setOutputLowOnPin(bin1.port, bin1.pin);
    GPIO_setOutputLowOnPin(bin2.port, bin2.pin);
    break;
  }

  Timer_A_setCompareValue(vrefTimer->timer,
                          vrefTimer->pwmModeConfig.compareRegister,
                          MIN((uint16_t)vrefTimer->pwmModeConfig.timerPeriod,
                              (uint16_t)vrefTimer->pwmModeConfig.timerPeriod *
                              abs(windingBCurrentTarget)/1638));
}

ErrorStatus drv8210_set_step_size(stepper_step_mode_e step_size)
{
  switch (step_size) {
    case STEPPER_FULL_STEP:
      stepSize = 32;
      break;
    case STEPPER_1_2_STEP:
      stepSize = 16;
      break;
    case STEPPER_1_4_STEP:
      stepSize = 8;
      break;
    case STEPPER_1_8_STEP:
      stepSize = 4;
      break;
    case STEPPER_1_16_STEP:
      stepSize = 2;
      break;
    case STEPPER_1_32_STEP:
      stepSize = 1;
      break;
    default:
      stepSize = 1;
      break;
  }

  return SUCCESS;
}

ErrorStatus drv8210_set_target(int32_t target_position)
{
  motorTrajectory.target = target_position;

  return SUCCESS;
}

ErrorStatus drv8210_set_speed(uint16_t max_speed, uint16_t min_speed)
{
  ErrorStatus returnStatus = ERROR;
  returnStatus = trajectory_set_speed(&motorTrajectory,
                                      max_speed,
                                      min_speed,
                                      STEP_FREQUENCY);
  return returnStatus;
}

ErrorStatus drv8210_set_current(uint16_t max_current, uint16_t idle_current)
{
  ErrorStatus returnStatus = ERROR;
  maxCurrent = max_current;
  idleCurrent = idle_current;
  return returnStatus;
}

ErrorStatus drv8210_set_acceleration(uint16_t acceleration)
{
  ErrorStatus returnStatus = ERROR;
  returnStatus = trajectory_set_acceleration(&motorTrajectory,
                              acceleration,
                              STEP_FREQUENCY);
  return returnStatus;
}

void drv8210_step()
{
  if (stepperStepEnable) {
    static uint32_t setCurrent;
    static int32_t currentStep = 0;
    static bool_t bemfRunning = FALSE;
    float toTarget = motorTrajectory.target - motorTrajectory.position;
    if (toTarget != 0.0f) {
      int32_t distance =
              currentStep - motorTrajectory.position;
      bool_t step = FALSE;
      if (distance < 0) {
        step = TRUE;
        //GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_2);
        currentStep++;
      } else if (distance != 0) {
        currentStep--;
        //GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_2);
        step = TRUE;
      }
      setCurrent = maxCurrent;
    } else {
      setCurrent = idleCurrent;
    }
    uint8_t index = (currentStep * stepSize) % 128;
    windingACurrentTarget = (int32_t)(windingACurrent[index] * (float)setCurrent * RSENSE * 16383 / 3300);
    windingBCurrentTarget = (int32_t)(windingBCurrent[index] * (float)setCurrent * RSENSE * 16383 / 3300);

    trajectory_calc(toTarget, &motorTrajectory);
    //GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_2);
  }
}


/*
void drv8210_step()
{
  if (stepperStepEnable) {
    static int32_t currentStep = 0;
    static bool_t bemfRunning = FALSE;
    float toTarget = motorTrajectory.target - motorTrajectory.position;
    if (toTarget != 0.0f) {
      int32_t distance =
              currentStep - motorTrajectory.position;
      bool_t step = FALSE;
      if (distance < 0) {
        step = TRUE;
        currentStep++;
      } else if (distance != 0) {
        currentStep--;
        step = TRUE;
      }
      if (step == TRUE) {
        GPIO_toggleOutputOnPin(4, 4);
        uint8_t index = (currentStep * stepSize) % 128;
        // !!OBS!! All timers shall have the same frequency!

        int16_t dutyCycleA = (uint16_t)aIn1Timer->pwmModeConfig.timerPeriod *
            (windingACurrent[index] * currentDampingFactor);

        int16_t dutyCycleB = (uint16_t)bIn1Timer->pwmModeConfig.timerPeriod *
            (windingBCurrent[index] * currentDampingFactor);

        if (dutyCycleA < 0) {
          Timer_A_setCompareValue(aIn2Timer->timer,
                                  aIn2Timer->pwmModeConfig.compareRegister,
                                  -dutyCycleA);
          Timer_A_setCompareValue(aIn1Timer->timer,
                                  aIn1Timer->pwmModeConfig.compareRegister,
                                  0);
          // Stop BEMF measurement
          if (bemfRunning) {
            if (bemf_stop_meas() == SUCCESS) {
              bemfRunning = FALSE;
              GPIO_setOutputLowOnPin(10, 4);
            }
          }
        } else if (dutyCycleA > 0) {
          Timer_A_setCompareValue(aIn1Timer->timer,
                                  aIn1Timer->pwmModeConfig.compareRegister,
                                  dutyCycleA);
          Timer_A_setCompareValue(aIn2Timer->timer,
                                  aIn2Timer->pwmModeConfig.compareRegister,
                                  0);
          // Stop BEMF measurement
          if (bemfRunning) {
            if (bemf_stop_meas() == SUCCESS) {
              bemfRunning = FALSE;
              GPIO_setOutputLowOnPin(10, 4);
            }
          }
        } else if (dutyCycleA == 0) {
          Timer_A_setCompareValue(aIn2Timer->timer,
                                  aIn2Timer->pwmModeConfig.compareRegister,
                                  0);
          Timer_A_setCompareValue(aIn1Timer->timer,
                                  aIn1Timer->pwmModeConfig.compareRegister,
                                  0);
          // Enable start of BEMF measurement
          if (!bemfRunning) {
            if (bemf_start_meas(A8) == SUCCESS) {
              bemfRunning = TRUE;
              GPIO_setOutputHighOnPin(10, 4);
            }
          }
        }
        if (dutyCycleB < 0) {
          Timer_A_setCompareValue(bIn2Timer->timer,
                                  bIn2Timer->pwmModeConfig.compareRegister,
                                  -dutyCycleB);
          Timer_A_setCompareValue(bIn1Timer->timer,
                                  bIn1Timer->pwmModeConfig.compareRegister,
                                  0);
        } else if (dutyCycleB > 0) {
          Timer_A_setCompareValue(bIn1Timer->timer,
                                  bIn1Timer->pwmModeConfig.compareRegister,
                                  dutyCycleB);
          Timer_A_setCompareValue(bIn2Timer->timer,
                                  bIn2Timer->pwmModeConfig.compareRegister,
                                  0);
        } else if (dutyCycleB == 0) {
          Timer_A_setCompareValue(bIn2Timer->timer,
                                  bIn2Timer->pwmModeConfig.compareRegister,
                                  0);
          Timer_A_setCompareValue(bIn1Timer->timer,
                                  bIn1Timer->pwmModeConfig.compareRegister,
                                  0);
        }
      }
    } else {
      if (!bemfRunning) {
        if (bemf_stop_meas() == SUCCESS) {
          bemfRunning = FALSE;
        }
      }
      Timer_A_setCompareValue(aIn1Timer->timer,
                              aIn1Timer->pwmModeConfig.compareRegister,
                              0);
      Timer_A_setCompareValue(aIn2Timer->timer,
                              aIn2Timer->pwmModeConfig.compareRegister,
                              0);
      Timer_A_setCompareValue(bIn1Timer->timer,
                              bIn1Timer->pwmModeConfig.compareRegister,
                              0);
      Timer_A_setCompareValue(bIn2Timer->timer,
                              bIn2Timer->pwmModeConfig.compareRegister,
                              0);
    }

    trajectory_calc(toTarget, &motorTrajectory);
  }

}
*/
ErrorStatus drv8210_stepper_step_enable(bool_t enable_disable) {
  stepperStepEnable = enable_disable;
}

/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */

/*
 * End of file: DRV8210.c
 */
