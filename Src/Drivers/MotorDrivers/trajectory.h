/**
 * @file trajectory.h
 * @brief Trajectory calculator for motor movement
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _TRAJECTORY_H_
#define _TRAJECTORY_H_

#include "../../Common/common_types.h"
#include <stdint.h>
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */
#define STEP_FREQUENCY 50000 // TODO(caer) move
/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */
typedef struct {
  float lastPosition;       // Last position calculated in trajectory [usteps]
  float position;           // Current position [usteps]
  float target;             // Current target [usteps]
  float minDistance;        // Distance it takes to accelerate to min speed [usteps]
  float acceleration;       // Current acceleration [usteps/interrupt^2]
  float maxAcceleration;    // Maximum acceleration for trajectory [usteps/interrupt^2]
  float maxSpeed;           // Maximum speed that shall be reached during trajectory [usteps/interrupt]
  float speed;              // Current speed [usteps/interrupt]
  float minSpeed;           // Start speed for trajectory. Stepper motors often can't start at 0 speed [usteps/interrupt]
} trajectory_s;
/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus trajectory_set_speed(trajectory_s* trajectory,
                                 uint16_t max_speed,
                                 uint16_t min_speed,
                                 uint32_t step_frequency);
ErrorStatus trajectory_set_acceleration(trajectory_s* trajectory,
                                        uint32_t acceleration,
                                        uint32_t step_frequency);
ErrorStatus trajectory_calc_no_break(float distance, trajectory_s* trajectory);
ErrorStatus trajectory_calc(float distance, trajectory_s* trajectory);

#ifdef __cplusplus
}
#endif

#endif /* _TRAJECTORY_H_ */

/*
 * End of file: trajectory.h
 */
