/*
 * pid.h
 *
 *  Created on: 12 nov. 2021
 *      Author: caer
 */

#ifndef SRC_DRIVERS_MOTORDRIVERS_PID_H_
#define SRC_DRIVERS_MOTORDRIVERS_PID_H_



void pid_reset();
float pid_control(float e);
void pid_set(float kp, float ki, float kd);

#endif /* SRC_DRIVERS_MOTORDRIVERS_PID_H_ */
