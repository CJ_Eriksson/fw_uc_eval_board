/**
 * @file stepper.h
 * @brief Helper functions for steppper motors using dir. step and PWM Vref as
 *        current control
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _STEPPER_H_
#define _STEPPER_H_

#include "../../Common/common_types.h"
#include "trajectory.h"
#include "../../Hal/timer_a.h"
#include "../../Board/board.h"
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */
typedef enum {
  STEPPER_1_256_STEP,
  STEPPER_1_128_STEP,
  STEPPER_1_64_STEP,
  STEPPER_1_32_STEP,
  STEPPER_1_16_STEP,
  STEPPER_1_8_STEP,
  STEPPER_1_4_STEP,
  STEPPER_1_2_STEP,
  STEPPER_FULL_STEP,
} stepper_step_mode_e;

typedef enum {
  STEPPER_IDLE,
  STEPPER_HOMING,
  STEPPER_NORMAL
} stepper_state_e;


/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */
/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */
extern uint32_t bemfthreshold;
/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */

#ifdef __cplusplus
}
#endif


ErrorStatus stepper_set_current(timer_s* vref_timer, float duty_cycle);

ErrorStatus stepper_step(int32_t* current_step,
                         trajectory_s* trajectory,
                         gpio_s step_io,
                         gpio_s dir_io,
                         stepper_step_mode_e stepper_mode,
                         stepper_state_e* stepper_state);

#endif /* _STEPPER_H_ */

/*
 * End of file: stepper.h
 */
