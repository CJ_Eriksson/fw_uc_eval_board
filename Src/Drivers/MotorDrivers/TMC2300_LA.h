/**
 * @file TMC2300_LA.h
 * @brief Driver for the TMC2300-LA motor driver
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _TMC2300_LA_H_
#define _TMC2300_LA_H_

#include "../../Hal/uart.h"
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */

#define TMC2300LA_MAX_CURRENT_MA 266.0f //220 // 1ohm Rsense = 220mA RMS
/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */
typedef enum {
  TMC2300_REG_GCONF_0 = 0x00,
  TMC2300_REG_GSTAT_1 = 0x01,
  TMC2300_REG_IFCNT_2 = 0x02,
  TMC2300_REG_SLAVECONF_3 = 0x03,
  TMC2300_REG_IOIN = 0x06,
  TMC2300_REG_IHOLD_IRUNL_10 = 0x10,
  TMC2300_REG_TPOWER_DOWN_11 = 0x11,
  TMC2300_REG_TSTEP_12 = 0x12,
  TMC2300_REG_VACTUAL_22 = 0x22,
  TMC2300_REG_TCOOLTHRS_14 = 0x14,
  TMC2300_REG_SGTHRS_40 = 0x40,
  TMC2300_REG_SG_VALUE_41 = 0x41,
  TMC2300_REG_COOLCONF_42 = 0x42,
  TMC2300_REG_MSCNT_6A = 0x6A,
  TMC2300_REG_CHOPCONF_6C = 0x6C,
  TMC2300_REG_DRV_STATUS_6F = 0x6F,
  TMC2300_REG_PWMCONF_70 = 0x70,
  TMC2300_REG_PWM_SCALE_71 = 0x71,
  TMC2300_REG_PWM_AUTO_72 = 0x72,

} tmc2300_register_e;
/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus tmc2300_init(uint_fast8_t step_port, uint_fast16_t step_pin,
                         uint_fast8_t dir_port, uint_fast16_t dir_pin,
#ifndef TMC2300LA_UART
                         uint_fast8_t pdn_port, uint_fast16_t pdn_pin,
#endif
                         uint_fast8_t mode_port, uint_fast16_t mode_pin,
                         uint_fast8_t MS1_port, uint_fast16_t MS1_pin,
                         uint_fast8_t MS2_port, uint_fast16_t MS2_pin,
                         uint_fast8_t diag_port, uint_fast16_t diag_pin,
                         uint_fast8_t enable_port, uint_fast16_t enable_pin,
                         uint_fast8_t stepper_port, uint_fast16_t stepper_pin,
#ifdef TMC2300LA_UART
                         eUSCI_UART* uart_instance,
#endif
                         uint16_t acceleration,
                         uint16_t min_speed,
                         uint16_t max_speed,
                         uint16_t max_current,
                         uint16_t idle_current);

ErrorStatus tmc2300_enable_driver();
ErrorStatus tmc2300_set_stepmode(stepper_step_mode_e step_mode);

#ifdef TMC2300LA_UART
ErrorStatus tmc2300_set_current(uint16_t max_current, uint16_t idle_current);
ErrorStatus tmc2300_read_register(tmc2300_register_e reg, uint32_t* data);
#endif
float tmc2300_get_speed();
ErrorStatus tmc2300_set_speed(uint16_t max_speed, uint16_t min_speed);
ErrorStatus tmc2300_set_acceleration(uint16_t acceleration);
void tmc2300_step();
ErrorStatus tmc2300_stepper_step_enable(bool_t enable_disable);
ErrorStatus tmc2300_set_target(int32_t target_position);

#ifdef __cplusplus
}
#endif

#endif /* _TMC2300_LA_H_ */

/*
 * End of file: TMC2300_LA.h
 */
