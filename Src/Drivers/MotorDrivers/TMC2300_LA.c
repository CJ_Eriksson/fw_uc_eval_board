/**
 * @file TMC2300_LA.c
 * @brief Driver for the TMC2300_LA motor driver
 *
 * @copyright
 *        Unpublished Work Copyright 2021 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Hal/timer_a.h"
#include "../../Board/board.h"
#include "../../Common/common_types.h"
#include "../Time/Systime.h"
#include "trajectory.h"
#include "stepper.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "TMC2300_LA.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define MV_PER_MAMP 1.32f  // From datasheet might needed to be calibrated
#define SYNC_BYTE 0x05
#define TMC2300_CURRENT_STEPS 31 // 32-1
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static gpio_s step;
static gpio_s dir;
#ifndef TMC2300LA_UART
static gpio_s pdn;
#endif
static gpio_s mode;
static gpio_s MS1;
static gpio_s MS2;
static gpio_s diag;
static gpio_s enable;
static gpio_s stepper;
static trajectory_s motorTrajectory;
static stepper_step_mode_e stepperMode;
static stepper_state_e state;

#ifdef TMC2300LA_UART
static uint8_t tmc2300_address;
eUSCI_UART* uart;
static uint32_t maxCurrent;
static uint32_t idleCurrent;
static uint32_t current;
static uint16_t stepAcceleration;
static bool stepperStepEnable = FALSE;
#endif
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */
#ifdef TMC2300LA_UART
ErrorStatus write(tmc2300_register_e reg, uint32_t data);
ErrorStatus read(tmc2300_register_e reg, uint32_t* data);
uint8_t calc_crc(uint8_t* data, uint8_t nr_of_bytes);
#endif
/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
ErrorStatus tmc2300_init(uint_fast8_t step_port, uint_fast16_t step_pin,
                         uint_fast8_t dir_port, uint_fast16_t dir_pin,
#ifndef TMC2300LA_UART
                         uint_fast8_t pdn_port, uint_fast16_t pdn_pin,
#endif
                         uint_fast8_t mode_port, uint_fast16_t mode_pin,
                         uint_fast8_t MS1_port, uint_fast16_t MS1_pin,
                         uint_fast8_t MS2_port, uint_fast16_t MS2_pin,
                         uint_fast8_t diag_port, uint_fast16_t diag_pin,
                         uint_fast8_t enable_port, uint_fast16_t enable_pin,
                         uint_fast8_t stepper_port, uint_fast16_t stepper_pin,
#ifdef TMC2300LA_UART
                         eUSCI_UART* uart_instance,
#endif
                         uint16_t acceleration,
                         uint16_t min_speed,
                         uint16_t max_speed,
                         uint16_t max_current,
                         uint16_t idle_current)
{

  step.port = step_port;
  step.pin = step_pin;
  dir.port = dir_port;
  dir.pin = dir_pin;
#ifndef TMC2300LA_UART
  pdn.port = pdn_port;
  pdn.pin = pdn_pin;
#endif
  mode.port =  mode_port;
  mode.pin =  mode_pin;
  MS1.port = MS1_port;
  MS1.pin = MS1_pin;
  MS2.port = MS2_port;
  MS2.pin = MS2_pin;
  diag.port = diag_port;
  diag.pin = diag_pin;
  enable.port = enable_port;
  enable.pin = enable_pin;
  stepper.port = stepper_port;
  stepper.pin = stepper_pin;
  GPIO_setOutputHighOnPin(enable.port, enable.pin);
#ifdef TMC2300LA_UART
  GPIO_setOutputLowOnPin(MS1.port, MS1.pin);
  GPIO_setOutputLowOnPin(MS2.port, MS2.pin);
  tmc2300_address = 0x00; // Slave address selected by MS1 and MS2 pins
  uart = uart_instance;
#endif

  // Step
  tmc2300_set_stepmode(STEPPER_1_4_STEP);

  // Trajectory
  tmc2300_set_speed(max_speed, min_speed);
  tmc2300_set_acceleration(acceleration);;

  // Configure as stand alone stepper
  GPIO_setOutputLowOnPin(mode.port, mode.pin);
  GPIO_setOutputHighOnPin(stepper.port, stepper.pin);
#ifndef TMC2300LA_UART
  GPIO_setOutpuHighOnPin(mode.port, mode.pin);
  GPIO_setOutputHighOnPin(pdn.port, pdn.pin);
#endif

  motorTrajectory.lastPosition = motorTrajectory.position = 0.0f;
  motorTrajectory.speed = 0.0f;

#ifdef TMC2300LA_UART
  tmc2300_set_current(max_current, idle_current);
#endif
  uint32_t status;
  //read(TMC2300_REG_DRV_STATUS_6F, &status);

  read(TMC2300_REG_PWMCONF_70, &status);
  status &=~0x30000;
  status |= 0x30000; // 2/410 fclk
  status &=~0xF000000;
  status |= 0xF000000;  // Increments select

  status &=~0x40000;  // Reset for new tuning

  status &=~0x300000;
  status |= 0x100000; // Freewheeling at standstill
  write(TMC2300_REG_PWMCONF_70, status);

  read(TMC2300_REG_PWMCONF_70, &status);
  status |= 0x40000; // Reset for new tuning
  systime_wait_ms(1); // Wait before write back auto tuning.
  write(TMC2300_REG_PWMCONF_70, status);
  status = 0;
  read(TMC2300_REG_GCONF_0, &status);
  status |=0x10;
  write(TMC2300_REG_GCONF_0, &status);

  return SUCCESS;
}
ErrorStatus tmc2300_enable_driver()  // TODO(caer) only use with UART.
{
  ErrorStatus returnStatus = ERROR;
  uint32_t tempRegister = 0;
   if (read(TMC2300_REG_CHOPCONF_6C, &tempRegister) == SUCCESS) {
     tempRegister |= 0x01;
     if (write(TMC2300_REG_CHOPCONF_6C, tempRegister) == SUCCESS) {
       GPIO_setOutputHighOnPin(enable.port, enable.pin);
       returnStatus = SUCCESS;
     }
   }
   tempRegister = 0xFFFFAAAA;
   read(TMC2300_REG_CHOPCONF_6C, &tempRegister);
  return returnStatus;
}

float tmc2300_get_speed() {
  return motorTrajectory.speed;
}
ErrorStatus tmc2300_set_stepmode(stepper_step_mode_e step_mode)
{
  ErrorStatus returnStatus = ERROR;
#ifdef TMC2300LA_UART
  uint32_t tempRegister = 0;
  if (read(TMC2300_REG_CHOPCONF_6C, &tempRegister) == SUCCESS) {
    tempRegister &=~0x0F000000;
    tempRegister |= (((uint32_t)(step_mode) << 24) & 0x0F000000);
    if (step_mode != STEPPER_1_256_STEP) {
      tempRegister &=~0x10000000; // Remove extrapolation to 256 micro steps
    } else {
      tempRegister |= 0x10000000; // Enable extrapolation to 256 micro steps
    }

  }
    returnStatus = write(TMC2300_REG_CHOPCONF_6C, tempRegister);
    tempRegister= 0;
    read(TMC2300_REG_CHOPCONF_6C, &tempRegister);

#endif
#ifndef TMC2300LA_UART
  switch (step_mode) {
  case STEPPER_FULL_STEP: {
    // NA
    break;
  }
  case STEPPER_1_2_STEP: {
    // NA
    break;
  }
  case STEPPER_1_4_STEP: {
    // NA
    break;
  }
  case STEPPER_1_8_STEP: {
    GPIO_setOutputLowOnPin(MS1.port, MS1.pin);
    GPIO_setOutputLowOnPin(MS2.port, MS2.pin);
    returnStatus = SUCCESS;
    break;
  }
  case STEPPER_1_16_STEP: {
    GPIO_setOutputHighOnPin(MS1.port, MS1.pin);
    GPIO_setOutputHighOnPin(MS2.port, MS2.pin);
    returnStatus = SUCCESS;
    break;
  }
  case STEPPER_1_32_STEP: {
    GPIO_setOutputHighOnPin(MS1.port, MS1.pin);
    GPIO_setOutputLowOnPin(MS2.port, MS2.pin);
    returnStatus = SUCCESS;
    break;
  }
  case STEPPER_1_64_STEP: {
    GPIO_setOutputLowOnPin(MS1.port, MS1.pin);
    GPIO_setOutputHighOnPin(MS2.port, MS2.pin);
    returnStatus = SUCCESS;
    break;
  }
  case STEPPER_1_128_STEP: {
    // NA
    break;
  }
  case STEPPER_1_256_STEP: {
    // NA
    break;
  }
    default:
      returnStatus = ERROR;
      break;
  }
#endif
  stepperMode = step_mode;
  return returnStatus;
}
#ifdef TMC2300LA_UART

ErrorStatus tmc2300_set_current(uint16_t max_current, uint16_t idle_current)
{
  ErrorStatus returnStatus = ERROR;
  if (max_current < TMC2300LA_MAX_CURRENT_MA && idle_current < TMC2300LA_MAX_CURRENT_MA) {
    maxCurrent =  (uint32_t)((((float)max_current) /
        TMC2300LA_MAX_CURRENT_MA) * TMC2300_CURRENT_STEPS);  // TODO (caer) trunkating value is ok?
    idleCurrent =  (uint32_t)((((float)idle_current) /
        TMC2300LA_MAX_CURRENT_MA) * TMC2300_CURRENT_STEPS);  // TODO (caer) trunkating value is ok?
    returnStatus = SUCCESS;
  }
  returnStatus = write(TMC2300_REG_IHOLD_IRUNL_10,
                       (idleCurrent | (maxCurrent << 8)) & 0x1F1F);
  return returnStatus;
}
ErrorStatus tmc2300_read_register(tmc2300_register_e reg, uint32_t* data)
{
  ErrorStatus returnStatus = ERROR;
  if (read(reg, data) == SUCCESS) {
    returnStatus = SUCCESS;
  }

  return returnStatus;
}

ErrorStatus tmc2300_write_register(tmc2300_register_e reg, uint32_t* data)
{
  ErrorStatus returnStatus = ERROR;
  if (write(reg, data) == SUCCESS) {
    returnStatus = SUCCESS;
  }

  return returnStatus;
}

#endif

ErrorStatus tmc2300_set_speed(uint16_t max_speed, uint16_t min_speed)
{
  ErrorStatus returnStatus = ERROR;
  returnStatus = trajectory_set_speed(&motorTrajectory,
                                      max_speed,
                                      min_speed,
                                      STEP_FREQUENCY);
  if (returnStatus == SUCCESS) {
    returnStatus = trajectory_set_acceleration(&motorTrajectory,
                                               stepAcceleration,
                                               STEP_FREQUENCY);
  }
  return returnStatus;
}

ErrorStatus tmc2300_set_acceleration(uint16_t acceleration)
{
  ErrorStatus returnStatus = ERROR;
  returnStatus = trajectory_set_acceleration(&motorTrajectory,
                              acceleration,
                              STEP_FREQUENCY);
  stepAcceleration = acceleration;
  return returnStatus;
}

void tmc2300_step()
{
  if (stepperStepEnable) {
    static int32_t stepPosition;  // Current position for the stepper motor
    stepper_step(&stepPosition, &motorTrajectory, step, dir, stepperMode, &state);
  }
  /*if (GPIO_getInputPinValue(GPIO_PORT_2, GPIO_PIN_MD3_CTRL_DIAG) == GPIO_INPUT_PIN_HIGH) {
    GPIO_setOutputHighOnPin(10,0x04);
  } else {
    GPIO_setOutputLowOnPin(10,0x04);
  }*/
}

ErrorStatus tmc2300_stepper_step_enable(bool_t enable_disable) {
  stepperStepEnable = enable_disable;
}

ErrorStatus tmc2300_set_target(int32_t target_position)
{
  motorTrajectory.target = (float)target_position;
  return SUCCESS;
}

/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */
#ifdef TMC2300LA_UART
ErrorStatus write(tmc2300_register_e reg, uint32_t data)
{
  ErrorStatus returnStatus = SUCCESS;
  uint8_t dataBuffer[8] = {0};
  dataBuffer[0] = SYNC_BYTE;
  dataBuffer[1] = tmc2300_address;
  dataBuffer[2] = ((uint8_t)(reg) |0x80);  // 0x80 for write
  dataBuffer[3] = (uint8_t)(data >> 24);
  dataBuffer[4] = (uint8_t)(data >> 16);
  dataBuffer[5] = (uint8_t)(data >> 8);
  dataBuffer[6] = (uint8_t)(data);
  dataBuffer[7] = calc_crc(dataBuffer, 7);

  uint32_t stop_tick = systime_get_tick() + 10;
  int8_t i = 0;
  for (i = 0; i < 8; ++i) {
    UART_transmitData(uart->moduleInstance, dataBuffer[i]);
    do {
      if (UART_getInterruptStatus(uart->moduleInstance,
                                         EUSCI_A_UART_TRANSMIT_INTERRUPT_FLAG) != 0) {
        UART_clearInterruptFlag(uart->moduleInstance, EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG);
        UART_clearInterruptFlag(uart->moduleInstance, EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT_FLAG);
        returnStatus = SUCCESS;
        break;
      }
    } while(((int16_t)(systime_get_tick() - stop_tick)) < 0);
  }
  if (returnStatus == SUCCESS) {
    returnStatus = ERROR;
    // Wait for last byte to be sent, because RX and TX shares the same line
    do {
      if (UART_getInterruptStatus(uart->moduleInstance,
                                  EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT_FLAG) != 0) {
        UART_clearInterruptFlag(uart->moduleInstance, EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT_FLAG);
        UART_clearInterruptFlag(uart->moduleInstance, EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG);
        returnStatus = SUCCESS;
        break;
      }
    } while(((int16_t)(systime_get_tick() - stop_tick)) < 0);
  }
  return returnStatus;
}

ErrorStatus read(tmc2300_register_e reg, uint32_t* data)
{
  ErrorStatus returnStatus = SUCCESS;
  uint8_t dataBuffer[8] = {0};
  dataBuffer[0] = SYNC_BYTE;
  dataBuffer[1] = tmc2300_address;
  dataBuffer[2] = (uint8_t)(reg);

  dataBuffer[3] = calc_crc(dataBuffer, 3);
  uint32_t stop_tick = systime_get_tick() + 10;
  int8_t i = 0;
  for (i = 0; i < 4; ++i) {
    UART_transmitData(uart->moduleInstance, dataBuffer[i]);
    do {
      if (UART_getInterruptStatus(uart->moduleInstance,
                                  EUSCI_A_UART_TRANSMIT_INTERRUPT_FLAG) != 0) {
        UART_clearInterruptFlag(uart->moduleInstance, EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG);
        UART_clearInterruptFlag(uart->moduleInstance, EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT_FLAG);
        returnStatus = SUCCESS;
        break;
      }
    } while(((int16_t)(systime_get_tick() - stop_tick)) < 0);
  }
  // Wait for last byte to be sent, because RX and TX shares the same line
  do {
    if (UART_getInterruptStatus(uart->moduleInstance,
                                EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT_FLAG) != 0) {
      UART_clearInterruptFlag(uart->moduleInstance, EUSCI_A_UART_TRANSMIT_COMPLETE_INTERRUPT_FLAG);
      UART_clearInterruptFlag(uart->moduleInstance, EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG);
      returnStatus = SUCCESS;
      break;
    }
  } while(((int16_t)(systime_get_tick() - stop_tick)) < 0);
  if (returnStatus == SUCCESS) {
    returnStatus = ERROR;
    stop_tick = systime_get_tick() + 10;
    uint8_t receivedBytes = 0;
    do {

      if (UART_getInterruptStatus(uart->moduleInstance,
                                  EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG) != 0) {
        dataBuffer[receivedBytes++] = UART_receiveData(uart->moduleInstance);
        UART_clearInterruptFlag(uart->moduleInstance, EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG);
      }
    } while(receivedBytes < 8 &&
        (((int16_t)(systime_get_tick() - stop_tick)) < 0));
    // Check that all bytes have been received and that the CRC check is ok
    if ((receivedBytes == 8) && (dataBuffer[7] == calc_crc(dataBuffer, 7))) {
      *data = (uint32_t)dataBuffer[3] << 24;
      *data |= (uint32_t)dataBuffer[4] << 16;
      *data |= (uint32_t)dataBuffer[5] << 8;
      *data |= (uint32_t)dataBuffer[6];
      returnStatus = SUCCESS;
    }
  }

  return returnStatus;
}

uint8_t calc_crc(uint8_t* data, uint8_t nr_of_bytes)
{
  uint8_t i,j;
  uint8_t crc = 0;
  uint8_t currentByte;
  for (i = 0; i < (nr_of_bytes); ++i) { // Execute for all bytes of a message
    currentByte = data[i]; // Retrieve a byte to be sent from Array
    for (j = 0; j < 8; ++j) {
      if ((crc >> 7) ^ (currentByte & 0x01)) {// update CRC based result of XOR operation
        crc = (crc << 1) ^ 0x07;
      }
      else {
        crc = (crc << 1);
      }
    currentByte = currentByte >> 1;
    } // for CRC bit
  } // for message byte
  return crc;
}
#endif
/*
 * End of file: TMC2300_LA.c
 */
