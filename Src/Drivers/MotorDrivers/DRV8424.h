/**
 * @file DRV8242.h
 * @brief Driver for the DRV8242 motor driver
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _DRV8424_H_
#define _DRV8424_H_

#include "../../Hal/timer_a.h"
#include "stepper.h"
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */
#define DRV8424_MAX_CURRENT 1000
#define DRV8424_MAX_SPEED STEP_FREQUENCY
#define DRV8424_MAX_ACCELERATION STEP_FREQUENCY/2
/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */
typedef enum {
  DRV8424_DECAY_SMART_DYNAMIC,
  DRV8424_DECAY_SMART_RIPPLE,
  DRV8424_DECAY_MIXED_30FAST,
  DRV8424_DECAY_MIXED_60FAST,
  DRV8424_DECAY_SLOW,
  DRV8424_DECAY_SLOW_BOTH
} drv8424_decay_modes_e;
/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */

ErrorStatus drv8424_init(timer_s* vref_timer,
                         uint_fast8_t step_port, uint_fast16_t step_pin,
                         uint_fast8_t dir_port, uint_fast16_t dir_pin,
                         uint_fast8_t fault_port, uint_fast16_t fault_pin,
                         uint_fast8_t sleep_port, uint_fast16_t sleep_pin,
                         uint_fast8_t enable_port, uint_fast16_t enable_pin,
                         uint_fast8_t decay0_port, uint_fast16_t decay0_pin,
                         uint_fast8_t decay1_port, uint_fast16_t decay1_pin,
                         uint_fast8_t toff_port, uint_fast16_t toff_pin,
                         uint_fast8_t m0_port, uint_fast16_t m0_pin,
                         uint_fast8_t m1_port, uint_fast16_t m1_pin,
                         uint32_t acceleration, uint16_t min_speed, uint16_t max_speed, uint16_t max_current, uint16_t idle_current);
ErrorStatus drv8424_sleep(bool_t sleep);
ErrorStatus drv8424_set_stepmode(stepper_step_mode_e step_mode);
ErrorStatus drv8424_set_current(uint16_t max_current, uint16_t idle_current);
ErrorStatus drv8424_set_speed(uint16_t max_speed, uint16_t min_speed);
ErrorStatus drv8424_set_acceleration(uint32_t acceleration);
ErrorStatus drv8424_stop();
void drv8424_step();
ErrorStatus drv8424_stepper_step_enable(bool_t enable_disable);
ErrorStatus drv8424_homing(int32_t target);
stepper_state_e drv8424_get_state();
ErrorStatus drv8424_set_target(int32_t target_position);
ErrorStatus drv8424_set_decay(drv8424_decay_modes_e decay_mode);
float drv8424_get_speed();
float drv8424_get_position();

#ifdef __cplusplus
}
#endif

#endif /* DRV8424 */

/*
 * End of file: DRV8424.h
 */
