/**
 * @file DRV8210.h
 * @brief Driver for the DRV8210 motor driver
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _DRV8210_H_
#define _DRV8210_H_

#include "../../Hal/timer_a.h"
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */
/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus drv8210_init(timer_s* vref_timer, uint_fast8_t ain1_port, uint_fast16_t ain1_pin,
                         uint_fast8_t ain2_port, uint_fast16_t ain2_pin,
                         uint_fast8_t bin1_port, uint_fast16_t bin1_pin,
                         uint_fast8_t bin2_port, uint_fast16_t bin2_pin,
                         uint16_t acceleration,
                         uint16_t min_speed,
                         uint16_t max_speed,
                         uint16_t max_current,
                         uint16_t idle_current);
/*
ErrorStatus drv8210_init(timer_s* ain1_timer,
                         timer_s* ain2_timer,
                         timer_s* bin1_timer,
                         timer_s* bin2_timer,
                         uint16_t acceleration,
                         uint16_t min_speed,
                         uint16_t max_speed,
                         uint16_t max_current,
                         uint16_t idle_current);
*/
ErrorStatus drv8210_set_step_size(stepper_step_mode_e step_size);
ErrorStatus drv8210_set_target(int32_t target_position);
ErrorStatus drv8210_set_speed(uint16_t max_speed, uint16_t min_speed);
ErrorStatus drv8210_set_current(uint16_t max_current, uint16_t idle_current);
ErrorStatus drv8210_set_acceleration(uint16_t acceleration);
void drv8210_step();
void drv8210_current_control();
ErrorStatus drv8210_stepper_step_enable(bool_t enable_disable);

#ifdef __cplusplus
}
#endif

#endif /* _DRV8210_H_ */

/*
 * End of file: DRV8210.h
 */
