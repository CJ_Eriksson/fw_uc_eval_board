/**
 * @file trajectory.c
 * @brief Trajectory calculator for motor movement
 *
 * @copyright
 *        Unpublished Work Copyright 2021 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include <math.h>
#include "../../Common/common_types.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "trajectory.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief Set trajectory min and max speed. The min speed is the starting speed
 *        for the stepper motor. The max speed is the target speed
 * @param [in] trajectory - Trajectory instance
 *             max_speed - Target speed
 *             min_speed - Start speed
 *             step_frequency - frequency used for the stepper calculations
 * @return SUCCESS on SUCCESS
 */
ErrorStatus trajectory_set_speed(trajectory_s* trajectory,
                                 uint16_t max_speed,
                                 uint16_t min_speed,
                                 uint32_t step_frequency)
{
  ErrorStatus returnStatus = ERROR;
  if (max_speed <= step_frequency &&
      min_speed < step_frequency &&
      min_speed < max_speed &&
      step_frequency > 0) {
    trajectory->maxSpeed = ((float)max_speed) / step_frequency;
    trajectory->minSpeed = ((float)min_speed) / step_frequency;
    returnStatus = SUCCESS;
  }
  return returnStatus;
}

/**
 * @brief Set trajectory acceleration and min distance. Min distance is the
 *        disctance required to accelerate to min_speed. Trajectory minSpeed
 *        must be set since it is use for the minDistance calculation.
 * @param [in] trajectory - Trajectory instance
 *             acceleration - ACceleration
 *             step_frequency - frequency used for the stepper calculations
 * @return SUCCESS on SUCCESS
 */
ErrorStatus trajectory_set_acceleration(trajectory_s* trajectory,
                                        uint32_t acceleration,
                                        uint32_t step_frequency)
{
  ErrorStatus returnStatus = ERROR;
  if (trajectory->minSpeed != 0 || step_frequency > 0) {
    trajectory->maxAcceleration = ((float)acceleration) /
        (uint64_t)((uint64_t)step_frequency * (uint64_t)step_frequency);
    trajectory->minDistance =
        trajectory->minSpeed * trajectory->minSpeed /
        (2.0f * trajectory->maxAcceleration);
    returnStatus = SUCCESS;
  }
  return returnStatus;
}


/**
 * @brief Iterate trajectory algorithm one step.
 *        Shall be called from isr() function at defined frequency of 50kHz. The
 *        acceleration is set in steps of 500steps/isr^2. Minimum speed shall
 *        be set to 500 steps/s. This function is only used for calculating
 *        the trajectory for a test sequence that demands exact the same distance
 *        and acceleration time for different speeds. The motor will do a
 *        complete stop at the target position, no deceleartion
 * @param [in] distance - distance to target
 *             trajectory - trajectory instance to update
 * @return SUCCESS on SUCCESS
 */
ErrorStatus trajectory_calc_no_break(float distance, trajectory_s* trajectory) {
  trajectory->lastPosition = trajectory->position;
  if (distance <= -1.0) {
    trajectory->acceleration =
            -trajectory->maxSpeed - trajectory->speed;
  } else if (distance >= 1.0) {
    trajectory->acceleration =
            trajectory->maxSpeed - trajectory->speed;
  } else {
    // On target - set position to exactly target,
    // set speed and acceleration to exactly zero.
    trajectory->position = trajectory->target;
    trajectory->speed = 0;
    trajectory->acceleration  = 0;
  }

  // Limit acceleration, calculate new speed and position.
  trajectory->acceleration  = MAX(MIN(trajectory->acceleration ,trajectory->maxAcceleration), -trajectory->maxAcceleration);
  trajectory->speed += trajectory->acceleration;
  trajectory->position += trajectory->speed;

  return SUCCESS;
}

/**
 * @brief Iterate trajectory algorithm one step.
 *        Shall be called from isr() function at defined frequency.
 * @param [in] distance - distance to target
 *             trajectory - trajectory instance to update
 * @return SUCCESS on SUCCESS
 */
ErrorStatus trajectory_calc(float distance, trajectory_s* trajectory) {
  trajectory->lastPosition = trajectory->position;
  if (distance <= -1.0) {
    // Target requires negative movement
    // Subtract min_distance to end up at target position with min_speed.
    distance -= trajectory->minDistance;
    // Calculate optimum acceleration as difference between max allowed speed
    // to be able to stop at target and current speed. The 1.9 constant
    // should ideally be 2.0, but is set slightly lower to force breaking a
    // little earlier than ideal to assure that we actually stop in time
    // after rounding errors etc.
    trajectory->acceleration =
        MAX(-trajectory->maxSpeed, -sqrtf(-2.0f * distance * trajectory->maxAcceleration)) - trajectory->speed;
  } else if (distance >= 1.0) {
    // Target requires positive movement
    // Add min_distance to end up at target position with min_speed.
    distance += trajectory->minDistance;
    // Calculate optimum acceleration as difference between max allowed speed
    // to be able to stop at target and current speed.
    float d = sqrtf(1.9f * distance * trajectory->maxAcceleration);
    trajectory->acceleration =
        MIN(trajectory->maxSpeed, d) - trajectory->speed;
  } else if (trajectory->speed > trajectory->minSpeed) {
    // On target, but moving in positive direction.
    trajectory->acceleration = -trajectory->maxAcceleration;
  } else if (trajectory->speed < -trajectory->minSpeed) {
    // On target, but moving in negative direction.
    trajectory->acceleration = trajectory->maxAcceleration;
  } else {
    // On target and not moving too fast - set position to exactly target,
    // set speed and acceleration to exactly zero.
    trajectory->position = trajectory->target;
    trajectory->speed = 0;
    trajectory->acceleration  = 0;
  }

  // Limit acceleration, calculate new speed and position.
  trajectory->acceleration  = MAX(MIN(trajectory->acceleration ,trajectory->maxAcceleration), -trajectory->maxAcceleration);
  trajectory->speed += trajectory->acceleration;
  trajectory->position += trajectory->speed;

  return SUCCESS;
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */

/*
 * End of file: trajectory.c
 */
