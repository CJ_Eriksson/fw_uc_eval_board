/**
 * @file DRV8424.c
 * @brief Driver for the DRV8424 motor driver
 *
 * @copyright
 *        Unpublished Work Copyright 2021 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Hal/timer_a.h"
#include "../../Board/board.h"
#include "../../Common/common_types.h"
#include "../BEMF/BEMF_Sensor.h"
#include "../Time/Systime.h"
#include "trajectory.h"
#include "stepper.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "DRV8424.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
//#define MV_PER_MAMP 1.32f  // From datasheet might needed to be calibrated
#define MV_PER_MAMP 6.0f  // For DRV8834 1.2ohm

/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static gpio_s step;
static gpio_s dir;
static gpio_s fault;
static gpio_s sleep;
static gpio_s enable;
static gpio_s decay0;
static gpio_s decay1;
static gpio_s toff;
static gpio_s m0;
static gpio_s m1;
static timer_s* vrefTimer;
static trajectory_s motorTrajectory;
static uint16_t maxCurrent;
static uint16_t idleCurrent;
static uint16_t current;
static uint32_t stepAcceleration;
static bool stepperStepEnable = FALSE;
static stepper_step_mode_e stepperMode;
static bool_t home = FALSE;
static stepper_state_e state = STEPPER_IDLE;
 /*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */
/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
ErrorStatus drv8424_init(timer_s* vref_timer,
                         uint_fast8_t step_port, uint_fast16_t step_pin,
                         uint_fast8_t dir_port, uint_fast16_t dir_pin,
                         uint_fast8_t fault_port, uint_fast16_t fault_pin,
                         uint_fast8_t sleep_port, uint_fast16_t sleep_pin,
                         uint_fast8_t enable_port, uint_fast16_t enable_pin,
                         uint_fast8_t decay0_port, uint_fast16_t decay0_pin,
                         uint_fast8_t decay1_port, uint_fast16_t decay1_pin,
                         uint_fast8_t toff_port, uint_fast16_t toff_pin,
                         uint_fast8_t m0_port, uint_fast16_t m0_pin,
                         uint_fast8_t m1_port, uint_fast16_t m1_pin,
                         uint32_t acceleration, uint16_t min_speed, uint16_t max_speed, uint16_t max_current, uint16_t idle_current)
{
  vrefTimer = vref_timer;

  step.port = step_port;
  step.pin = step_pin;
  dir.port = dir_port;
  dir.pin = dir_pin;
  fault.port = fault_port;
  fault.pin = fault_pin;
  sleep.port = sleep_port;
  sleep.pin = sleep_pin;
  enable.port = enable_port;
  enable.pin = enable_pin;
  decay0.port = decay0_port;
  decay0.pin = decay0_pin;
  decay1.port = decay1_port;
  decay1.pin = decay1_pin;
  toff.port = toff_port;
  toff.pin = toff_pin;
  m0.port = m0_port;
  m0.pin = m0_pin;
  m1.port = m1_port;
  m1.pin = m1_pin;

  // Smart tune decay mode
  GPIO_setOutputLowOnPin(decay0.port, decay0.pin);
  GPIO_setOutputHighOnPin(decay1.port, decay1.pin);

  // Enable
  GPIO_setOutputLowOnPin(enable.port, enable.pin);
  GPIO_setOutputHighOnPin(sleep.port, sleep.pin);
  // 16us Toff
  GPIO_setOutputHighOnPin(toff.port, toff.pin);

  drv8424_set_speed(max_speed, min_speed);
  drv8424_set_acceleration(acceleration);

  motorTrajectory.lastPosition = motorTrajectory.position = 0.0f;
  motorTrajectory.speed = 0.0f;

  drv8424_set_current(max_current, idle_current);
  drv8424_set_stepmode(STEPPER_1_8_STEP);

  return SUCCESS;
}

ErrorStatus drv8424_sleep(bool_t put_to_sleep)
{
  if (put_to_sleep == TRUE) {
    GPIO_setOutputLowOnPin(sleep.port, sleep.pin);
  } else {
    GPIO_setOutputHighOnPin(sleep.port, sleep.pin);
  }
  return SUCCESS;
}

ErrorStatus drv8424_set_stepmode(stepper_step_mode_e step_mode)
{
  ErrorStatus returnStatus = SUCCESS;
  switch (step_mode) {
  case STEPPER_FULL_STEP: {
    // 0 0
    GPIO_setAsOutputPin(m0.port, m0.pin);
    GPIO_setAsOutputPin(m1.port, m1.pin);
    GPIO_setOutputLowOnPin(m0.port, m0.pin);
    GPIO_setOutputLowOnPin(m1.port, m1.pin);
    break;
  }
  case STEPPER_1_2_STEP: {
    // Hi-z 0
    GPIO_setAsOutputPin(m0.port, m0.pin);
    GPIO_setAsOutputPin(m1.port, m1.pin);
    GPIO_setOutputHighOnPin(m0.port, m0.pin);
    GPIO_setOutputLowOnPin(m1.port, m1.pin);
    break;
  }
  case STEPPER_1_4_STEP: {
    // 0 1
    GPIO_setAsInputPin(m0.port, m0.pin);
    GPIO_setAsOutputPin(m1.port, m1.pin);
    GPIO_setOutputLowOnPin(m1.port, m1.pin);
    break;
  }
  case STEPPER_1_8_STEP: {
    // 1 1
    GPIO_setAsOutputPin(m0.port, m0.pin);
    GPIO_setAsOutputPin(m1.port, m1.pin);
    GPIO_setOutputLowOnPin(m0.port, m0.pin);
    GPIO_setOutputHighOnPin(m1.port, m1.pin);
    break;
  }
  case STEPPER_1_16_STEP: {
    // Hi-z 1
    GPIO_setAsOutputPin(m0.port, m0.pin);
    GPIO_setAsOutputPin(m1.port, m1.pin);
    GPIO_setOutputHighOnPin(m0.port, m0.pin);
    GPIO_setOutputHighOnPin(m1.port, m1.pin);
    break;
  }
  case STEPPER_1_32_STEP: {
    // 0 Hi-z
    GPIO_setAsInputPin(m0.port, m0.pin);
    GPIO_setAsOutputPin(m1.port, m1.pin);
    GPIO_setOutputHighOnPin(m1.port, m1.pin);
    break;
  }
    default:
      returnStatus = ERROR;
      break;
  }
  stepperMode = step_mode;
  stepper_set_step_size(stepperMode);
  return returnStatus;
}
/*
ErrorStatus drv8424_set_stepmode(stepper_step_mode_e step_mode)
{
  ErrorStatus returnStatus = SUCCESS;
  switch (step_mode) {
  case STEPPER_FULL_STEP: {
    // 0 0
    GPIO_setAsOutputPin(m0.port, m0.pin);
    GPIO_setAsOutputPin(m1.port, m1.pin);
    GPIO_setOutputLowOnPin(m0.port, m0.pin);
    GPIO_setOutputLowOnPin(m1.port, m1.pin);
    break;
  }
  case STEPPER_1_2_STEP: {
    // Hi-z 0
    GPIO_setAsInputPin(m0.port, m0.pin);
    GPIO_setAsOutputPin(m1.port, m1.pin);
    GPIO_setOutputLowOnPin(m1.port, m1.pin);
    break;
  }
  case STEPPER_1_4_STEP: {
    // 0 1
    GPIO_setAsOutputPin(m0.port, m0.pin);
    GPIO_setAsOutputPin(m1.port, m1.pin);
    GPIO_setOutputLowOnPin(m0.port, m0.pin);
    GPIO_setOutputHighOnPin(m1.port, m1.pin);
    break;
  }
  case STEPPER_1_8_STEP: {
    // 1 1
    GPIO_setAsOutputPin(m0.port, m0.pin);
    GPIO_setAsOutputPin(m1.port, m1.pin);
    GPIO_setOutputHighOnPin(m0.port, m0.pin);
    GPIO_setOutputHighOnPin(m1.port, m1.pin);
    break;
  }
  case STEPPER_1_16_STEP: {
    // Hi-z 1
    GPIO_setAsInputPin(m0.port, m0.pin);
    GPIO_setAsOutputPin(m1.port, m1.pin);
    GPIO_setOutputHighOnPin(m1.port, m1.pin);
    break;
  }
  case STEPPER_1_32_STEP: {
    // 0 Hi-z
    GPIO_setAsOutputPin(m0.port, m0.pin);
    GPIO_setAsInputPin(m1.port, m1.pin);
    GPIO_setOutputLowOnPin(m0.port, m0.pin);
    break;
  }
  case STEPPER_1_64_STEP: {
    // NA
    returnStatus = ERROR;
    break;
  }
  case STEPPER_1_128_STEP: {
    // Hi-z Hi-z
    GPIO_setAsInputPin(m0.port, m0.pin);
    GPIO_setAsInputPin(m1.port, m1.pin);
    break;
  }
  case STEPPER_1_256_STEP: {
    // 1 Hi-z
    GPIO_setAsOutputPin(m0.port, m0.pin);
    GPIO_setAsInputPin(m1.port, m1.pin);
    GPIO_setOutputHighOnPin(m0.port, m0.pin);
    break;
  }
    default:
      returnStatus = ERROR;
      break;
  }
  stepperMode = step_mode;
  stepper_set_step_size(stepperMode);
  return returnStatus;
}
*/
ErrorStatus drv8424_set_current(uint16_t max_current, uint16_t idle_current)
{
  ErrorStatus returnStatus = ERROR;
  if (max_current < DRV8424_MAX_CURRENT && idle_current < DRV8424_MAX_CURRENT) {
    maxCurrent = max_current;
    idleCurrent = idle_current;
    returnStatus = SUCCESS;
  }
  return returnStatus;
}

ErrorStatus drv8424_set_speed(uint16_t max_speed, uint16_t min_speed)
{
  ErrorStatus returnStatus = ERROR;
  returnStatus = trajectory_set_speed(&motorTrajectory,
                                      max_speed,
                                      min_speed,
                                      STEP_FREQUENCY);
  if (returnStatus == SUCCESS) {
    returnStatus = trajectory_set_acceleration(&motorTrajectory,
                                               stepAcceleration,
                                               STEP_FREQUENCY);
  }

  return returnStatus;
}

ErrorStatus drv8424_stop() {
  motorTrajectory.target = motorTrajectory.position;
  motorTrajectory.speed = 0.0f;
  motorTrajectory.acceleration = 0.0f;
}

ErrorStatus drv8424_set_acceleration(uint32_t acceleration)
{
  ErrorStatus returnStatus = ERROR;
  returnStatus = trajectory_set_acceleration(&motorTrajectory,
                              acceleration,
                              STEP_FREQUENCY);
  stepAcceleration = acceleration;
  return returnStatus;
}

void drv8424_step()
{
  if (stepperStepEnable) {
    static int32_t stepPosition;  // Current position for the stepper motor
    if (motorTrajectory.target != motorTrajectory.position && current != maxCurrent) {
      if (stepper_set_current(vrefTimer,
                              (float)(MV_PER_MAMP * maxCurrent) / PROCESS_VOLTAGE)
          == SUCCESS) {
        current = maxCurrent;
      }
    } else if (motorTrajectory.target == motorTrajectory.position && current != idleCurrent) {
      if (stepper_set_current(vrefTimer,
                              (float)(MV_PER_MAMP * idleCurrent) / PROCESS_VOLTAGE)
          == SUCCESS) {
        current = idleCurrent;
      }
    }
/*
    stepper_step_no_trajectory(&stepPosition,
                               &motorTrajectory,
                               step,
                               dir,
                                           STEP_FREQUENCY/20);*/

    stepper_step(&stepPosition, &motorTrajectory, step, dir, stepperMode, &state);

    /*if (motorTrajectory.speed == motorTrajectory.maxSpeed) {
      GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_2);
    } else {
      GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_2);
    }*/
    /*if (motorTrajectory.position >= 640) {
      GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_2);
    } else {
      GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_2);
    }*/

  }
}

ErrorStatus drv8424_stepper_step_enable(bool_t enable_disable) {
  stepperStepEnable = enable_disable;
  bemf_init(A10);
}

ErrorStatus drv8424_set_decay(drv8424_decay_modes_e decay_mode) {
  GPIO_setAsOutputPin(decay0.port, decay0.pin);
  GPIO_setAsOutputPin(decay1.port, decay1.pin);
  switch (decay_mode) {
  case DRV8424_DECAY_SMART_DYNAMIC:
    GPIO_setOutputLowOnPin(decay0.port, decay0.pin);
    GPIO_setOutputLowOnPin(decay1.port, decay1.pin);
    break;
  case DRV8424_DECAY_SMART_RIPPLE:
    GPIO_setOutputLowOnPin(decay0.port, decay0.pin);
    GPIO_setOutputHighOnPin(decay1.port, decay1.pin);
    break;
  case DRV8424_DECAY_MIXED_30FAST:
    GPIO_setOutputHighOnPin(decay0.port, decay0.pin);
    GPIO_setOutputLowOnPin(decay1.port, decay1.pin);
    break;
  case DRV8424_DECAY_MIXED_60FAST:
    GPIO_setAsInputPin(decay0.port, decay0.pin);
    GPIO_setOutputLowOnPin(decay1.port, decay1.pin);
    break;
  case DRV8424_DECAY_SLOW:
    GPIO_setOutputHighOnPin(decay0.port, decay0.pin);
    GPIO_setOutputHighOnPin(decay1.port, decay1.pin);
    break;
  case DRV8424_DECAY_SLOW_BOTH:
    GPIO_setAsInputPin(decay0.port, decay0.pin);
    GPIO_setOutputHighOnPin(decay1.port, decay1.pin);
    break;
  }
  return SUCCESS;
}

ErrorStatus drv8424_homing(int32_t target)
{
  drv8424_set_target(target);
  home = FALSE;
  state = STEPPER_HOMING;
}

stepper_state_e drv8424_get_state()
{
  return state;
}

ErrorStatus drv8424_set_target(int32_t target_position)
{
  motorTrajectory.target = target_position;
  return SUCCESS;
}

float drv8424_get_speed()
{
  return motorTrajectory.speed;
}

float drv8424_get_position()
{
  return motorTrajectory.position;
}

/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */
/*
 * End of file: DRV8424.c
 */
