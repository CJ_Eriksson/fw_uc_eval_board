/**
 * @file ADC.h
 * @brief Driver for the ADC
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _ADC_H_
#define _ADC_H_

#include <stdint.h>
#include "../../Hal/adc14.h"
#include "../../Common/common_types.h"

/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */
#define MAX_NUMBER_OF_CHANNELS 32
#define ADC_MAX_CONV_ARRAY 256
#define ADC_REFERENCE_VOLTAGE 3300.0f
#define ADC_MAX_VALUE 16383  // Change if ADC resolution is changed
/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */
typedef enum {
  A0 = 0,
  A1,
  A2,
  A3,
  A4,
  A5,
  A6,
  A7,
  A8,
  A9,
  A10,
  A11,
  A12,
  A13,
  A14,
  A15,
  A16,
  A17,
  A18,
  A19,
  A20,
  A21,
  A22,
  A23
} adc_channel_e;
/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus adc_init(uint8_t number_of_channels,
                     uint8_t* ad_input_channel,
                     bool_t dma);
ErrorStatus adc_change_freq_and_sample_time(uint32_t clk_source,
                                            uint32_t pre_divider,
                                            uint32_t divider,
                                            uint32_t sample_and_hold);
ErrorStatus adc_start_meas(adc_channel_e channel,
                           bool_t continues,
                           uint32_t number_of_conversion,
                           uint16_t* conv_array);
ErrorStatus adc_stop_meas();
ErrorStatus adc_trigger_dma_conversion();
bool_t adc_check_meas_ready();
uint16_t adc_get_meas(uint32_t channel);
ErrorStatus adc_get_conv_arry(uint16_t* data, uint8_t nr_of_bytes);
bool_t adc_conversion_complete(void);
//TODO(caer) add check for running channel and array with last converted value for each channel

#ifdef __cplusplus
}
#endif

#endif /* _ADC_H_ */

/*
 * End of file: ADC.h
 */
