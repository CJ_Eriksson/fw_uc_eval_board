/**
 * @file ADC.c
 * @brief Driver for the ADC
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "ADC.h"
#include "../../HAL/systick.h"
#include "../../HAL/dma.h"
#include "../../HAL/gpio.h"
/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static uint32_t adcChannelsMask = 0;
static uint16_t* convArray;
static uint16_t* convArrayPnt;
static uint16_t* convArrayEndPnt;
static bool_t convArrayFull = FALSE;
static bool_t dmaEnabled = FALSE;
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */
static void ISR_adc();
/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief Initialization of the adc driver
 * @param [in]  number_of_channels - Total number of channels to be initialized
 *              ad_input_channel - Array of ADx input channels, size shall
 *                                 correspond to number_of_channels
 *              dma - use dma as transfer
 * @return SUCCESS on success
 */
ErrorStatus adc_init(uint8_t number_of_channels, uint8_t* ad_input_channel, bool_t dma)
{
  ErrorStatus returnStatus = ERROR;
  if (number_of_channels > MAX_NUMBER_OF_CHANNELS) {
    return returnStatus;
  }
  ADC14_enableModule();
  if (ADC14_initModule(ADC_CLOCKSOURCE_ADCOSC, ADC_PREDIVIDER_1,
                               ADC_DIVIDER_1, 0) == true) {
    // Configure memory to specific ADC conversion channel according to
    // ad_input_channel parameter.
    int16_t i = 0;
    for (i = 0; i < number_of_channels; ++i) {
      returnStatus = SUCCESS;
      if (ADC14_configureConversionMemory(1<<ad_input_channel[i],
                                          ADC_VREFPOS_AVCC_VREFNEG_VSS,
                                          ad_input_channel[i],
                                          false) != true) {
        return returnStatus = ERROR;
      }
      adcChannelsMask |= 1<<ad_input_channel[i];
    }
    ADC14_setSampleHoldTime(ADC_PULSE_WIDTH_32, ADC_PULSE_WIDTH_32); // TODO (caer) check what emory is used
  }
  if (returnStatus != ERROR) {
    ADC14_setResolution(ADC_14BIT);  // TODO(caer) Function to set variable to use in voltagecontrol for converion
    returnStatus = SUCCESS;
  }

  if (!dma) {
    ADC14_registerInterrupt(ISR_adc);
    ADC14_clearInterruptFlag(adcChannelsMask);
    ADC14_enableInterrupt(adcChannelsMask);
  } else {
    DMA_disableChannelAttribute(DMA_CH7_ADC14 ,
                                UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST |
                                UDMA_ATTR_HIGH_PRIORITY |
                                UDMA_ATTR_REQMASK);
    // Setting Control Indexes.
    DMA_setChannelControl(UDMA_PRI_SELECT | DMA_CH7_ADC14 ,
                          UDMA_SIZE_16 |
                          UDMA_SRC_INC_NONE |
                          UDMA_DST_INC_16 |
                          UDMA_ARB_1);
    /* Assigning/Enabling Interrupts */
    DMA_assignChannel(DMA_CH7_ADC14);
    dmaEnabled = TRUE;
  }
  //ADC14_registerInterrupt(ISR_adc);
  //ADC14_clearInterruptFlag(adcChannelsMask);
  //ADC14_enableInterrupt(adcChannelsMask);
  return returnStatus;
}

/**
 * @brief Changes the ADC sampling frequency depending on the clk and division
 *        factors. Also possible to change the sample and hold time
 * @param [in]  clk_source - Clock source for ADC
 *                 - \b ADC_CLOCKSOURCE_ADCOSC
 *                 - \b ADC_CLOCKSOURCE_SYSOSC
 *                 - \b ADC_CLOCKSOURCE_ACLK
 *                 - \b ADC_CLOCKSOURCE_MCLK
 *                 - \b ADC_CLOCKSOURCE_SMCLK
 *                 - \b ADC_CLOCKSOURCE_HSMCLK
 *              pre_divider - First divider
 *                 - \b ADC_PREDIVIDER_1
 *                 - \b ADC_PREDIVIDER_4
 *                 - \b ADC_PREDIVIDER_32
 *                 - \b ADC_PREDIVIDER_64
 *              divider - second divider stage
 *                 - \b ADC_DIVIDER_1
 *                 - \b ADC_DIVIDER_2
 *                 - \b ADC_DIVIDER_3
 *                 - \b ADC_DIVIDER_4
 *                 - \b ADC_DIVIDER_5
 *                 - \b ADC_DIVIDER_6
 *                 - \b ADC_DIVIDER_7
 *                 - \b ADC_DIVIDER_8
 *              sample_and_hold - Sample and hold for ADC conversion
 *                 - \b ADC_PULSE_WIDTH_4
 *                 - \b ADC_PULSE_WIDTH_8
 *                 - \b ADC_PULSE_WIDTH_16
 *                 - \b ADC_PULSE_WIDTH_32
 *                 - \b ADC_PULSE_WIDTH_64
 *                 - \b ADC_PULSE_WIDTH_96
 *                 - \b ADC_PULSE_WIDTH_128
 *                 - \b ADC_PULSE_WIDTH_192
 * @return SUCCESS on success
 */
ErrorStatus adc_change_freq_and_sample_time(uint32_t clk_source,
                                            uint32_t pre_divider,
                                            uint32_t divider,
                                            uint32_t sample_and_hold)
{
  ErrorStatus returnStatus = ERROR;
  ADC14_disableConversion();
  ADC14_enableModule();
  if (ADC14_initModule(clk_source,
                       pre_divider,
                       divider,
                       0) == true) {
    if (ADC14_setSampleHoldTime(sample_and_hold, sample_and_hold) == TRUE) {
      returnStatus = SUCCESS;
    }
  }
  return returnStatus;
}

/**
 * @brief Start measurement of the selected channel in single or continues mode
 *        In single mode the busy function can be used for check if conversion
 *        is ready. In continues mode the number of conversion can be checked
 *        and then stop the conversions by issuing a conversion stop.
 * @param [in]  channel - Channel to be sampled and the corresponding ADC14MEMx
 *                        register holds the result. Channel A0 -> MEM0 etc.
 *              continues - Use single conversion in continues mode
 *              number_of_converison - Total number of conversion to be added to
 *                                     result array
 *              conv_array - Array to store conversion result. The user makes
 *                           sure thtat the array is not going out of scope.
 * @return SUCCESS on success
 */
ErrorStatus adc_start_meas(adc_channel_e channel,
                           bool_t continues,
                           uint32_t number_of_conversion,
                           uint16_t* conv_array)
{
  ErrorStatus returnStatus = ERROR;
  if (number_of_conversion > ADC_MAX_CONV_ARRAY) {
    return returnStatus;
  }
  if (conv_array == NULL) {
    return returnStatus;
  } else {
    convArray = conv_array;
  }

  // Check if channel is initialized
  uint8_t memChannel = 0;
  for (memChannel = 0; memChannel < MAX_NUMBER_OF_CHANNELS; ++memChannel) {
    if (ADC14_checkMemoryChannel(memChannel) == (uint32_t)channel) {
      returnStatus = SUCCESS;
      break;
    }
  }

  if (returnStatus == SUCCESS) {
    returnStatus = ERROR;

    ADC14_disableConversion();  // TODO(caer) is this ok or shall we not allow a new conversion to start when another is ongoing?
    while(ADC14_isBusy());  // Need to wait until conversion is ready after disabling. Otherwise the start will fail
    if (!dmaEnabled) {
      convArrayPnt = &convArray[0];
      convArrayEndPnt = &convArray[number_of_conversion];
      convArrayFull = FALSE;
    }

    if (number_of_conversion > ADC_MAX_CONV_ARRAY) {
      return returnStatus;
    }
    ADC14_configureSingleSampleMode(channel, continues);
    if (continues) {
      if (ADC14_enableSampleTimer(ADC_AUTOMATIC_ITERATION) == true) {
        returnStatus = SUCCESS;
      }
    } else {
      if (ADC14_enableSampleTimer(ADC_MANUAL_ITERATION) == true) {
        returnStatus = SUCCESS;
      }
    }

    if (returnStatus == SUCCESS) {
      returnStatus = ERROR;
      if (ADC14_enableConversion() == true) {
        if (ADC14_toggleConversionTrigger() == true) {
          if (dmaEnabled) {
            DMA_clearInterruptFlag(DMA_CH7_RESERVED0); // interrupt flag is used for check that data is ready
            DMA_setChannelTransfer(UDMA_PRI_SELECT | DMA_CH7_ADC14 ,
                                   UDMA_MODE_BASIC, (void*) &ADC14->MEM[(uint8_t)channel],
                                   (void*)convArray, number_of_conversion);
            DMA_enableChannel(7);
          }
              returnStatus = SUCCESS;
        }
      }
    }
  }
  return returnStatus;
}

/**
 * @brief Start measurement of the selected channels in single or continues mode
 *        In single mode the busy function can be used for check if conversion
 *        is ready. In continues mode the number of conversion can be checked
 *        and then stop the conversions by issuing the conversion is stop.
 * @param [in]  channel - Channels to be sampled and the corresponding ADC14MEMx
 *                        register holds the result. Channel A0 -> MEM0 etc.
 *              continues - Use single conversion in continues mode
 *              number_of_converison - Total number of conversion to be added to
 *                                     result array
 *              conv_array - Array to store conversion result. The user makes
 *                           sure thtat the array is not going out of scope.
 * @return SUCCESS on success
 */
ErrorStatus adc_start_meas_multi(adc_channel_e* channels,
                                 uint8_t number_of_channels,
                                 bool_t continues)
{
  ErrorStatus returnStatus = ERROR;
  uint8_t i = 0;
  for (i = 0; i < number_of_channels; ++i) {
    if (ADC14_configureConversionMemory(1<<i,
                                        ADC_VREFPOS_AVCC_VREFNEG_VSS,
                                        channels[i],
                                        false) != true) {
      return returnStatus = ERROR;
    }
  }

  ADC14_disableConversion();  // TODO(caer) is this ok or shall we not allow a new conversion to start when another is ongoing?
  while(ADC14_isBusy());  // Need to wait until conversion is ready after disabling. Otherwise the start will fail

  ADC14_configureMultiSequenceMode(ADC_MEM0, 1 << (number_of_channels - 1), continues);
  if (continues) {
    if (ADC14_enableSampleTimer(ADC_AUTOMATIC_ITERATION) == true) {
      returnStatus = SUCCESS;
    }
  } else {
    if (ADC14_enableSampleTimer(ADC_MANUAL_ITERATION) == true) {
      returnStatus = SUCCESS;
    }
  }

  if (returnStatus == SUCCESS) {
    returnStatus = ERROR;
    if (ADC14_enableConversion() == true) {
      if (ADC14_toggleConversionTrigger() == true) {
            returnStatus = SUCCESS;
      }
    }
  }
  return returnStatus;
}

/**
 * @brief Stop measurement
 * @param [in]
 * @return SUCCESS on success
 */
ErrorStatus adc_stop_meas()
{
  ADC14_disableConversion();
  return SUCCESS;
}

ErrorStatus adc_trigger_dma_conversion()
{
  ErrorStatus returnStatus = ERROR;
  if (ADC14_enableConversion() == true) {
    if (ADC14_toggleConversionTrigger() == true) {
      DMA_clearInterruptFlag(DMA_CH7_RESERVED0); // interrupt flag is used for check that data is ready
      DMA_enableChannel(7);
      returnStatus = SUCCESS;
    }
  }
}
/**
 * @brief Check if convesion is busy. NOTE! Only valid in non continues mode.
 *        IN continues mode this will always return busy.
 *        In DMA mode the function is returning ready when DMA has transfered
 *        all data to the array
 * @param [in]  -
 * @return TRUE if conversion is busy
 */
bool_t adc_check_meas_ready() {
  bool_t returnBool = FALSE;
  if (!dmaEnabled) {
    if(ADC14_isBusy() == true) {
      returnBool = FALSE;
    } else {
      returnBool = TRUE;
    }
  } else {
    if ((DMA_getInterruptStatus() & (1 << DMA_CH7_RESERVED0)) != 0) {
      returnBool = TRUE;
    } else {
      returnBool = FALSE;
    }
  }

  return returnBool;
}

/**
 * @brief Get measurement from corresponding memory bank
 * @param [in]  channel - Channel to be read.
 * @return Result - ADC conversion for selected channel
 */
uint16_t adc_get_meas(uint32_t channel) {
  return ADC14_getResult(1<<channel);  // Get result is bit masked memory position
}

/**
 * @brief Get ADC result array
 * @param [out]  data - pointer to data
 * @return SUCCESS on SUCCESS
 */
ErrorStatus adc_get_conv_arry(uint16_t* data, uint8_t nr_of_bytes)
{
  ErrorStatus returnStatus = ERROR;
  if (nr_of_bytes > ADC_MAX_CONV_ARRAY) {
    return returnStatus;
  }
  uint8_t byte = 0;
  for (byte = 0; byte < nr_of_bytes; ++byte) {
    data[byte] = convArray[byte];
  }
  returnStatus = SUCCESS;

  return returnStatus;
}
/**
 * @brief Check if measurement array is filled with selected number of
 *        conversions requested in start function aka conversion completed
 * @param [in]
 * @return TRUE if array is filled
 */
bool_t adc_conversion_complete(void) {
  bool_t returnBool = FALSE;
  if (dmaEnabled) {
    if ((DMA_getInterruptStatus() & (1<<DMA_CH7_RESERVED0)) != 0) {
      returnBool = TRUE;
    }
  } else {
    returnBool = convArrayFull;
  }
  return returnBool;
}

/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */
static void ISR_adc() {

  uint_fast64_t interruptMask = ADC14_getEnabledInterruptStatus();
  uint8_t channel = 0;
  for (channel = 0; channel < MAX_NUMBER_OF_CHANNELS; ++channel) {
    if ((interruptMask & (1 << channel)) != 0) {
      *convArrayPnt++ = ADC14_getResult(1<<channel);
      ADC14_clearInterruptFlag(1<<channel);
      break;
    }
  }
  if (convArrayPnt == convArrayEndPnt) {
    convArrayPnt = &convArray[0];
    convArrayFull = TRUE;
    ADC14_disableConversion();
  }
  ADC14_clearInterruptFlag(0xFFFFFFFF);
  GPIO_toggleOutputOnPin(2,0x40);
}
/*
 * End of file: ADC.c
 */
