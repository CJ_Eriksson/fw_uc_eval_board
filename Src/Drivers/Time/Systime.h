/**
 * @file Systime.h
 * @brief Driver for the system time
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */
/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _SYSTIME_H_
#define _SYSTIME_H_
#include <stdint.h>
#include "../../Common/common_types.h"
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */
typedef enum
{
  SYSTIME_TICK_FREQ_10HZ         = 100U,
  SYSTIME_TICK_FREQ_100HZ        = 10U,
  SYSTIME_TICK_FREQ_1kHZ         = 1U,
  HAL_TICK_FREQ_DEFAULT      = SYSTIME_TICK_FREQ_1kHZ
} systime_tick_freq_e;

/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus systime_init(systime_tick_freq_e freq);
uint32_t systime_get_tick(void);
void systime_reset_tick(void);

void systime_wait_ms(uint32_t delay);

#ifdef __cplusplus
}
#endif

#endif /* _SYSTIME_H_ */

/*
 * End of file: SystimeI.h
 */
