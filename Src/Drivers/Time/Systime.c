/**
 * @file Systime.c
 * @brief Driver for the system time
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Common/common_types.h"
#include "msp.h"
#include "../../Hal/systick.h"
#include "../../Hal/rtc_c.h"

#include "Systime.h"
/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define IS_TICKFREQ(FREQ) (((FREQ) == SYSTIME_TICK_FREQ_10HZ)  || \
                           ((FREQ) == SYSTIME_TICK_FREQ_100HZ) || \
                           ((FREQ) == SYSTIME_TICK_FREQ_1kHZ))
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static uint32_t tick = 0;
static systime_tick_freq_e tickFreq= SYSTIME_TICK_FREQ_1kHZ;
static uint32_t msPeriod;
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */
static void systick_ISR();
/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */

/**
 * @brief Initialization of the systime. The systick clock is calibrated to the
 * RTC clock. If RTC clock is not running, the init is not possible and it
 * returns error. The calibration and reset to the RTC requires minimum 2s.
 * @param [in] freq - tick increment
 * @return SUCCESS on success
 */
ErrorStatus systime_init(systime_tick_freq_e freq)
{
  ErrorStatus returnStatus = ERROR;
  if (!IS_TICKFREQ(freq) || RTC_C_on_hold()) {
    return returnStatus;
  }
  // Calibrate tick counter to 1ms
  SysTick_setPeriod(0xFFFFFF);
  uint32_t tickTemp;
  RTC_C_Calendar calTime = RTC_C_getCalendarTime();
  while(calTime.seconds == RTC_C_getCalendarTime().seconds);
  SysTick_enableModule();
  calTime.seconds = RTC_C_getCalendarTime().seconds;
  while(calTime.seconds == RTC_C_getCalendarTime().seconds);
  tickTemp = SysTick_getValue();
  msPeriod = 0xFFFFFF - tickTemp ;
  SysTick_disableModule();

  // Add the number of overflows for the tick counter during 1s for different
  // SystemCoreClock values.
  if (SystemCoreClock > 2 * 0xFFFFFF) {
    msPeriod += (2 * 0xFFFFFF);
  } else if (SystemCoreClock >= 0xFFFFFF) {
    msPeriod += 0xFFFFFF;
  }

  /* Configure the SysTick to have interrupt in 1ms time basis*/
  SysTick_setPeriod((msPeriod * freq)/1000);
  if (SysTick_getPeriod() == (msPeriod * freq)/1000) {
    returnStatus = SUCCESS;
    Interrupt_setPriority(15, 0);
    SysTick_enableModule();
    SysTick_enableInterrupt();
    SysTick_registerInterrupt(systick_ISR);
  }
  // Reset tick to RTC time
  while(calTime.seconds == RTC_C_getCalendarTime().seconds);
  systime_reset_tick();

  return returnStatus;
}

/**
 * @brief Get system tick value
 * @param [in]
 * @return Current systime tick value
 */
uint32_t systime_get_tick(void)
{
  return tick;
}

/**
 * @brief Reset system tick value
 * @param [in]
 * @return
 */
void systime_reset_tick(void)
{
  SysTick_setPeriod((msPeriod * tickFreq)/1000);
  tick = 0;
}

/**
 * @brief Wait delay number of ms
 * @param [in] delay - delay time in ms
 * @return
 */
#pragma GCC push_options
#pragma GCC optimize("O0")
#pragma FUNCTION_OPTIONS(systime_wait_ms, "--opt_level=off")
void systime_wait_ms(uint32_t delay)
{

  volatile uint32_t tickstart = tick;
  volatile uint32_t wait = delay;

  /* Add freq to guarantee minimum wait */
  if (wait < MAX_UINT32) {
    wait += (uint32_t)(tickFreq);
  }

  while(TRUE) {
    if ((tick - tickstart) >= wait) {
      break;
    }
  }
}
#pragma GCC pop_options
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief ISR for systick timer
 * @param [in]
 * @return
 */
static void systick_ISR()
{
  tick+=tickFreq;
}
/*
 * End of file: Systime.c
 */
