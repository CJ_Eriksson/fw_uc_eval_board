/**
 * @file timer.c
 * @brief Driver for the hal timers
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */
//TODO(caer) rename to intervaltimer
/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Hal/timer_a.h"
#include "../../Hal/gpio.h"
#include "../../Board/board.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "timer.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */
#define ACLK_FREQ 32768
/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
static timer_interval_s intervalTimers[5];

/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */
void enable_timer(timer_interval_s timer);
void disable_timer(timer_interval_s timer);
void isr_ccr0(void);
void isr_ccrX(void);
/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
/**
 * @brief Initialize of interval isr timer.
 * @param [in] isr_period_1_ms - First period interval
 *             isr1Handler - function pointer to isr to be handled at interval
 *             specified in isr_period_1_ms
 *             isr_period_2_ms - Second period interval
 *             isr2Handler - function pointer to isr to be handled at interval
 *             specified in isr_period_2_ms
 *             If NULL ptr for function, the interval is not enabled
 * @return SUCCESS on SUCCESS
 */
ErrorStatus timer_A0_interval_init(timer_interval_s timers[5])
{
  ErrorStatus returnStatus = ERROR;

  Timer_A_ContinuousModeConfig timerA0ContConfig;
  timerA0ContConfig.clockSource = TIMER_A_CLOCKSOURCE_ACLK;
  timerA0ContConfig.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
  timerA0ContConfig.timerClear = TIMER_A_SKIP_CLEAR;
  timerA0ContConfig.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_DISABLE;
  Timer_A_configureContinuousMode(TIMER_A0_BASE, &timerA0ContConfig);

  uint8_t timer = 0;
  Timer_A_CompareModeConfig timerA0CompConfig;
  for (timer = 0; timer < 5; ++timer) { // Loop through all interval timers and set.
    if (timers[timer].interval >= (2*ACLK_FREQ - 1)) {  // MAx 2s interval
      return returnStatus;
    }
    if (timers[timer].name != NULL) {
      enable_timer(timers[timer]);
    }
    intervalTimers[timer] = timers[timer];
  }
  Timer_A_startCounter(TIMER_A0_BASE, TIMER_A_CONTINUOUS_MODE);

  return returnStatus;
}

ErrorStatus timer_A0_register_interval(timer_interval_s interval_timer)
{
  ErrorStatus returnStatus = ERROR;
  uint8_t timer = 0;
  for (timer = 0; timer < 5; ++timer) {
    if (interval_timer.ccrRegister ==  intervalTimers[timer].ccrRegister) {
      break;  // Timer already registered at that CC register
    }
  }
  if (timer == 5) {
    for (timer = 0; timer < 5; ++timer) {
      if (intervalTimers[timer].name == NULL) { // Empty interval timer
        intervalTimers[timer] = interval_timer;
        break;
      }
    }
    if (timer < 5) {  // All timer interval instances were not occupied
        enable_timer(intervalTimers[timer]);
        returnStatus = SUCCESS;
      }
  }

  return returnStatus;
}

ErrorStatus timer_A0_deregister_interval(char* interval_timer)
{
  ErrorStatus returnStatus = ERROR;
  uint8_t timer = 0;
  for (timer = 0; timer < 5; ++timer) {
    if (*intervalTimers[timer].name == *interval_timer) {
      disable_timer(intervalTimers[timer]);
      break;
    }
  }

  if (timer < 5) {  // Timer found and disable
    intervalTimers[timer].name = NULL;
    intervalTimers[timer].ccrRegister = 0;
    returnStatus = SUCCESS;
  }

  return returnStatus;
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */
void enable_timer(timer_interval_s timer) {
  Timer_A_CompareModeConfig timerA0CompConfig;
  timerA0CompConfig.compareInterruptEnable =
      TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE;
  timerA0CompConfig.compareOutputMode = TIMER_A_OUTPUTMODE_TOGGLE;
  timerA0CompConfig.compareRegister = timer.ccrRegister;
  timerA0CompConfig.compareValue = timer.interval
      + Timer_A_getCounterValue(TIMER_A0_BASE);
  Timer_A_initCompare(TIMER_A0_BASE, &timerA0CompConfig);


  if(timer.ccrRegister == TIMER_A_CAPTURECOMPARE_REGISTER_0) {
    Timer_A_registerInterrupt(TIMER_A0_BASE,
                              TIMER_A_CCR0_INTERRUPT,
                              isr_ccr0);
   } else {
     Timer_A_registerInterrupt(TIMER_A0_BASE,
                               TIMER_A_CCRX_AND_OVERFLOW_INTERRUPT,
                               isr_ccrX);
   }
  Timer_A_clearInterruptFlag(TIMER_A0_BASE);
  Timer_A_clearCaptureCompareInterrupt(TIMER_A0_BASE, timer.ccrRegister);
}

void disable_timer(timer_interval_s timer) {
  Timer_A_CompareModeConfig timerA0CompConfig;
  timerA0CompConfig.compareInterruptEnable =
      TIMER_A_CAPTURECOMPARE_INTERRUPT_DISABLE;
  timerA0CompConfig.compareOutputMode = TIMER_A_OUTPUTMODE_TOGGLE;
  timerA0CompConfig.compareRegister = timer.ccrRegister;
  timerA0CompConfig.compareValue = 0;
  Timer_A_clearCaptureCompareInterrupt(TIMER_A0_BASE, timer.ccrRegister);
  Timer_A_initCompare(TIMER_A0_BASE, &timerA0CompConfig);
}
/**
 * @brief ISR for timer interrupt from TimerA0
 * @param [in]
 * @return None
 */
// TODO(caer) CHECK INLINE
void isr_ccr0(void)
{
  uint8_t timer = 0;
  for (timer; timer < 5; ++timer) {
    if (intervalTimers[timer].ccrRegister == TIMER_A_CAPTURECOMPARE_REGISTER_0) {
      if (Timer_A_getCaptureCompareEnabledInterruptStatus(TIMER_A0_BASE,
                                                          intervalTimers[timer].ccrRegister)) {
        Timer_A_setCompareValue(TIMER_A0_BASE, intervalTimers[timer].ccrRegister, Timer_A_GetCompareValue(TIMER_A0_BASE, intervalTimers[timer].ccrRegister) + intervalTimers[timer].interval);
        Timer_A_clearInterruptFlag(TIMER_A0_BASE);
        Timer_A_clearCaptureCompareInterrupt(TIMER_A0_BASE, intervalTimers[timer].ccrRegister);
        intervalTimers[timer].intHandler();
      }
    }
  }
}

void isr_ccrX(void)
{

  uint8_t timer = 0;
  for (timer; timer < 5; ++timer) {
    if (intervalTimers[timer].ccrRegister != TIMER_A_CAPTURECOMPARE_REGISTER_0) {
      if (Timer_A_getCaptureCompareEnabledInterruptStatus(TIMER_A0_BASE,
                                                          intervalTimers[timer].ccrRegister)) {

        Timer_A_setCompareValue(TIMER_A0_BASE, intervalTimers[timer].ccrRegister, Timer_A_GetCompareValue(TIMER_A0_BASE, intervalTimers[timer].ccrRegister) + intervalTimers[timer].interval);
        Timer_A_clearInterruptFlag(TIMER_A0_BASE);
        Timer_A_clearCaptureCompareInterrupt(TIMER_A0_BASE, intervalTimers[timer].ccrRegister);
        intervalTimers[timer].intHandler();
        break;
      }
    }
  }

}
/*
 * End of file: timer.c
 */
