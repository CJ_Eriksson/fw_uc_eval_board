/**
 * @file timer.h
 * @brief Driver for the hal timers
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */
/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _TIMER_H_
#define _TIMER_H_

#include "../../Common/common_types.h"
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */
#define TIMER_A1_MAX_INTERVAL_MS 2000
/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */
typedef struct {
  char* name;
  uint8_t ccrRegister;
  void (*intHandler)(void);
  uint16_t interval;
} timer_interval_s;
/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus timer_A0_interval_init(timer_interval_s timers[5]);
ErrorStatus timer_A0_register_interval(timer_interval_s interval_timer);
ErrorStatus timer_A0_deregister_interval(char* interval_timer);

#ifdef __cplusplus
}
#endif

#endif /* _TIMER_H_ */

/*
 * End of file: timerr.h
 */
