/**
 * @file encoder.c
 * @brief Encoder handler
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../../Common/common_types.h"
#include "../../Hal/gpio.h"
#include "../../Board/board.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "encoder.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */
uint32_t encoderCount;
uint32_t state;
uint32_t prevState;
int32_t quadEncoderMatrix[4][4] = {{0,-1,1,2},{1,0,2,-1},{-1,2,0,1},{2,1,-1,0}};
gpio_s encA;
gpio_s encB;
/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */
static inline void change_transition();
/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
ErrorStatus encoder_init(uint_fast8_t enca_port, uint_fast16_t enca_pin,
                         uint_fast8_t encb_port, uint_fast16_t encb_pin)
{
  encA.port = enca_port;
  encA.pin = enca_pin;
  encB.port = encb_port;
  encB.pin = encb_pin;
  state = (GPIO_getInputPinValue(encA.port, encA.pin) << 1 | GPIO_getInputPinValue(encB.port, encB.pin));
  prevState = state;
  change_transition();

  GPIO_enableInterrupt(encA.port, encA.pin);
  GPIO_enableInterrupt(encB.port, encB.pin);

  return SUCCESS;
}

uint32_t encoder_get_cnt()
{
  return encoderCount;
}

void encoder_isr()
{

  state = (GPIO_getInputPinValue(encA.port, encA.pin) << 1 | GPIO_getInputPinValue(encB.port, encB.pin));
  int32_t cnt = quadEncoderMatrix[prevState][state];
  if (cnt != 2) {
    encoderCount += cnt;  // intentional over/underflow.
  } else {

    encoderCount = 0xABCDEF;
  }
  prevState = state;
  change_transition();
  // TODO(caer) Add error if cnt = 2
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */
static inline void change_transition()
{
  if ((state & 0x02) != 0) {
    GPIO_interruptEdgeSelect(encA.port,
                             encA.pin,
                             GPIO_HIGH_TO_LOW_TRANSITION);
  } else {
    GPIO_interruptEdgeSelect(encA.port,
                             encA.pin,
                             GPIO_LOW_TO_HIGH_TRANSITION);
  }

  if ((state & 0x01) != 0) {
    GPIO_interruptEdgeSelect(encB.port,
                             encB.pin,
                             GPIO_HIGH_TO_LOW_TRANSITION);
  } else {
    GPIO_interruptEdgeSelect(encB.port,
                             encB.pin,
                             GPIO_LOW_TO_HIGH_TRANSITION);
  }
}
/*
 * End of file: template.c
 */
