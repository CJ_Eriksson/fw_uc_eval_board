/**
 * @file board.c
 * @brief A description of the module�s purpose.
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Includes
 * -----------------------------------------------------------------------------
 */
#include "../Common/common_types.h"
#include "../Hal/interrupt.h"
/*
 * -----------------------------------------------------------------------------
 * Application Includes
 * -----------------------------------------------------------------------------
 */
#include "board.h"

/*
 * -----------------------------------------------------------------------------
 * Private defines
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private constant definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private variable definitions
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Private function prototypes
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public functions definitions
 * -----------------------------------------------------------------------------
 */
ErrorStatus board_init()
{
  ErrorStatus returnStatus = ERROR;
  // Init GPIO inputs to be ADC inputs
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_4,
                                             GPIO_PIN_VIN_VSENSE,
                                             GPIO_TERTIARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_4,
                                             GPIO_PIN_MOTOR_V_SENSE,
                                             GPIO_TERTIARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_4,
                                             GPIO_PIN_SUPER_CAP_V_SENSE,
                                             GPIO_TERTIARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_4,
                                             GPIO_PIN_P4_2,
                                             GPIO_TERTIARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_5,
                                             GPIO_PIN_PRESSURE_B,
                                             GPIO_TERTIARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_5,
                                             GPIO_PIN_PRESSURE_A,
                                             GPIO_TERTIARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_4,
                                             GPIO_PIN_MD2_BEMF,
                                             GPIO_TERTIARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_4,
                                             GPIO_PIN_MD1_BEMF,
                                             GPIO_TERTIARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_8,
                                             GPIO_PIN_P8_7,
                                             GPIO_TERTIARY_MODULE_FUNCTION);


  // GPIO Timer output
  /*GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_6,
                                             GPIO_PIN_MD1_CTRL_VREF_PWM,
                                             GPIO_PRIMARY_MODULE_FUNCTION);
*/
  /*
  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_7,
                                              GPIO_PIN_MD2_A_IN1_PH,
                                              GPIO_PRIMARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_7,
                                              GPIO_PIN_MD2_A_IN2_EN,
                                              GPIO_PRIMARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_7,
                                              GPIO_PIN_MD2_B_IN1_PH,
                                              GPIO_PRIMARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_7,
                                              GPIO_PIN_MD2_B_IN2_EN,
                                              GPIO_PRIMARY_MODULE_FUNCTION);
*/
  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_5,
                                              0x40,
                                              GPIO_PRIMARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_5,
                                              0x80,
                                              GPIO_PRIMARY_MODULE_FUNCTION);

  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_6,
                                              GPIO_PIN_MD1_CTRL_VREF_PWM,
                                              GPIO_PRIMARY_MODULE_FUNCTION);





  // GPIO I2C
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_1,
                                             GPIO_PIN_SDA_SENSOR1,
                                             GPIO_PRIMARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_1,
                                             GPIO_PIN_SCL_SENSOR1,
                                             GPIO_PRIMARY_MODULE_FUNCTION);

  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_3,
                                             GPIO_PIN_SDA_SENSOR2,
                                             GPIO_PRIMARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_3,
                                             GPIO_PIN_SCL_SENSOR2,
                                             GPIO_PRIMARY_MODULE_FUNCTION);

  // GPIO UART
#ifdef TMC2300LA_UART
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_2,
                                             GPIO_PIN_MD3_UART_RX,
                                             GPIO_PRIMARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_2,
                                             GPIO_PIN_MD3_UART_TX,
                                             GPIO_PRIMARY_MODULE_FUNCTION);
#endif
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_1,
                                             GPIO_PIN_UART_RX,
                                             GPIO_PRIMARY_MODULE_FUNCTION);

  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_1,
                                             GPIO_PIN_UART_TX,
                                             GPIO_PRIMARY_MODULE_FUNCTION);
  // GPIO SPI
  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_6,
                                             GPIO_PIN_SPIB1_CLK,
                                             GPIO_PRIMARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_6,
                                             GPIO_PIN_SPIB1_SIMO,
                                             GPIO_PRIMARY_MODULE_FUNCTION);
  GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_6,
                                     GPIO_PIN_SPIB1_SOMI);  // Missing pull-up on pin
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_6,
                                             GPIO_PIN_SPIB1_SOMI,
                                             GPIO_PRIMARY_MODULE_FUNCTION);

  // Init GPIOs for output
  GPIO_setAsOutputPin(GPIO_PORT_1, GPIO_PIN_RES_SENSOR1);

  GPIO_setAsOutputPin(GPIO_PORT_2, GPIO_PIN_MOTOR_V_EN);
  GPIO_setAsOutputPin(GPIO_PORT_2, GPIO_PIN_SUPER_CAP_CHRG_EN);
  GPIO_setAsOutputPin(GPIO_PORT_2, GPIO_PIN_SUPER_CAP_V_SENSE_EN);
  GPIO_setAsOutputPin(GPIO_PORT_2, GPIO_PIN_MD3_CTRL_EN);
  GPIO_setOutputLowOnPin(GPIO_PORT_2, GPIO_PIN_MD3_CTRL_EN);
#ifndef TMC2300LA_UART
  GPIO_setAsOutputPin(GPIO_PORT_2, GPIO_PIN_MD3_UART_RX);
#endif

  GPIO_setAsOutputPin(GPIO_PORT_3, GPIO_PIN_RES_SENSOR2);

  GPIO_setAsOutputPin(GPIO_PORT_4, GPIO_PIN_SUPER_CAP_BAT_EN);

  GPIO_setAsOutputPin(GPIO_PORT_6, GPIO_PIN_MD1_CTRL_STEP);
  GPIO_setAsOutputPin(GPIO_PORT_6, GPIO_PIN_MD1_CTRL_DIR);
  GPIO_setAsOutputPin(GPIO_PORT_6, GPIO_PIN_MD1_CTRL_NSLEEP);

  GPIO_setAsOutputPin(GPIO_PORT_7, GPIO_PIN_VIN_VSENSE_EN);
  GPIO_setAsOutputPin(GPIO_PORT_7, GPIO_PIN_V_ANALOG_EN);
  GPIO_setAsOutputPin(GPIO_PORT_7, GPIO_PIN_MD1_CTRL_EN);


  GPIO_setAsOutputPin(GPIO_PORT_7, GPIO_PIN_MD2_A_IN1_PH);
  GPIO_setAsOutputPin(GPIO_PORT_7, GPIO_PIN_MD2_A_IN2_EN);
  GPIO_setAsOutputPin(GPIO_PORT_7, GPIO_PIN_MD2_B_IN1_PH);
  GPIO_setAsOutputPin(GPIO_PORT_7, GPIO_PIN_MD2_B_IN2_EN);


  GPIO_setAsOutputPin(GPIO_PORT_8, GPIO_PIN_MD3_PWR_EN);
  GPIO_setAsOutputPin(GPIO_PORT_8, GPIO_PIN_M3_CTRL_STEP);
  GPIO_setAsOutputPin(GPIO_PORT_8, GPIO_PIN_M3_CTRL_DIR);
  GPIO_setAsOutputPin(GPIO_PORT_8, GPIO_PIN_MD3_MS1_AD0);
  GPIO_setAsOutputPin(GPIO_PORT_8, GPIO_PIN_MD3_MS2_AD1);
  GPIO_setAsOutputPin(GPIO_PORT_8, GPIO_PIN_MD3_MODE);
  GPIO_setAsOutputPin(GPIO_PORT_8, GPIO_PIN_MD3_STEPPER);
  //GPIO_setAsOutputPin(GPIO_PORT_8, GPIO_PIN_P8_7);

  GPIO_setAsOutputPin(GPIO_PORT_9, GPIO_PIN_MD1_PWR_EN);
  GPIO_setAsOutputPin(GPIO_PORT_9, GPIO_PIN_MD2_PWR_EN);
  GPIO_setAsOutputPin(GPIO_PORT_9, GPIO_PIN_MD1_DECAY_1);
  GPIO_setAsOutputPin(GPIO_PORT_9, GPIO_PIN_MD1_DECAY_0);

  GPIO_setAsOutputPin(GPIO_PORT_10, GPIO_PIN_P10_2);
  GPIO_setAsOutputPin(GPIO_PORT_10, GPIO_PIN_P10_3);
  GPIO_setAsOutputPin(GPIO_PORT_10, GPIO_PIN_P10_4);
  GPIO_setAsOutputPin(GPIO_PORT_10, GPIO_PIN_P10_5);

  GPIO_setAsOutputPin(GPIO_PORT_J, GPIO_PIN_SD_CARD_CS);
  GPIO_setOutputHighOnPin(GPIO_PORT_J, GPIO_PIN_SD_CARD_CS);
  GPIO_setAsOutputPin(GPIO_PORT_J, GPIO_PIN_ACC_CS);
  GPIO_setOutputHighOnPin(GPIO_PORT_J, GPIO_PIN_ACC_CS);
  GPIO_setAsOutputPin(GPIO_PORT_J, GPIO_PIN_LED);






  // Init GPIOs for Input
  GPIO_setAsInputPin(GPIO_PORT_1, GPIO_PIN_EOC_SENSOR1);

  GPIO_setAsInputPin(GPIO_PORT_2, GPIO_PIN_MD3_CTRL_DIAG);

  GPIO_setAsInputPin(GPIO_PORT_3, GPIO_PIN_EOC_SENSOR2);

 /* GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_5,
                                       GPIO_PIN_SUPER_CAP_LOW_VOLTAGE);  // Missing pull-up on pin
*/

  GPIO_setAsInputPin(GPIO_PORT_2, GPIO_PIN_ENC_B);
  GPIO_setAsInputPin(GPIO_PORT_5, GPIO_PIN_ENC_A);

  GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_10,
                                       GPIO_PIN_BUTTON1);  // Missing pull-up on pin
  GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_10,
                                       GPIO_PIN_BUTTON2);  // Missing pull-up on pin


  // GPIO interrupt
  GPIO_registerInterrupt(GPIO_PORT_2, board_gpio_port2_isr);
  GPIO_registerInterrupt(GPIO_PORT_4, board_gpio_port4_isr);




  /*GPIO_registerInterrupt(GPIO_PORT_5, board_gpio_port5_isr);
  GPIO_interruptEdgeSelect(GPIO_PORT_5,
                       GPIO_PIN_SUPER_CAP_LOW_VOLTAGE,
                       GPIO_HIGH_TO_LOW_TRANSITION);
  GPIO_enableInterrupt(GPIO_PORT_5,
                       GPIO_PIN_SUPER_CAP_LOW_VOLTAGE);
  Interrupt_setPriority(INT_PORT5, 100);  // TODO (caer) check prio, high so that systick has higher
*/

 /* GPIO_registerInterrupt(GPIO_PORT_10, board_gpio_port10_isr);
  GPIO_interruptEdgeSelect(GPIO_PORT_10,
                           GPIO_PIN_BUTTON1,
                       GPIO_HIGH_TO_LOW_TRANSITION);
  GPIO_enableInterrupt(GPIO_PORT_10,
                       GPIO_PIN_BUTTON1);
  Interrupt_setPriority(INT_PORT10, 100); // TODO (caer) check prio, high so that systick has higher

  GPIO_registerInterrupt(GPIO_PORT_10, board_gpio_port10_isr);
  GPIO_interruptEdgeSelect(GPIO_PORT_10,
                           GPIO_PIN_BUTTON2,
                       GPIO_HIGH_TO_LOW_TRANSITION);
  GPIO_enableInterrupt(GPIO_PORT_10,
                       GPIO_PIN_BUTTON2);
  Interrupt_setPriority(INT_PORT10, 100); // TODO (caer) check prio, high so that systick has higher*/
  // TEST
//  GPIO_setAsOutputPin(1,1);
//  GPIO_setAsOutputPin(2,1);
//  GPIO_setAsOutputPin(6,1);
//  //GPIO_setAsOutputPin(4,0x02);
//  //GPIO_setAsOutputPin(GPIO_PORT_5,GPIO_PIN_P5_6);
//  GPIO_setAsOutputPin(4,0x04);
//  GPIO_setAsOutputPin(6,0x01);
//  GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_1, 0x10);
//  GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_1, 0x02);

  /*GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_5,
                                             GPIO_PIN_FORCE_A,
                                             GPIO_TERTIARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_5,
                                             GPIO_PIN_PRESSURE_B,
                                             GPIO_TERTIARY_MODULE_FUNCTION);
  GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_5,
                                             GPIO_PIN_PRESSURE_A,
                                             GPIO_TERTIARY_MODULE_FUNCTION);
                                             */
  ///
  return returnStatus;
}
// TODO (caer) Add all interrupt as weak and redirect in main

__attribute__((weak)) void board_gpio_port2_isr()
{
  // Clear all interrupts
  GPIO_clearInterruptFlag(GPIO_PORT_P2,
                          GPIO_getEnabledInterruptStatus(GPIO_PORT_P2));
}


__attribute__((weak)) void board_gpio_port4_isr()
{
  // Clear all interrupts
  GPIO_clearInterruptFlag(GPIO_PORT_P4,
                          GPIO_getEnabledInterruptStatus(GPIO_PORT_P4));
}

__attribute__((weak)) void board_gpio_port5_isr()
{
  // Clear all interrupts
  GPIO_clearInterruptFlag(GPIO_PORT_P5,
                          GPIO_getEnabledInterruptStatus(GPIO_PORT_P5));
}

__attribute__((weak)) void board_gpio_port7_isr()
{
  // Clear all interrupts
  GPIO_clearInterruptFlag(GPIO_PORT_P7,
                          GPIO_getEnabledInterruptStatus(GPIO_PORT_P7));
}

__attribute__((weak)) void board_gpio_port10_isr()
{
  // Clear all interrupts
  GPIO_clearInterruptFlag(GPIO_PORT_P10,
                          GPIO_getEnabledInterruptStatus(GPIO_PORT_P10));
}
/*
 * -----------------------------------------------------------------------------
 * Private function definitions
 * -----------------------------------------------------------------------------
 */

/*
 * End of file: board.c
 */
