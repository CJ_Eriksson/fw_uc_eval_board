#include <msp.h>
#include "board.h"
#include "../Hal/gpio.h"
#include "../Hal/systick.h"
#include "../Hal/timer_a.h"
#include "../Hal/rtc_c.h"
#include "../Hal/interrupt.h"
#include "../Hal/i2c.h"
#include "../Hal/uart.h"
#include "../Hal/dma.h"
#include "../Hal/spi.h"
#include "../Drivers/Time/systime.h"
#include "../Drivers/ADC/ADC.h"
#include "../Drivers/Power/VoltageControl.h"
#include "../Drivers/Power/SuperCap.h"
#include "../Drivers/Interface/button.h"
#include "../Drivers/Time/timer.h"
#include "../Drivers/PressureSensors/MPRLS.h"
#include "../Drivers/PressureSensors/MS5407_AM.h"
#include "../Drivers/MotorDrivers/trajectory.h"
#include "../Drivers/MotorDrivers/DRV8424.h"
#include "../Drivers/MotorDrivers/TMC2300_LA.h"
#include "../Drivers/MotorDrivers/DRV8210.h"
#include "../Drivers/MotorDrivers/pid.h"
#include "../Drivers/IMU/ICM_20948.h"
#include "../Drivers/Encoders/encoder.h"
#include "../Fatfs/ff.h"
#include "../Drivers/SDCard/SD_Card_SPI.h"
#include "../Log/Log_Buffer.h"
#include "../Log/Debug_UART.h"

#include "main.h"

static void test1();
static void test2();
static void test3();
static void message_poll();
static eUSCI_I2C i2c1;
static eUSCI_I2C i2c2;

uint32_t bemfthreshold = 0;
/* DMA Control Table */
#if defined(__TI_COMPILER_VERSION__)
#pragma DATA_ALIGN(controlTable, 256)
#elif defined(__IAR_SYSTEMS_ICC__)
#pragma data_alignment=1024
#elif defined(__GNUC__)
__attribute__ ((aligned (1024)))
#elif defined(__CC_ARM)
__align(1024)
#endif

#define BEMF_TARGET 0.55f // 0.55V of BEMF
#define PRESSURE_SAMPLE_TIME_MS 10
/* Control table holds the DMA settings for each channel. The alignment is done
 * since the DMA_CTLBASE register hold the pointer to this memory segment.
 * But the DMA_CTLBASE needs an alignment of 256 since it discards the lowest
 * byte in the register address. The reference manual states
 * 32byte, but it is not possible to write the lowest byte to the register and
 * I don't know why this is. The size of the table is unknown, I think it is
 * number of channels (32) and the struct has 4 uint32_t variables =
*/
static uint8_t controlTable[1024];
static uint8_t testbuff[514];
static lastUartMessage[UART_DEBUG_MESSAGE_SIZE];

static FATFS FatFs;  //Fatfs handle
static FIL fil;      //File handle
static FRESULT fres; //Result after operations

void init_i2c(void);
/**
 * main.c
 */


static void stepper_isr();

void main(void)
 {

	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer
    DMA_enableModule();
    DMA_setControlBase(controlTable);
	// Init board GPIO:s
	board_init();
	GPIO_setOutputLowOnPin(GPIO_PORT_J, GPIO_PIN_LED);
	// RTC clock must be enabled for systime init
    RTC_C_Calendar initCalendar;
    initCalendar.seconds = 0;
    initCalendar.minutes = 0;
    initCalendar.hours = 0;
    initCalendar.dayOfmonth = 1;
    initCalendar.month = 1;
    initCalendar.year = 2021;

    RTC_C_initCalendar(&initCalendar, 0);
    RTC_C_startClock();
    // System tick, synced with RTC clock
    systime_init(HAL_TICK_FREQ_DEFAULT);
    // Logg buffer init
    log_buffer_init();
    eUSCI_UART uart0;
    uart0.moduleInstance = EUSCI_A0_BASE;
    uart0.config.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
    uart0.config.clockPrescalar = SystemCoreClock/460800;
    uart0.config.uartMode = EUSCI_A_UART_MODE;
    uart0.config.parity = EUSCI_A_UART_NO_PARITY;
    uart0.config.numberofStopBits = EUSCI_A_UART_ONE_STOP_BIT;
    uart0.config.msborLsbFirst = EUSCI_A_UART_LSB_FIRST;
    uart0.config.dataLength = EUSCI_A_UART_8_BIT_LEN;
    uart0.config.overSampling = EUSCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION;
    UART_initModule(uart0.moduleInstance, &uart0.config);
    UART_enableModule(uart0.moduleInstance);
    debug_uart_dma_init(&uart0, DMA_CH1_EUSCIA0RX, DMA_CH0_EUSCIA0TX);

//    uint8_t debugRead[UART_DEBUG_MESSAGE_SIZE];
//    uint16_t nrOfBytes = 0;
//    while(1) {
//      debug_uart_read(&debugRead, &nrOfBytes);
//      if (nrOfBytes != 0) {
//        debug_uart_dma_send((void*)&debugRead, 16);
//      }
//      //while(BUSY == debug_uart_dma_send((void*)&testsend, 16));
//      //systime_wait_ms(200);
//    }
    /////////////////////////////////// DMA TEST


//    DMA_disableChannelAttribute(DMA_CH7_ADC14 ,
//                                UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST |
//                                UDMA_ATTR_HIGH_PRIORITY |
//                                UDMA_ATTR_REQMASK);
    /////////////////////////////////

    /////////////////ADC TEST/////////////////////
    // ADC init
    uint8_t ad_channels[ADC_CHANNELS] = {A0, A1, A6, A7, A8, A10, A12};
    adc_init(ADC_CHANNELS, ad_channels, TRUE);

    vc_init(GPIO_PORT_2, GPIO_PIN_MOTOR_V_EN,
            GPIO_PORT_9, GPIO_PIN_MD1_PWR_EN,
            GPIO_PORT_9, GPIO_PIN_MD2_PWR_EN,
            GPIO_PORT_8, GPIO_PIN_MD3_PWR_EN,
            GPIO_PORT_7, GPIO_PIN_V_ANALOG_EN,
            GPIO_PORT_4, GPIO_PIN_SUPER_CAP_BAT_EN,
            GPIO_PORT_2, GPIO_PIN_SUPER_CAP_CHRG_EN,
            GPIO_PORT_7, GPIO_PIN_VIN_VSENSE_EN,
            GPIO_PORT_2, GPIO_PIN_SUPER_CAP_V_SENSE_EN);

    uint32_t tick1 = systime_get_tick();
    uint16_t temopb[128] = {0}; // If assigned to {0} the DMA is fucked up...
    adc_change_freq_and_sample_time(ADC_CLOCKSOURCE_ADCOSC,
                                    ADC_PREDIVIDER_64,
                                    ADC_DIVIDER_8,
                                    ADC_PULSE_WIDTH_192);
   // adc_start_meas(A10, TRUE, 40, temopb);
   // while(adc_conversion_complete() == FALSE);
    //adc_start_meas(A1, TRUE, 40, temopb);
    //while(adc_conversion_complete() == FALSE);
    ///////////////////////////////////////////


    //////////////TIMER TEST//////////////////
    timer_interval_s superCapADC;
    superCapADC.name = "SUPERCAP";
    superCapADC.interval = 32768 * 1000/1000; // 1Hz
    superCapADC.ccrRegister = TIMER_A_CAPTURECOMPARE_REGISTER_0;
    superCapADC.intHandler = super_cap_isr;

    timer_interval_s defaultT;
    defaultT.name = NULL;
    defaultT.ccrRegister = 0;
    timer_interval_s timers[5];
    timers[0] = superCapADC;
    timers[1] = defaultT;
    timers[2] = defaultT;
    timers[3] = defaultT;
    timers[4] = defaultT;
    timer_A0_interval_init(timers);

    ////////////CAP TEST//////////////
    //super_cap_init(GPIO_PORT_5, GPIO_PIN_SUPER_CAP_LOW_VOLTAGE);
   // super_cap_charge_start();
    //super_cap_charge_sm();

    ///////////////////BUTTON///////////////
    button_init(GPIO_PORT_10,
                GPIO_PIN_BUTTON1,
                GPIO_PORT_10,
                GPIO_PIN_BUTTON1,
                NULL, NULL, NULL, NULL);

    ///////// TODO(caer) move
    Interrupt_setPriority(INT_TA0_0, 100);
    Interrupt_setPriority(INT_TA0_N, 100);

    /////////PRESSURE TEST///////////////////
    init_i2c();
    mprls_sensor_s mprls1, mprls2;

    mprls1.i2c = &i2c1;
    mprls1.address = 0x18;
    mprls1.measReadyPort = GPIO_PORT_1;
    mprls1.measReadyPin = GPIO_PIN_EOC_SENSOR1;
    mprls1.resSensorPort = GPIO_PORT_1;
    mprls1.resSensorPin = GPIO_PIN_RES_SENSOR1;

    mprls2.i2c = &i2c2;
    mprls2.address = 0x18;  // Both sensors has the same address, therefore different I2C:s
    mprls2.measReadyPort = GPIO_PORT_3;
    mprls2.measReadyPin = GPIO_PIN_EOC_SENSOR2;
    mprls2.resSensorPort = GPIO_PORT_3;
    mprls2.resSensorPin = GPIO_PIN_RES_SENSOR2;
    MPRLS_init(&mprls1);
    MPRL_reset(&mprls1);

    MPRL_start_measure(&mprls1);

    systime_wait_ms(3); // WHY!?!

    float pmeas, pmeas2;
    while(MPRL_read_measure(&mprls1, &pmeas) == BUSY);

    MPRLS_init(&mprls2);
    MPRL_reset(&mprls2);
    systime_wait_ms(4);  // 2.5ms power on reset according to datasheet
    MPRL_start_measure(&mprls2);
    systime_wait_ms(1); // WHY!?!
    while(MPRL_read_measure(&mprls2, &pmeas2) == BUSY);
    MPRL_start_measure(&mprls2);
    systime_wait_ms(3); // WHY!?!
    while(MPRL_read_measure(&mprls2, &pmeas2) == BUSY);

    //ms5407_sensor_s ms5407_A, ms5407_B;
    //ms5407_A.adcChannel = A0;
    //ms5407_A.sensorNumber = 1;

    //ms5407_B.adcChannel = A1;
//    ms5407_B.sensorNumber = 2;
//    MS5407_start_measure(&ms5407_A);
//    float data;
//    while(MP5407_read_measure(&ms5407_A, &data) != SUCCESS);
//    MS5407_start_measure(&ms5407_B);
//    float data1;
//    while(MP5407_read_measure(&ms5407_B, &data1) != SUCCESS);
    //////////////////////////////////

    ////// ENABLE MOTOR 5V DIRECTLY /////////
    GPIO_setOutputLowOnPin(GPIO_PORT_9, GPIO_PIN_MD2_PWR_EN);
    GPIO_setOutputLowOnPin(GPIO_PORT_9, GPIO_PIN_MD1_PWR_EN);
    GPIO_setOutputLowOnPin(GPIO_PORT_8, GPIO_PIN_MD3_PWR_EN);
    GPIO_setOutputLowOnPin(GPIO_PORT_4, GPIO_PIN_SUPER_CAP_BAT_EN);
    GPIO_setOutputLowOnPin(GPIO_PORT_2, GPIO_PIN_SUPER_CAP_CHRG_EN);
    GPIO_setOutputLowOnPin(GPIO_PORT_2, GPIO_PIN_MOTOR_V_EN);
    systime_wait_ms(200);
    GPIO_setOutputLowOnPin(GPIO_PORT_4, GPIO_PIN_SUPER_CAP_BAT_EN);
    //systime_wait_ms(20);
    GPIO_setOutputLowOnPin(GPIO_PORT_2, GPIO_PIN_SUPER_CAP_CHRG_EN);
    systime_wait_ms(20);
    GPIO_setOutputHighOnPin(GPIO_PORT_2, GPIO_PIN_MOTOR_V_EN);
    systime_wait_ms(20);
    //GPIO_setOutputHighOnPin(GPIO_PORT_8, GPIO_PIN_MD3_PWR_EN);
    /////////////////////////////////////////

    ///////////////// DRV8424 ////////////////////////////////
    timer_s vrefM1Timer;
    vrefM1Timer.timer = TIMER_A2_BASE;
    vrefM1Timer.pwmModeConfig.clockSource = TIMER_A_CLOCKSOURCE_SMCLK;
    vrefM1Timer.pwmModeConfig.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    vrefM1Timer.pwmModeConfig.timerPeriod = SystemCoreClock / 120000;
    vrefM1Timer.pwmModeConfig.compareOutputMode = TIMER_A_OUTPUTMODE_RESET_SET;
    vrefM1Timer.pwmModeConfig.compareRegister = TIMER_A_CAPTURECOMPARE_REGISTER_4;
    vrefM1Timer.pwmModeConfig.dutyCycle = 0;

    Timer_A_generatePWM(vrefM1Timer.timer, &vrefM1Timer.pwmModeConfig);
    /*while(1)
    {    Timer_A_setCompareValue(vrefM1Timer.timer,
                            vrefM1Timer.pwmModeConfig.compareRegister,
                            SystemCoreClock / 60000 *0.5 );
      systime_wait_ms(3000);
      Timer_A_setCompareValue(vrefM1Timer.timer,
                                  vrefM1Timer.pwmModeConfig.compareRegister,
                                  SystemCoreClock / 60000 *0.1);
            systime_wait_ms(3000);
    }*/
    // Config stepper timer
    Timer_A_UpModeConfig ta3UpConfig;
    ta3UpConfig.clockSource = TIMER_A_CLOCKSOURCE_SMCLK;
    ta3UpConfig.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    ta3UpConfig.timerClear = TIMER_A_SKIP_CLEAR;
    ta3UpConfig.timerInterruptEnable_TAIE = TIMER_A_TAIE_INTERRUPT_DISABLE;
    ta3UpConfig.timerPeriod = SystemCoreClock / STEP_FREQUENCY;  // 100kHz current control for DRV8210, 25kHz ISR for stepper
    Timer_A_configureUpMode(TIMER_A3_BASE, &ta3UpConfig);
    Timer_A_registerInterrupt(TIMER_A3_BASE, TIMER_A_CCRX_AND_OVERFLOW_INTERRUPT, stepper_isr);
    Timer_A_startCounter(TIMER_A3_BASE, TIMER_A_UP_MODE);
    //Timer_A_enableInterrupt(TIMER_A3_BASE);

    GPIO_setOutputLowOnPin(GPIO_PORT_9, GPIO_PIN_MD1_PWR_EN);
    systime_wait_ms(500);

    drv8424_init(&vrefM1Timer,
                 GPIO_PORT_6, GPIO_PIN_MD1_CTRL_STEP,
                 GPIO_PORT_6, GPIO_PIN_MD1_CTRL_DIR,
                 GPIO_PORT_6, GPIO_PIN_MD1_CTRL_NFAULT,
                 GPIO_PORT_6, GPIO_PIN_MD1_CTRL_NSLEEP,
                 GPIO_PORT_10, GPIO_PIN_P10_5,
                 GPIO_PORT_9, GPIO_PIN_MD1_DECAY_0,
                 GPIO_PORT_9, GPIO_PIN_MD1_DECAY_1,
                 GPIO_PORT_9, GPIO_PIN_MD1_TOFF,
                 GPIO_PORT_9, GPIO_PIN_MD1_M0,
                 GPIO_PORT_9, GPIO_PIN_MD1_M1,
                 30000, 400, 3000, 130, 0);
    drv8424_stepper_step_enable(TRUE);

    /////////////////////////////////////////////////////////////

    ///////////////// TMC2300-LA ////////////////////////////////
    eUSCI_UART uart1;
    uart1.moduleInstance = EUSCI_A1_BASE;
    uart1.config.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
    uart1.config.clockPrescalar = SystemCoreClock/460800;
    uart1.config.uartMode = EUSCI_A_UART_MODE;
    uart1.config.parity = EUSCI_A_UART_NO_PARITY;
    uart1.config.numberofStopBits = EUSCI_A_UART_ONE_STOP_BIT;
    uart1.config.msborLsbFirst = EUSCI_A_UART_LSB_FIRST;
    uart1.config.dataLength = EUSCI_A_UART_8_BIT_LEN;
    uart1.config.overSampling = EUSCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION;
    UART_initModule(uart1.moduleInstance, &uart1.config);
    UART_enableModule(uart1.moduleInstance);
    uint8_t datas = 0;

    GPIO_setOutputHighOnPin(GPIO_PORT_8, GPIO_PIN_MD3_PWR_EN);
    systime_wait_ms(100);
    tmc2300_init(GPIO_PORT_8, GPIO_PIN_M3_CTRL_STEP,
                 GPIO_PORT_8, GPIO_PIN_M3_CTRL_DIR,
                 GPIO_PORT_8, GPIO_PIN_MD3_MODE,
                 GPIO_PORT_8, GPIO_PIN_MD3_MS1_AD0,
                 GPIO_PORT_8, GPIO_PIN_MD3_MS2_AD1,
                 GPIO_PORT_2, GPIO_PIN_MD3_CTRL_DIAG,
                 GPIO_PORT_2, GPIO_PIN_MD3_CTRL_EN,
                 GPIO_PORT_8, GPIO_PIN_MD3_STEPPER,
                 &uart1,
                 11250, 100, 12000, 130, 0);
    tmc2300_enable_driver();  // TODO(caer) only use with UART.

    systime_wait_ms(300);
    GPIO_setOutputLowOnPin(GPIO_PORT_8, GPIO_PIN_MD3_PWR_EN);
    //tmc2300_set_target(120000);

    //Timer_A_enableInterrupt(TIMER_A3_BASE); // Stepper timer isr disabled due to null pointers before init all stepper drivers
    //systime_wait_ms(300);
    //while(tmc2300_get_speed() != 0.0f);
    //GPIO_setOutputLowOnPin(GPIO_PORT_8, GPIO_PIN_MD3_PWR_EN);
    //systime_wait_ms(5000);
    //tmc2300_set_target(0);
    //systime_wait_ms(3000);
    //tmc2300_set_target(-150000);
    //systime_wait_ms(5000);
    //tmc2300_set_target(1000);
    //systime_wait_ms(2000);
    //tmc2300_set_speed(10000, 100);
    //tmc2300_set_target(200000);
    //GPIO_setOutputHighOnPin(GPIO_PORT_9, GPIO_PIN_MD2_PWR_EN);
    ////////////////////////////////////////////////////

    //////////////// DRV8210 ///////////////////////////
    Timer_A_PWMConfig pwmConfig;
    pwmConfig.clockSource = TIMER_A_CLOCKSOURCE_SMCLK;
    pwmConfig.clockSourceDivider = TIMER_A_CLOCKSOURCE_DIVIDER_1;
    pwmConfig.timerPeriod = SystemCoreClock/90000;
    pwmConfig.dutyCycle = 0;
    pwmConfig.compareOutputMode = TIMER_A_OUTPUTMODE_RESET_SET;  //Slow decay RESET_SET == Fast Decay

/*
    timer_s drv8210_pwm_timer_Ain1;
    timer_s drv8210_pwm_timer_Ain2;
    timer_s drv8210_pwm_timer_Bin1;
    timer_s drv8210_pwm_timer_Bin2;
    drv8210_pwm_timer_Ain1.timer =
        drv8210_pwm_timer_Ain2.timer =
            drv8210_pwm_timer_Bin1.timer =
                drv8210_pwm_timer_Bin2.timer = TIMER_A1_BASE; // TODO(caer) A1 base but launchpad has some fault with one of the outputs

    drv8210_pwm_timer_Ain1.pwmModeConfig = pwmConfig;
    drv8210_pwm_timer_Ain1.pwmModeConfig.compareRegister =
        TIMER_A_CAPTURECOMPARE_REGISTER_4;

    drv8210_pwm_timer_Ain2.pwmModeConfig = pwmConfig;
    drv8210_pwm_timer_Ain2.pwmModeConfig.compareRegister =
        TIMER_A_CAPTURECOMPARE_REGISTER_3;

    drv8210_pwm_timer_Bin1.pwmModeConfig = pwmConfig;
    drv8210_pwm_timer_Bin1.pwmModeConfig.compareRegister =
        TIMER_A_CAPTURECOMPARE_REGISTER_2;

    drv8210_pwm_timer_Bin2.pwmModeConfig = pwmConfig;
    drv8210_pwm_timer_Bin2.pwmModeConfig.compareRegister =
        TIMER_A_CAPTURECOMPARE_REGISTER_1;
*/
    GPIO_setOutputHighOnPin(GPIO_PORT_9, GPIO_PIN_MD2_MODE); // TODO(caer) move to drv8210 init
    drv8210_init(&vrefM1Timer, GPIO_PORT_7, GPIO_PIN_MD2_A_IN1_PH,
                 GPIO_PORT_7, GPIO_PIN_MD2_A_IN2_EN,
                 GPIO_PORT_7, GPIO_PIN_MD2_B_IN1_PH,
                 GPIO_PORT_7, GPIO_PIN_MD2_B_IN2_EN,
                 30000, 100, 4000, 200, 50);

    /*drv8210_init(&drv8210_pwm_timer_Ain1,
                 &drv8210_pwm_timer_Ain2,
                 &drv8210_pwm_timer_Bin1,
                 &drv8210_pwm_timer_Bin2,
                 10000, 100, 9600, 1000, 10);*/
    //Timer_A_enableInterrupt(TIMER_A3_BASE); // Stepper timer isr disabled due to null pointers before init all stepper drivers
   // drv8210_stepper_step_enable(TRUE);
   // drv8210_set_target(50000);
    //systime_wait_ms(8000);
    //drv8210_set_target(1000000);
   // while(1);
    ////////////////////////////////////////////////////////

    //////////// ENABLE STEPPER TIMER ///////////////////////
    Timer_A_enableInterrupt(TIMER_A3_BASE); // Stepper timer isr disabled due to null pointers before init all stepper drivers
    /////////////////////////////////////////////////////////

    //////////// ENABLE ENCODER ////////////////////
    encoder_init(GPIO_PORT_4, GPIO_PIN_ENC_A, GPIO_PORT_2, GPIO_PIN_ENC_B);
    ////////////// SPI B1 SD-Card //////////////////////////
    // The SD card only works at <600kHz SPI communication. At higher speed
    // different errors occurs for the send/receive
    spi_s spi_b1;
    spi_b1.module = EUSCI_B1_BASE;
    spi_b1.speed = 600000; // Problem with higher speed
    spi_b1.config.spiMode = EUSCI_SPI_3PIN;
    spi_b1.config.selectClockSource = EUSCI_SPI_CLOCKSOURCE_SMCLK;
    spi_b1.config.msbFirst = EUSCI_SPI_MSB_FIRST;
    spi_b1.config.desiredSpiClock = spi_b1.speed; // 1 Mbit/s
    spi_b1.config.clockSourceFrequency = SystemCoreClock;
    spi_b1.config.clockPolarity = EUSCI_SPI_CLOCKPOLARITY_INACTIVITY_HIGH;
    spi_b1.config.clockPhase = EUSCI_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT;
    SPI_initMaster(spi_b1.module, &spi_b1.config);
    SPI_enableModule(spi_b1.module);
    //sd_spi_init(&spi_b1, GPIO_PORT_J, GPIO_PIN_SD_CARD_CS, 3300);
    sd_spi_init(&spi_b1, GPIO_PORT_J, GPIO_PIN_SD_CARD_CS, 3300, DMA_CH3_EUSCIB1RX0, DMA_CH2_EUSCIB1TX0);
    sd_spi_initialize(0);
/*
    //Open the file system
    FatFs.pdrv = 1;
    DWORD free_clusters, free_sectors, total_sectors;
    FATFS* getFreeFs;
    uint16_t count = 0;
    uint32_t tickstart, tickstop = 0;

    tickstart = systime_get_tick();
    fres = f_mount(&FatFs, "", 1); //1=mount now
    if (fres != FR_OK) {
      while(1);
    }

    //Let's get some statistics from the SD card
    fres = f_getfree("", &free_clusters, &getFreeFs);
    if (fres != FR_OK) {
      while(1);
    }

    TCHAR name[20];
    DWORD vsn = 0;
    fres = f_getlabel("", name, &vsn);
    if (fres != FR_OK) {
      while(1);
    }

    fres = f_open(&fil, "test.txt", FA_WRITE | FA_OPEN_APPEND);
    if (fres != FR_OK) {
      while(1);
    }
/*
    uint8_t text[10] = {'H','E','J','P','a','A','I','G','\r','\n'};
    UINT written = 0;
    fres = f_write (&fil, &text, 10, &written);
    if (fres != FR_OK) {
       while(1);
    }
    fres = f_close(&fil);
*/
    ////////////////////////////////////////////////////////////

    ////////////////////// IMU /////////////////////////////////
    imu_init(&spi_b1, GPIO_PORT_J, GPIO_PIN_ACC_CS, IMU_ACC_SENSITIVITY_4G, TRUE, IMU_GYRO_SENSITIVITY_250dps, TRUE);
/*
    float z_acc = 0;
    float z_gyro = 0;
    imu_read_acc(IMU_ACC_Z, &z_acc);
    imu_read_gyro(IMU_GYRO_Z, &z_gyro);
    int16_t magx, magy, magz;
    imu_get_magnetometer_from_ext_reg(&magx, &magy, &magz);*/
    ///////////////////////////////////////////////////////////
/*
while(1) {
  tmc2300_read_register(TMC2300_REG_PWM_SCALE_71, &scaleSum);
  debugRead[2] = (((uint8_t)(scaleSum) % 100) % 10) | 0x30;
  debugRead[1] = (((uint8_t)(scaleSum) % 100) / 10) | 0x30;
  debugRead[0] = ((uint8_t)(scaleSum) / 100) | 0x30;
  speed = tmc2300_get_speed();
  debugRead[7] = ((uint32_t)(speed*10000) % 10) | 0x30;
  debugRead[6] = ((uint32_t)(speed*1000) % 10) | 0x30;
  debugRead[5] = ((uint32_t)(speed*100) % 10)| 0x30;
  debugRead[4] = ((uint32_t)(speed*10)) | 0x30;
  //uint32_t time = systime_get_tick();
  //debugRead[9] = ((uint32_t)(time) % 100 / 10) | 0x30;
  //debugRead[10] = ((uint32_t)(time) % 10) | 0x30;
  tmc2300_read_register(TMC2300_REG_SG_VALUE_41, &sg);
  debugRead[12] = (uint8_t)(sg % 10) | 0x30;
  debugRead[11] = (uint8_t)((sg % 100) / 10) | 0x30;
  debugRead[10] = (uint8_t)((sg % 1000) / 100) | 0x30;
  debugRead[9] = (uint8_t)(sg / 1000) | 0x30;
  debugRead[14] = '\r';
  debugRead[15] = '\n';
  //uint32_t status;
  //tmc2300_read_register(TMC2300_REG_PWMCONF_70, &status);
  debug_uart_dma_send((void*)&debugRead, 16);
} */
    float x_acc, y_acc, z_acc;
    unsigned int byte_to_write;
    static char data[50];
    float P = 0.18f;
    float I = 0.0f;
    float D = 0.0f;
    float control = 0.0f;
    static float volt = 0.0f;
    uint32_t bemfData, prevBemfData;
    uint32_t encoderStart = 0;
    uint32_t encoderStop = 0;
    uint32_t encoderEnd = 0;
    uint32_t lastEncoderPos = 0;
    float stepStart = 0;
    pid_set(P,I,D);
    GPIO_setOutputHighOnPin(GPIO_PORT_J, GPIO_PIN_LED);

    drv8424_set_current(184,0);
    drv8424_set_speed(8000, 250);
    drv8424_set_stepmode(STEPPER_1_8_STEP);
    drv8424_set_acceleration(29000);

    //drv8424_set_target(12000);
    systime_wait_ms(1000);
    //GPIO_setOutputLowOnPin(GPIO_PORT_7, GPIO_PIN_MD1_CTRL_EN);
    //GPIO_setOutputLowOnPin(GPIO_PORT_6, GPIO_PIN_MD1_CTRL_NSLEEP);

    //drv8424_homing(20000);
    //drv8424_set_target(70000);
    uint32_t sample_time = systime_get_tick();
    bool_t pressureSamplingRunning = FALSE;
    GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_4);
    GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_3);
    FOREVER {
/*
      bemfData = bemf_get_last_meas();
      if (prevBemfData != bemfData) {
        byte_to_write = sprintf(data,
                                        "%09u:%07ld:%06ld:%06ld\r\n", systime_get_tick(), bemfData, (int32_t)(drv8424_get_speed()*1000), (int32_t)(drv8424_get_position()*1000));
        debug_uart_dma_send(&data, byte_to_write);
        prevBemfData = bemfData;
      }
*/
      if (pressureSamplingRunning == TRUE) {
        pmeas2 = 0;
        GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_4);
        if (GPIO_getInputPinValue(GPIO_PORT_3, GPIO_PIN_EOC_SENSOR2) == GPIO_INPUT_PIN_HIGH) {
          if (MPRL_read_measure(&mprls2, &pmeas2) == SUCCESS) {
            MPRL_read_measure(&mprls1, &pmeas);
            pressureSamplingRunning = FALSE;
            byte_to_write = sprintf(data,"%09u:%10d:%10d:%10d:\r\n", systime_get_tick(), (uint32_t)((pmeas2-pmeas)*10), (uint32_t)(pmeas*10), (int32_t)(drv8424_get_position()*1000));
            debug_uart_dma_send(&data, byte_to_write);
          }
        }
        GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_4);
      }

      if (systime_get_tick() >= sample_time && pressureSamplingRunning == FALSE) {
        GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_3);
        // Calculate time for next sample.
        MPRL_start_measure(&mprls2);
        MPRL_start_measure(&mprls1);
        pressureSamplingRunning = TRUE;
        sample_time = systime_get_tick() + PRESSURE_SAMPLE_TIME_MS;
        GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_3);
      }



      message_poll();

    }
}
//GPIO_setOutputHighOnPin(6,0x01);
//GPIO_setOutputHighOnPin(4,0x02);
//  GPIO_setOutputHighOnPin(2,0x40);
void stepper_isr()
{
 GPIO_setOutputHighOnPin(GPIO_PORT_10, GPIO_PIN_P10_2);
//  static uint32_t tick = 0;
//  tick++;
//  if (tick == 4) {
    //tmc2300_step();
    drv8424_step();
    //drv8210_step();
//    tick = 0;
//  }
  //drv8210_current_control();
  Timer_A_clearInterruptFlag(TIMER_A3_BASE);
  GPIO_setOutputLowOnPin(GPIO_PORT_10, GPIO_PIN_P10_2);

}
void board_gpio_port2_isr()
{
  //GPIO_setOutputHighOnPin(10,0x04);
  encoder_isr();
  //Clear all interrupt since only one for port 2
  GPIO_clearInterruptFlag(GPIO_PORT_P2,
                          GPIO_getEnabledInterruptStatus(GPIO_PORT_P2));
//  GPIO_setOutputLowOnPin(10,0x04);
}

void board_gpio_port4_isr()
{
  //GPIO_setOutputHighOnPin(10,0x04);
  encoder_isr();
  //Clear all interrupt since only one for port 4
  GPIO_clearInterruptFlag(GPIO_PORT_P4,
                          GPIO_getEnabledInterruptStatus(GPIO_PORT_P4));
 // GPIO_setOutputLowOnPin(10,0x04);
}

void board_gpio_port5_isr()
{

  uint16_t status = GPIO_getEnabledInterruptStatus(GPIO_PORT_P5);
  if ((status & GPIO_PIN_SUPER_CAP_LOW_VOLTAGE) != 0) {
    super_cap_isr_low_voltage_pin();
    GPIO_clearInterruptFlag(GPIO_PORT_P5,
                                (status & GPIO_PIN_SUPER_CAP_LOW_VOLTAGE));
  }
}


void init_i2c(void)
{
  i2c1.config.autoSTOPGeneration = EUSCI_B_I2C_NO_AUTO_STOP;
  i2c1.config.byteCounterThreshold = 0;
  i2c1.config.dataRate = EUSCI_B_I2C_SET_DATA_RATE_400KBPS;
  i2c1.config.i2cClk = SystemCoreClock;
  i2c1.config.selectClockSource = EUSCI_B_I2C_CLOCKSOURCE_SMCLK;
  i2c1.moduleInstance = EUSCI_B0_BASE;

  i2c2.config.autoSTOPGeneration = EUSCI_B_I2C_NO_AUTO_STOP;
  i2c2.config.byteCounterThreshold = 0;
  i2c2.config.dataRate = EUSCI_B_I2C_SET_DATA_RATE_400KBPS;
  i2c2.config.i2cClk = SystemCoreClock;
  i2c2.config.selectClockSource = EUSCI_B_I2C_CLOCKSOURCE_SMCLK;
  i2c2.moduleInstance = EUSCI_B2_BASE;

  I2C_initMaster(i2c1.moduleInstance, &i2c1.config);
  I2C_initMaster(i2c2.moduleInstance, &i2c2.config);
}

static void message_poll()
{

  ErrorStatus returnStatus = ERROR;
  uint8_t readBuffer[UART_DEBUG_MESSAGE_SIZE];
  uint16_t nrOfBytes = 0;
  char message[UART_DEBUG_MESSAGE_SIZE];
  debug_uart_read(&readBuffer, &nrOfBytes);
  if (nrOfBytes != 0) {
    memcpy(message, readBuffer, UART_DEBUG_MESSAGE_SIZE);
    memcpy(lastUartMessage, message, UART_DEBUG_MESSAGE_SIZE);
    if (strstr(message, "STOP")) {
      drv8424_stop();
      returnStatus = SUCCESS;
    } else if (strstr(message, "STEPSIZE_")) {
      uint16_t stepSize = (*(message + 9) - 0x30)*10 + (*(message + 10) - 0x30);
      stepper_step_mode_e stepMode;
      switch (stepSize) {
        case 1:
          stepMode = STEPPER_FULL_STEP;
          break;
        case 2:
          stepMode = STEPPER_1_2_STEP;
          break;
        case 4:
          stepMode = STEPPER_1_4_STEP;
          break;
        case 8:
          stepMode = STEPPER_1_8_STEP;
          break;
        case 16:
          stepMode = STEPPER_1_16_STEP;
          break;
        case 32:
          stepMode = STEPPER_1_32_STEP;
          break;
        case 64:
          stepMode = STEPPER_1_64_STEP;
          break;
        case 128:
          stepMode = STEPPER_1_128_STEP;
          break;
        case 256:
          stepMode = STEPPER_1_256_STEP;
          break;
        default:
          stepMode = STEPPER_FULL_STEP;
      }
      if (drv8210_set_step_size(stepMode) == SUCCESS &&
          drv8424_set_stepmode(stepMode) == SUCCESS &&
          tmc2300_set_stepmode(stepMode) == SUCCESS) {
        returnStatus = SUCCESS;
      }
    } else if (strstr(message, "DECAY_")) {
      drv8424_decay_modes_e decay_mode = (drv8424_decay_modes_e)(*(message + 6) - 0x30);
      returnStatus = drv8424_set_decay(decay_mode);
    } else if (strstr(message, "SPEED_")) {
      uint32_t stepMaxSpeed = (*(message + 6) - 0x30)*10000 + (*(message + 7) - 0x30)*1000 + (*(message + 8)- 0x30)*100 + (*(message + 9) - 0x30)*10 + (*(message + 10) - 0x30);
      uint32_t stepMinSpeed = (*(message + 12) - 0x30)*100 + (*(message + 13) - 0x30)*10 + (*(message + 14) - 0x30);
      if (drv8210_set_speed(stepMaxSpeed, stepMinSpeed) == SUCCESS &&
      drv8424_set_speed(stepMaxSpeed, stepMinSpeed) == SUCCESS &&
      tmc2300_set_speed(stepMaxSpeed, stepMinSpeed) == SUCCESS) {
        returnStatus = SUCCESS;
      }
    } else if (strstr(message, "DIST_")) {
      int32_t target = (*(message + 6) - 0x30)*100000 + (*(message + 7) - 0x30)*10000 + (*(message + 8) - 0x30)*1000 + (*(message + 9)- 0x30)*100 + (*(message + 10) - 0x30)*10 + (*(message + 11) - 0x30);
      if (*(message+5) == '-') {
        target = -target;
      }
      if (drv8210_set_target(target) == SUCCESS &&
          drv8424_set_target(target) == SUCCESS &&
          tmc2300_set_target(target) == SUCCESS) {
        returnStatus = SUCCESS;
      }
    } else if (strstr(message, "EN_")) {
        returnStatus = SUCCESS;
        if (strstr(message, "DRV8424")) {
          drv8424_stepper_step_enable(TRUE);
          GPIO_setOutputHighOnPin(GPIO_PORT_9, GPIO_PIN_MD1_PWR_EN);
        } else if (strstr(message, "DRV8210")) {
          drv8210_stepper_step_enable(TRUE);
          GPIO_setOutputHighOnPin(GPIO_PORT_9, GPIO_PIN_MD2_PWR_EN);
        } else if (strstr(message, "TMC2300")) {
          tmc2300_stepper_step_enable(TRUE);
          //GPIO_setOutputHighOnPin(GPIO_PORT_8, GPIO_PIN_MD3_PWR_EN);
        } else {
          returnStatus = ERROR;
        }
        returnStatus = SUCCESS;
    } else if (strstr(message, "DISABLE_")) {
        returnStatus = SUCCESS;
        if (strstr(message, "DRV8424")) {
          drv8424_stepper_step_enable(FALSE);
          GPIO_setOutputLowOnPin(GPIO_PORT_9, GPIO_PIN_MD1_PWR_EN);
        } else if (strstr(message, "DRV8210")) {
          drv8210_stepper_step_enable(FALSE);
          GPIO_setOutputLowOnPin(GPIO_PORT_9, GPIO_PIN_MD2_PWR_EN);
        } else if (strstr(message, "TMC2300")) {
          drv8210_stepper_step_enable(FALSE);
          //GPIO_setOutputLowOnPin(GPIO_PORT_8, GPIO_PIN_MD3_PWR_EN);
        } else {
          returnStatus = ERROR;
        }
  } else if (strstr(message, "CURR_")) {
    uint16_t maxCurrent = (*(message + 5) - 0x30)*1000 + (*(message + 6) - 0x30)*100 + (*(message + 7)- 0x30)*10 + (*(message + 8) - 0x30);
    uint16_t minCurrent = (*(message + 10) - 0x30)*100 + (*(message + 11) - 0x30)*10 + (*(message + 12) - 0x30);
    if (tmc2300_set_current(maxCurrent, minCurrent) == SUCCESS &&
    drv8424_set_current(maxCurrent, minCurrent) == SUCCESS) {
      returnStatus = SUCCESS;
    }
  }
    static uint8_t response[UART_DEBUG_MESSAGE_SIZE] = "________________";
    response[5] = response[6] = response[9] = '_';
    if (returnStatus == SUCCESS) {
      response[7] = 'O';
      response[8] = 'K';
    } else {
      response[5] = 'E';
      response[6] = 'R';
      response[7] = 'R';
      response[8] = 'O';
      response[9] = 'R';
    }
    debug_uart_dma_send((void*)&response, 16);
  }

}
