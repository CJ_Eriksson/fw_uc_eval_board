/**
 * @file board.h
 * @brief Configuration for the board
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _BOARD_H_
#define _BOARD_H_

#include <stdint.h>
#include "../Hal/gpio.h"
#include "../Common/common_types.h"
/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!L�GG TILL SK�TER INIT AV ALLA ING�NGAR
/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */

#define PROCESS_VOLTAGE 3300

#define ADC_CHANNELS 7

#define GPIO_PORT_0 0

#define GPIO_PORT_1 1
#define GPIO_PIN_ACC_FSYNC 0x01
#define GPIO_PIN_N_CHRG 0x02
#define GPIO_PIN_UART_RX 0x04
#define GPIO_PIN_UART_TX 0x08
#define GPIO_PIN_EOC_SENSOR1 0x10
#define GPIO_PIN_RES_SENSOR1 0x20
#define GPIO_PIN_SDA_SENSOR1 0x40
#define GPIO_PIN_SCL_SENSOR1 0x80

#define GPIO_PORT_2 2
#define GPIO_PIN_MD3_CTRL_DIAG 0x01
#define GPIO_PIN_MOTOR_V_EN 0x02
#define GPIO_PIN_MD3_UART_RX 0x04
#define GPIO_PIN_MD3_UART_TX 0x08
#define GPIO_PIN_ENC_B 0x10
#define GPIO_PIN_SUPER_CAP_CHRG_EN 0x20
#define GPIO_PIN_SUPER_CAP_V_SENSE_EN 0x40
#define GPIO_PIN_MD3_CTRL_EN 0x80

#define GPIO_PORT_3 3
#define GPIO_PIN_ACC_INT1 0x01
#define GPIO_PIN_ACC_INT2 0x02
#define GPIO_PIN_VLF 0x04
#define GPIO_PIN_EOC_SENSOR2 0x10
#define GPIO_PIN_RES_SENSOR2 0x20
#define GPIO_PIN_SDA_SENSOR2 0x40
#define GPIO_PIN_SCL_SENSOR2 0x80

#define GPIO_PORT_4 4
#define GPIO_PIN_ISENSE_NINT 0x01
#define GPIO_PIN_VIN_VSENSE 0x02
#define GPIO_PIN_P4_2 0x04
#define GPIO_PIN_MD1_BEMF 0x08
#define GPIO_PIN_SUPER_CAP_BAT_EN 0x10
#define GPIO_PIN_MD2_BEMF 0x20
#define GPIO_PIN_MOTOR_V_SENSE 0x40
#define GPIO_PIN_SUPER_CAP_V_SENSE 0x80

#define GPIO_PORT_5 5
#define GPIO_PIN_REF_B 0x01
#define GPIO_PIN_REF_A 0x02
#define GPIO_PIN_FORCE_B 0x04
#define GPIO_PIN_FORCE_A 0x08
#define GPIO_PIN_PRESSURE_B 0x10
#define GPIO_PIN_PRESSURE_A 0x20
#define GPIO_PIN_ENC_A 0x40
#define GPIO_PIN_SUPER_CAP_LOW_VOLTAGE 0x80

#define GPIO_PORT_6 6
#define GPIO_PIN_MD1_CTRL_NFAULT 0x01
#define GPIO_PIN_MD1_CTRL_DIR 0x02
#define GPIO_PIN_MD1_CTRL_NSLEEP 0x04
#define GPIO_PIN_SPIB1_CLK 0x08
#define GPIO_PIN_SPIB1_SIMO 0x10
#define GPIO_PIN_SPIB1_SOMI 0x20
#define GPIO_PIN_MD1_CTRL_STEP 0x40
#define GPIO_PIN_MD1_CTRL_VREF_PWM 0x80

#define GPIO_PORT_7 7
#define GPIO_PIN_VIN_VSENSE_EN 0x01
#define GPIO_PIN_MD1_CTRL_EN 0x02
#define GPIO_PIN_V_ANALOG_EN 0x04
#define GPIO_PIN_ENC_A_REPLACED 0x08
#define GPIO_PIN_MD2_A_IN1_PH 0x10
#define GPIO_PIN_MD2_A_IN2_EN 0x20
#define GPIO_PIN_MD2_B_IN1_PH 0x40
#define GPIO_PIN_MD2_B_IN2_EN 0x80

#define GPIO_PORT_8 8
#define GPIO_PIN_M3_CTRL_DIR 0x01
#define GPIO_PIN_M3_CTRL_STEP 0x02
#define GPIO_PIN_MD3_PWR_EN 0x04
#define GPIO_PIN_MD3_STEPPER 0x08
#define GPIO_PIN_MD3_MODE 0x10
#define GPIO_PIN_MD3_MS2_AD1 0x20
#define GPIO_PIN_MD3_MS1_AD0 0x40
#define GPIO_PIN_P8_7 0x80

#define GPIO_PORT_9 9
#define GPIO_PIN_MD1_PWR_EN 0x01
#define GPIO_PIN_MD1_M0 0x02
#define GPIO_PIN_MD1_M1 0x04
#define GPIO_PIN_MD1_DECAY_1 0x08
#define GPIO_PIN_MD1_DECAY_0 0x10
#define GPIO_PIN_MD1_TOFF 0x20
#define GPIO_PIN_MD2_PWR_EN 0x40
#define GPIO_PIN_MD2_MODE 0x80

#define GPIO_PORT_10 10
#define GPIO_PIN_BUTTON1 0x01
#define GPIO_PIN_BUTTON2 0x02
#define GPIO_PIN_P10_2 0x04
#define GPIO_PIN_P10_3 0x08
#define GPIO_PIN_P10_4 0x10
#define GPIO_PIN_P10_5 0x20

#define GPIO_PORT_J 11
#define GPIO_PIN_ACC_CS 0x01
#define GPIO_PIN_SPI_CS 0x02
#define GPIO_PIN_SD_CARD_CS 0x04
#define GPIO_PIN_I_SENSE_NSHDN 0x08
#define GPIO_PIN_LED 0x10
#define GPIO_PIN_CHARGE_NCHARGE_FAULT 0x20
/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */
typedef struct {
  uint_fast8_t port;
  uint_fast16_t pin;
} gpio_s;
/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */
ErrorStatus board_init();
void board_gpio_port2_isr();
void board_gpio_port4_isr();
void board_gpio_port5_isr();
void board_gpio_port10_isr();
void button_debounce_isr();

#ifdef __cplusplus
}
#endif

#endif /* _BOARD_H_ */

/*
 * End of file: board.h
 */

