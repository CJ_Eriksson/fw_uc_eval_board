/**
 * @file common_types.h
 * @brief This file contains common types
 *
 * @copyright
 *        Unpublished Work Copyright 2020 Implantica. All Rights Reserved.
 *        This work is proprietary and confidential to Implantica.
 *
 */

/*
 * -----------------------------------------------------------------------------
 * Include Guard
 * -----------------------------------------------------------------------------
 */
#ifndef _COMMON_TYPES_H_
#define _COMMON_TYPES_H_

/*
 * -----------------------------------------------------------------------------
 * C Linkage
 * -----------------------------------------------------------------------------
 */
#ifdef __cplusplus
extern "C" {
#endif

/*
 * -----------------------------------------------------------------------------
 * Public defines
 * -----------------------------------------------------------------------------
 */
#ifndef NULL
    #define NULL    (0)                      /**<  NULL Null definition  */
#endif

#define TMC2300LA_UART

#define MIN_UINT8   (0u)                     /**< Min value for a uint8_t  */
#define MAX_UINT8   (0xFFu)                  /**< Max value for a uint8_t  */
#define MIN_UINT16  (0u)                     /**< Min value for a uint16_t */
#define MAX_UINT16  (0xFFFFu)                /**< Max value for a uint16_t */
#define MIN_UINT32  (0uL)                    /**< Min value for a uint32_t */
#define MAX_UINT32  (0xFFFFFFFFuL)           /**< Max value for a uint32_t */
#define MIN_UINT64  (0uLL)                   /**< Min value for a uint64_t */
#define MAX_UINT64  (0xFFFFFFFFFFFFFFFFuLL)  /**< Max value for a uint64_t */

#define MAX_INT8    (0x7F)                   /**< Max value for a int8   */
#define MIN_INT8    (-MAX_INT8 - 1)          /**< Min value for a int8   */
#define MAX_INT16   (0x7FFF)                 /**< Max value for a int16  */
#define MIN_INT16   (-MAX_INT16 - 1)         /**< Min value for a int16  */
#define MAX_INT32   (0x7FFFFFFFL)            /**< Max value for a int32  */
#define MIN_INT32   (-MAX_INT32 - 1)         /**< Min value for a int32  */
#define MAX_INT64   (0x7FFFFFFFFFFFFFFFLL)   /**< Max value for a int64  */
#define MIN_INT64   (-MAX_INT64 - 1)         /**< Min value for a int64  */

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

/*
 * -----------------------------------------------------------------------------
 * Public enums
 * -----------------------------------------------------------------------------
 */
typedef char plainChar;  /**< typedef of a plain char, signed or unsigned is application dependent */

typedef enum {  /**< @enum bool_t */
   FALSE = 0,   /**< false definition for this enum */
   TRUE = 1     /**< true definition for this enum */
} bool_t;       /**< Re-definition of bool for MISRA compatibility */

typedef enum
{
  SUCCESS = 0U,
  ERROR = !SUCCESS,
  BUSY = 2
} ErrorStatus;
/*
 * -----------------------------------------------------------------------------
 * Public structs
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public data types
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public variable declaration
 * -----------------------------------------------------------------------------
 */

/*
 * -----------------------------------------------------------------------------
 * Public function prototypes
 * -----------------------------------------------------------------------------
 */

#ifdef __cplusplus
}
#endif

#endif /* _COMMON_TYPES_H_ */

/*
 * End of file: common_types.h
 */
